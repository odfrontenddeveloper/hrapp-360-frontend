import React from "react";
import { observer } from "mobx-react";
import globalConfig from "../appstate/global_config";

const ItemUserResults = observer((props) => {
  return (
    <div className={`item-user-assessment-list`}>
      <div className='item-user-assessment-list-avatar'>
        <div className='item-user-assessment-list-avatar-block' style={{ backgroundImage: `url('${globalConfig.serverAddress}/useravatar?login=${props.user.login}')` }}></div>
      </div>
      <div className='item-user-assessment-list-info'>
        <div className='item-user-assessment-list-info-text night-mode-text'>{props.user.surename}</div>
        <div className='item-user-assessment-list-info-text night-mode-text'>{props.user.name}</div>
        <div className='item-user-assessment-list-info-text night-mode-text'>{props.user.patronymic}</div>
      </div>
      <div className='item-user-assessment-list-info'>
        <div className='item-user-assessment-list-info-text night-mode-text'>{props.wp ? props.wp : `Укажите должность.`}</div>
      </div>
      <div className={`item-user-assessment-list-button`}>
        <button
          type='button'
          className={`mini-functional-button mini-success-button mini-results-button`}
          onClick={() => { props.getUserResults(props.user.id) }}
        ></button>
      </div>
    </div>
  );
});

export default ItemUserResults;