import React, { useState } from "react";
import RedactData from "./RedactData";
import globalConfig from "../appstate/global_config";
import { imageLoadOperations } from "../appstate/styles_config.js";
import { observer } from "mobx-react";
import axios from "axios";
import { resultsState } from "../appstate/showResultsState";

const RedactNameAssessment = observer((props) => {
  let refButton = React.createRef();

  let [resAddRename, changeresAddRename] = useState(null);
  let [formResColor, changeformResColor] = useState(null);

  let handlerAddWPForm = (e) => {
    e.preventDefault();
    let data = {};

    data.id = refButton.current.value;
    data.nameassessment = props.refInputName.current.value;
    imageLoadOperations.openImageLoad();
    axios({
      method: 'patch',
      url: globalConfig.serverAddress + `/renameAssessment`,
      data: data
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        imageLoadOperations.closeImageLoad();
        if (response.data.status == true) {
          resultsState.assessmentsList = resultsState.assessmentsList.map(function (el, i) {
            if (el.id == data.id) {
              el = response.data.content[0];
            }
            return el;
          });
          changeformResColor(true);
          changeresAddRename('Название мероприятия изменено.');
        }
        else {
          changeformResColor(false);
          changeresAddRename(response.data.content);
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  }

  let onCloseForm = () => {
    changeresAddRename(null);
  }

  return (
    <RedactData forRef={props.refForm} onCloseForm={onCloseForm}>
      <form
        className='standart-form vertical-form form-center night-mode-form-bg'
        onSubmit={(e) => { handlerAddWPForm(e) }}
      >
        <div className='form-container input-container form-container-column'>
          <label
            className='form-container-label night-mode-text'
            htmlFor='rename_assessment'
          >
            Введите новое название мероприятия:
          </label>
          <input
            id='rename_assessment'
            className='form-container-input night-mode-text night-mode-input'
            type='text'
            ref={props.refInputName}
          />
        </div>
        <div className='button-container'>
          <button
            type='submit'
            className='mini-functional-button mini-success-button mini-complete-button'
            value={props.redactId}
            ref={refButton}
          ></button>
        </div>
      </form>
      <div className={`result-container res-form-center ${formResColor ? 'successColor' : 'dangerColor'}`} >
        {resAddRename}
      </div>
    </RedactData>
  );
});

export default RedactNameAssessment;