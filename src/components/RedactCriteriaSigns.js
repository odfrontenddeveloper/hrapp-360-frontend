import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import CheckAuth from "./CheckAuth.js";
import UserAreaHeader from "./UserAreaHeader.js";
import { criteriaSignsOperations, criteriaSignsState } from "../appstate/criteriasignsState.js";
import SearchCompetenciesInCS from "./SearchCompetenciesInCS.js";
import ItemCompetenceInCS from "./ItemCompetenceInCS.js";
import AddRedactCriteriaForm from "./AddRedactCriteriaForm.js";
import ChangeCriteriaForms from "./ChangeCriteriaForms.js";

import '../appstate/arrayPrototype';
import shortid from "shortid";
import DeleteCriterionForm from "./DeleteCriterionForm.js";

const RedactCriteriaSigns = observer(() => {
  let refArea = React.createRef();
  let formAddCriteria = React.createRef();
  let formRedactCriteria = React.createRef();
  let formDeleteCriteria = React.createRef();

  let [redactCriterionID, changeRedactCriterionID] = useState(null);
  let [searchValue, changeSearchValue] = useState('');
  let [searchWPValue, changeSearchWPValue] = useState('');
  let [deleteId, changedeleteId] = useState(null);

  useEffect(() => {
    criteriaSignsOperations.getCompetenciesListForCS();
    criteriaSignsOperations.getWpListForCS();
  }, []);

  let openAddCriteriaForm = () => {
    formAddCriteria.current.style.display = 'flex';
  }

  let openRedactCriteriaForm = (id) => {
    if (redactCriterionID != id) {
      changeRedactCriterionID(id);
      criteriaSignsState.rangesRedact = {};
      criteriaSignsState.criterialist.forEach(criterion => {
        if (criterion.criterion.id == id) {
          criteriaSignsState.typeCheckboxRedact = criterion.criterion.typecheckbox ? true : false;
          criteriaSignsState.criterionRedact = criterion.criterion.text;
          criterion.signs.selectFields(['text', 'weight']).map(element => {
            let newid = shortid.generate();
            criteriaSignsState.rangesRedact[newid] = {
              name: element.text,
              value: element.weight ? element.weight : 0
            };
          });
        }
      });
    }
    formRedactCriteria.current.style.display = 'flex';
  }

  let openDeleteCriteriaForm = (value) => {
    changedeleteId(value);
    formDeleteCriteria.current.style.display = 'flex';
  }

  return (
    <div className='redact-criteria-signs-place night-mode-bg-block' ref={refArea}>
      <CheckAuth />
      <UserAreaHeader
        logout={refArea}
        backButton={true}
      />
      <div className='redact-criteria-signs-place-menu'>
        <div className='redact-criteria-signs-place-menu-select-competence'>
          <div className='redact-criteria-signs-place-menu-select-competence-area night-mode-form-bg'>
            <SearchCompetenciesInCS
              searchValue={searchValue}
              changeSearchValue={changeSearchValue}
              searchWPValue={searchWPValue}
              changeSearchWPValue={changeSearchWPValue}
            />
            <div className='redact-criteria-signs-place-menu-select-competence-area-list night-mode-input'>
              <div className='redact-criteria-signs-place-menu-select-competence-area-list-item night-mode-input'>
                {
                  criteriaSignsState.competenciesListForCS.map((el, i) => {
                    if (el.name.indexOf(searchValue) == -1) {
                      return;
                    }
                    if (!criteriaSignsState.wpCompetenceConnectsCS.includes(el.id)) {
                      return;
                    }
                    return (
                      <ItemCompetenceInCS
                        el={el}
                        key={el.id}
                        openRedactCriteriaForm={openRedactCriteriaForm}
                      />
                    );
                  })
                }
              </div>
            </div>
          </div>
        </div>
        <div className='redact-criteria-signs-place-menu-change-signs'>
          <div className='redact-criteria-signs-place-menu-change-signs-area night-mode-form-bg'>
            {
              criteriaSignsState.redactCompetenceMode ? (
                <ChangeCriteriaForms
                  openAddCriteriaForm={openAddCriteriaForm}
                  openRedactCriteriaForm={openRedactCriteriaForm}
                  openDeleteCriteriaForm={openDeleteCriteriaForm}
                />
              ) : (
                  <div className='night-mode-text'>Что бы приступить к редактированию, выберите компетенцию в списке компетенций.</div>
                )
            }
          </div>
        </div>
      </div>
      <AddRedactCriteriaForm
        refForm={formAddCriteria}
        modeAdd={true}
      />
      <AddRedactCriteriaForm
        refForm={formRedactCriteria}
        modeAdd={false}
        redactID={redactCriterionID}
      />
      <DeleteCriterionForm
        deleteId={deleteId}
        refForm={formDeleteCriteria}
      />
    </div>
  );
});

export default RedactCriteriaSigns;