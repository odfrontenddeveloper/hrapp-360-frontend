import React from "react";
import { criteriaSignsState } from "../appstate/criteriasignsState.js";

const SignRangeInput = (props) => {

  let calculateSum = () => {
    let fields = Object.keys(criteriaSignsState.rangesRedact).map((el) => {
      return criteriaSignsState.rangesRedact[el].value;
    });
    if (fields.length > 0) {
      return fields.reduce((element1, element2) => {
        return element1 + element2;
      });
    }
    return 0;
  }

  return (
    <div className={`item-sign-block`}>
      <div className='item-sign-block-text'>
        <textarea
          className='form-container-textarea form-container-textarea-fixed-width night-mode-text night-mode-input'
          value={props.signname}
          onChange={(e) => { props.changeSignInput(e.target.value, props.keyItem) }}
        >
        </textarea>
      </div>
      <div className='item-sign-block-range'>
        <input
          type='range'
          value={props.signvalue ? props.signvalue : 0}
          max={100}
          className={`standart-range-input ${props.modeAdd ? (
            props.sum != 100 && props.typecheckbox ? '' : 'green-thumb'
          ) : (
              calculateSum() != 100 && criteriaSignsState.typeCheckboxRedact ? '' : 'green-thumb'
            )}`}
          onChange={(e) => { props.changeRange(e.target.value, props.keyItem) }}
          style={{
            backgroundImage: `linear-gradient(
              ${props.modeAdd ? (props.sum != 100 && props.typecheckbox ? (
                `rgba(255, 217, 0, 0.5), rgba(255, 217, 0, 0.5), rgba(255, 217, 0, 0.5)`
              ) : (
                  `rgba(0, 255, 0, 0.3), rgba(0, 255, 0, 0.3), rgba(0, 255, 0, 0.3)`
                )) : (calculateSum() != 100 && criteriaSignsState.typeCheckboxRedact ? (
                  `rgba(255, 217, 0, 0.5), rgba(255, 217, 0, 0.5), rgba(255, 217, 0, 0.5)`
                ) : (
                    `rgba(0, 255, 0, 0.3), rgba(0, 255, 0, 0.3), rgba(0, 255, 0, 0.3)`
                  ))})`,
            backgroundSize: `${props.modeAdd ? (
              props.typecheckbox ? (Number(props.signvalue) + (100 - props.sum)) : 100
            ) : (
                criteriaSignsState.typeCheckboxRedact ? (Number(props.signvalue) + (100 - (calculateSum()))) : 100
              )}% 100%`,
            backgroundRepeat: `no-repeat`
          }}
        />
        <div className='input-container-nums night-mode-text'>{`${props.signvalue}%`}</div>
      </div>
      <div className='item-sign-block-button'>
        <button
          type='button'
          className='mini-functional-button mini-danger-button mini-delete-button'
          onClick={() => { props.deleteSign(props.keyItem) }}
        ></button>
      </div>
    </div>
  );
}

export default SignRangeInput;