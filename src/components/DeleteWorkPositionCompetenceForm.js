import React, { useState } from "react";
import RedactData from "./RedactData";
import { stylesConfig, imageLoadOperations } from "../appstate/styles_config";
import axios from "axios";
import globalConfig from "../appstate/global_config";
import { wpCompetenciesState, wpcstateOperations } from "../appstate/wpCompetenciesState";
import { ugstate, ugstateOperations } from "../appstate/usersGroupsState";
import listsofdata from "../appstate/listsofdata";

const DeleteWorkPositionCompetenceForm = props => {
  let refDeleteButton = React.createRef();
  let [resDelete, changeresDelete] = useState(null);

  let closeDeleteItemForm = () => {
    // props.refForm.current.style.animation = stylesConfig.animationHideElement;
    // setTimeout(() => {
      // if (props.refForm.current) {
        // props.refForm.current.style.animation = '';
        props.refForm.current.style.display = 'none';
      // }
    // }, stylesConfig.hideElementsTiming - 10);
  }

  let deleteHrefCollection = {
    'wp': '/deletewp',
    'competence': '/deletecompetence',
    'ug': '/deleteug'
  };

  let handlerDeleteForm = (e) => {
    imageLoadOperations.openImageLoad();
    e.preventDefault();
    let data = {
      id: refDeleteButton.current.value
    };
    axios({
      method: 'delete',
      url: globalConfig.serverAddress + deleteHrefCollection[props.field],
      data: data
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          changeresDelete(null);
          closeDeleteItemForm();
          if (props.field == 'wp') {
            if (wpCompetenciesState.redactCompetenciesListForWP && wpCompetenciesState.redactWPId) {
              if (data.id == wpCompetenciesState.redactWPId) {
                wpcstateOperations.disableState();
              }
            }
            listsofdata.listOfWP = listsofdata.listOfWP.filter((el, i) => {
              return el.id != data.id;
            });
          }
          else if (props.field == 'competence') {
            listsofdata.listOfCompetencies = listsofdata.listOfCompetencies.filter((el, i) => {
              return el.id != data.id;
            });
          }
          else if (props.field == 'ug') {
            if (ugstate.redactUGMode && ugstate.redactUGID) {
              if (data.id == ugstate.redactUGID) {
                ugstateOperations.disableState();
              }
            }
            ugstate.listOfUG = ugstate.listOfUG.filter((el, i) => {
              return el.id != data.id;
            });
          }
        }
        else {
          changeresDelete(response.data.content);
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  }

  let onCloseForm = () => {
    changeresDelete(null);
  }

  return (
    <RedactData forRef={props.refForm} onCloseForm={onCloseForm}>
      <form
        className='standart-form vertical-form form-center night-mode-form-bg'
        onSubmit={(e) => { handlerDeleteForm(e) }}
      >
        <div className='form-container input-container form-container-column'>
          <label
            className='form-container-label night-mode-text'
          >
            {props.field == 'wp' ? (
              `Вы действительно хотите удалить должность "${props.deleteName}"?`
            ) : (
              props.field == 'competence' ? (
                `Вы действительно хотите удалить компетенцию "${props.deleteName}"?`
              ) : (
                `Вы действительно хотите удалить группу "${props.deleteName}"?`
              )
              )}
          </label>
        </div>
        <div className='button-container'>
          <button
            type='submit'
            className='mini-functional-button mini-success-button mini-complete-button'
            value={props.deleteId}
            ref={refDeleteButton}
          ></button>
          <button
            type='button'
            className='mini-functional-button mini-danger-button mini-close-button'
            onClick={() => closeDeleteItemForm()}
          ></button>
        </div>
      </form>
      <div className='result-container res-form-center dangerColor'>{resDelete}</div>
    </RedactData>
  );
}

export default DeleteWorkPositionCompetenceForm;