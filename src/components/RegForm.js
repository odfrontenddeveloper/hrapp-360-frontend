import React, { useState } from "react";
import axios from 'axios';
import { stylesConfig, imageLoadOperations } from "../appstate/styles_config.js";
import globalConfig from "../appstate/global_config.js";
import { userslist } from "../appstate/redactStaffState.js";
import '../appstate/ArrayPrototypeModernize';

const RegForm = (props) => {
  let refRegPasword = React.createRef();
  let refRepeatRegPassword = React.createRef();

  let [regLogin, changeregLogin] = useState('');
  let [regName, changeregName] = useState('');
  let [regSurename, changeregSurename] = useState('');
  let [regPatronymic, changeregPatronymic] = useState('');
  let [regPassword, changeregPassword] = useState('');
  let [repeatRegPassword, changerepeatRegPassword] = useState('');
  let [showPassword, changeshowPassword] = useState(false);

  let [resregLogin, changeresregLogin] = useState(null);
  let [resregName, changeresregName] = useState(null);
  let [resregSurename, changeresregSurename] = useState(null);
  let [resregPatronymic, changeresregPatronymic] = useState(null);
  let [resregPassword, changeresregPassword] = useState(null);
  let [resrepeatRegPassword, changeresrepeatRegPassword] = useState(null);
  let [ressamePasswords, changeressamePasswords] = useState(null);
  let [resReg, changeresReg] = useState(null);

  let [formResColor, changeformResColor] = useState(null);

  let handlerLoginForm = (event) => {
    event.preventDefault();

    let createByAdmin = {};

    if (props.addUserByAdmin) {
      createByAdmin.createbyadmin = true;
    }

    let userAutData = {
      login: regLogin,
      name: regName,
      surename: regSurename,
      patronymic: regPatronymic,
      password: regPassword,
      repeatpassword: repeatRegPassword,
      ...createByAdmin
    };

    imageLoadOperations.openImageLoad();
    changeresReg(null);
    axios({
      method: 'post',
      url: globalConfig.serverAddress + (!props.addUserByAdmin ? '/reguser' : '/adduser'),
      data: userAutData
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        changeresregLogin(response.data.login.join(''));
        changeresregName(response.data.name.join(''));
        changeresregSurename(response.data.surename.join(''));
        changeresregPatronymic(response.data.patronymic.join(''));
        changeresregPassword(response.data.password.join(''));
        changeresrepeatRegPassword(response.data.repeatpassword.join(''));
        changeressamePasswords(response.data.samepasswords.join(''));
        if (response.data.status == true) {
          if (props.addUserByAdmin) {
            userslist.list.push(response.data.total);
          }
          changeformResColor(true);
          changeresReg('Аккаунт создан.');
        }
        else {
          changeformResColor(false);
          changeresReg(response.data.total);
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  }

  let samePasswords = () => {
    let p1 = regPassword;
    let p2 = repeatRegPassword;
    changeressamePasswords(null);
    if (p1 != '' && p2 != '') {
      if (p1 != p2) {
        changeressamePasswords('Пароли не совпадают.');
      }
      else {
        refRegPasword.current.style.boxShadow = '';
        refRepeatRegPassword.current.style.boxShadow = '';
        changeressamePasswords(null);
      }
    }
  }

  let validateForm = (event) => {
    event.target.style.boxShadow = 'none';
    let errorsList = [];

    let validateName = (field) => {
      if (event.target.value.length > 30) {
        errorsList.push(field + ' может состоять не более чем из 30 символов.');
      }
      if (!event.target.value.match(/^[а-яА-ЯёЁіІїЇєЄa-zA-Z0-9_]+$/)) {
        errorsList.push(field + ' может состоять из букв латинского, русского и украинского алфавитов, цифр и знака подчёркивания.');
      }
      if (errorsList.length > 0) {
        event.target.style.boxShadow = '0px 0px 0px 1px ' + stylesConfig.dangerColor;
      }
    }

    switch (event.target.id) {
      case 'regLogin':
        changeresregLogin(null);
        if (event.target.value.length != 0) {
          if (event.target.value.length < 6) {
            errorsList.push('Логин должен состоять не менее чем из 6 символов.');
          }
          if (event.target.value.length > 30) {
            errorsList.push('Логин должен состоять не более чем из 30 символов.');
          }
          if (!event.target.value.match(/^[A-Za-z0-9_]+$/)) {
            errorsList.push('Логин может состоять из букв латинского алфавита, цифр и знака подчёркивания.');
          }
          if (errorsList.length > 0) {
            event.target.style.boxShadow = '0px 0px 0px 1px ' + stylesConfig.dangerColor;
          }
          changeresregLogin(errorsList.join('\n'));
        }
        break;
      case 'regName':
        changeresregName(null);
        if (event.target.value.length != 0) {
          validateName('Имя');
          changeresregName(errorsList.join('\n'));
        }
        break;
      case 'regSurename':
        changeresregSurename(null);
        if (event.target.value.length != 0) {
          validateName('Фамилия');
          changeresregSurename(errorsList.join('\n'));
        }
        break;
      case 'regPatronymic':
        changeresregPatronymic(null);
        if (event.target.value.length != 0) {
          validateName('Отчество');
          changeresregPatronymic(errorsList.join('\n'));
        }
        break;
      case 'regPassword':
        changeresregPassword(null);
        if (event.target.value.length != 0) {
          if (event.target.value.length < 6) {
            errorsList.push('Пароль должен состоять не менее чем из 6 символов.');
          }
          if (event.target.value.length > 30) {
            errorsList.push('Пароль должен состоять не более чем из 30 символов.');
          }
          if (!event.target.value.match(/^[A-Za-z0-9_]+$/)) {
            errorsList.push('Пароль может состоять из букв латинского алфавита, цифр и знака подчёркивания.');
          }
          if (errorsList.length > 0) {
            event.target.style.boxShadow = '0px 0px 0px 1px ' + stylesConfig.dangerColor;
          }
          let copyArrErrors = [...errorsList];
          copyArrErrors.remove('Пароли не совпадают.');
          changeresregPassword(copyArrErrors.join('\n'));
          samePasswords();
        }
        break;
      case 'repeatRegPassword':
        changeresrepeatRegPassword(null);
        if (event.target.value.length != 0) {
          if (event.target.value.length < 6) {
            errorsList.push('Пароль должен состоять не менее чем из 6 символов.');
          }
          if (event.target.value.length > 30) {
            errorsList.push('Пароль должен состоять не более чем из 30 символов.');
          }
          if (!event.target.value.match(/^[A-Za-z0-9_]+$/)) {
            errorsList.push('Пароль может состоять из букв латинского алфавита, цифр и знака подчёркивания.');
          }
          if (errorsList.length > 0) {
            event.target.style.boxShadow = '0px 0px 0px 1px ' + stylesConfig.dangerColor;
          }
          let copyArrErrors = [...errorsList];
          copyArrErrors.remove('Пароли не совпадают.');
          changeresrepeatRegPassword(copyArrErrors.join('\n'));
          samePasswords();
        }
        break;
    }
  }

  let handlerBlurInput = (event) => {
    validateForm(event);
  }

  let handlerChangeInput = (event) => {
    if (event.target.type == 'text' || event.target.type == 'password') {
      if (event.target.style.boxShadow == stylesConfig.dangerColor + ' 0px 0px 0px 1px') {
        validateForm(event);
      }
    }
    if (event.target.id == 'repeatRegPassword' || event.target.id == 'regPassword') {
      samePasswords();
    }
  }

  let changePasswordInput = () => {
    changeshowPassword(!showPassword);
  }

  return (
    <div>
      <form className='standart-form form-center vertical-form reg-aut-user-form night-mode-form-bg' onSubmit={(e) => { handlerLoginForm(e) }} ref={props.forRef}>
        {
          props.addUserByAdmin ? (
            false
          ) : (
              <div className='button-container'>
                <button type='button' className='href-button' onClick={() => { props.changeForm() }} ref={props.forButton}>У меня есть учётная запись</button>
              </div>
            )
        }
        <div className='form-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='regLogin'>Логин:</label>
          <input id='regLogin' className='form-container-input night-mode-text night-mode-input' type='text' value={regLogin} onChange={(e) => { changeregLogin(e.target.value); handlerChangeInput(e) }} onBlur={(e) => { handlerBlurInput(e) }} />
        </div>
        <div className='form-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='regName'>Имя:</label>
          <input id='regName' className='form-container-input night-mode-text night-mode-input' type='text' value={regName} onChange={(e) => { changeregName(e.target.value); handlerChangeInput(e) }} onBlur={(e) => { handlerBlurInput(e) }} />
        </div>
        <div className='form-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='regSurename'>Фамилия:</label>
          <input id='regSurename' className='form-container-input night-mode-text night-mode-input' type='text' value={regSurename} onChange={(e) => { changeregSurename(e.target.value); handlerChangeInput(e) }} onBlur={(e) => { handlerBlurInput(e) }} />
        </div>
        <div className='form-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='regPatronymic'>Отчество:</label>
          <input id='regPatronymic' className='form-container-input night-mode-text night-mode-input' type='text' value={regPatronymic} onChange={(e) => { changeregPatronymic(e.target.value); handlerChangeInput(e) }} onBlur={(e) => { handlerBlurInput(e) }} />
        </div>
        <div className='form-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='regPassword'>Пароль:</label>
          <input id='regPassword' className='form-container-input night-mode-text night-mode-input' type={showPassword ? 'text' : 'password'} value={regPassword} onChange={(e) => { changeregPassword(e.target.value); handlerChangeInput(e) }} onBlur={(e) => { handlerBlurInput(e) }} />
        </div>
        <div className='form-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='repeatRegPassword'>Пароль:</label>
          <input id='repeatRegPassword' className='form-container-input night-mode-text night-mode-input' type={showPassword ? 'text' : 'password'} value={repeatRegPassword} onChange={(e) => { changerepeatRegPassword(e.target.value); handlerChangeInput(e) }} onBlur={(e) => { handlerBlurInput(e) }} />
        </div>
        <div className='button-container'>
          <button type='submit' className='functional-button success-button log-button'></button>
          <button type='button' className='functional-button warning-button eye-button' onClick={() => { changePasswordInput() }}></button>
        </div>
      </form>
      <div className='result-container form-center'>
        <div id='result-input-login-reg' className='dangerColor'>{resregLogin}</div>
        <div id='result-input-name-reg' className='dangerColor'>{resregName}</div>
        <div id='result-input-surename-reg' className='dangerColor'>{resregSurename}</div>
        <div id='result-input-patronymic-reg' className='dangerColor'>{resregPatronymic}</div>
        <div id='result-input-password-reg' className='dangerColor' ref={refRegPasword}>{resregPassword}</div>
        <div id='result-input-repeat-password-reg' className='dangerColor' ref={refRepeatRegPassword}>{resrepeatRegPassword}</div>
        <div id='result-input-same-passwords-reg' className='dangerColor'>{ressamePasswords}</div>
        <div id='result-reg' className={`${formResColor ? 'successColor' : 'dangerColor'}`}>{resReg}</div>
      </div>
    </div>
  );
}

export default RegForm;