import React, { useState, useEffect } from "react";
import { observer } from "mobx-react";
import ItemAssessment from "./ItemAssessment.js";
import SearchWPCompetenceForm from "./SearchWPCompetenceForm.js";
import RedactNameAssessment from "./RedactNameAssessment.js";
import { resultsState } from "../appstate/showResultsState.js";
import DeleteAssessmentForm from "./DeleteAssessmentForm.js";

const ListOfAssessments = observer((props) => {
  let formDeleteAssessment = React.createRef();
  let formRenameAssessment = React.createRef();
  let refRedactName = React.createRef();

  let counterDisplayItemsInList = 0;

  let [deleteIdAssessment, changedeleteIdAssessment] = useState('');
  let [deleteNameAssessment, changedeleteNameAssessment] = useState('');
  let [redactIdAssessment, changeredactIdAssessment] = useState('');
  let [redactNameAssessment, changeredactNameAssessment] = useState('');
  let [counterDisplayItems, changecounterDisplayItems] = useState(30);

  let setBaseCounterDisplayItems = () => {
    changecounterDisplayItems(30);
  }

  let incrementCounterDisplayItems = () => {
    changecounterDisplayItems(counterDisplayItems + 30);
  }

  let openDeleteAssessmentForm = (id, name) => {
    formDeleteAssessment.current.style.display = 'flex';
    changedeleteIdAssessment(id);
    changedeleteNameAssessment(name);
  }

  let openRenameAssessmentForm = (id, name) => {
    formRenameAssessment.current.style.display = 'flex';
    refRedactName.current.value = name;
    changeredactIdAssessment(id);
    changeredactNameAssessment(name);
  }

  return (
    <div className='work-positions-area-col work-positions-area-workpositions night-mode-form-bg'>
      <div className='work-positions-area-col-header night-mode-text'>Список мероприятий</div>
      <SearchWPCompetenceForm
        setBaseCounter={setBaseCounterDisplayItems}
        field={'assessment'}
      />
      <div className='work-positions-area-col-list night-mode-input'>
        <div
          id='list-of-work-positions'
          className='list-of-items'
        >
          {
            resultsState.assessmentsList.length > 0 ? (
              resultsState.assessmentsList.map((el, i) => {
                if (counterDisplayItemsInList == counterDisplayItems) {
                  counterDisplayItemsInList += 1;
                  return (
                    <div>
                      <button
                        key={'show-more-items-button'}
                        className='mini-functional-button mini-fiolet-button mini-more-button'
                        onClick={() => { incrementCounterDisplayItems() }}
                      ></button>
                    </div>
                  );
                }
                if (counterDisplayItemsInList > counterDisplayItems) {
                  return;
                }
                counterDisplayItemsInList += 1;
                return (
                  <ItemAssessment
                    key={'item-assessment-' + el.id}
                    counterIterator={i}
                    assessmentInfo={el}
                    onDelete={openDeleteAssessmentForm}
                    onRename={openRenameAssessmentForm}
                    refDiagram={props.refDiagram}
                  />
                );
              })
            ) : (
                <div className='null-list-message night-mode-text'>Ничего не найдено.</div>
              )
          }
        </div>
      </div>
      <RedactNameAssessment
        refForm={formRenameAssessment}
        redactId={redactIdAssessment}
        redactName={redactNameAssessment}
        refInputName={refRedactName}
      />
      <DeleteAssessmentForm
        refForm={formDeleteAssessment}
        deleteId={deleteIdAssessment}
        deleteName={deleteNameAssessment}
      />
    </div>
  );
});

export default ListOfAssessments;