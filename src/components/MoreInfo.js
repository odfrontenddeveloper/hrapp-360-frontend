import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import { resultsState } from "../appstate/showResultsState";
import Chart from 'Chart.js';
import shortid from "shortid";
import { stylesConfig } from "../appstate/styles_config";

const MoreInfo = observer((props) => {

  let myChart = null;
  let refDiagram = React.createRef();
  let [wp, changeWP] = useState('');
  let [chartType, changechartType] = useState(0);

  useEffect(() => {
    if (resultsState.showResMode == true && resultsState.resultsList.length != 0) {

      let selectedResult = resultsState.resultsList.filter((element) => {
        return element.USER_TO.id == resultsState.resultid_user_to && resultsState.selectedUsersForCheckRes.filter(el => el.user_to.id == resultsState.resultid_user_to).filter(el => el.selectedForDisplayResults).map(el => el.user_from.id).includes(element.USER_FROM.id);
      });

      if (selectedResult.length > 0) {
        changeWP(selectedResult[0].WP.name);

        let competencies = selectedResult[0].COMPETENCIES.map(element => {
          return element.COMPETENCE.name;
        });

        let criteria = selectedResult.map(el0 => {
          return el0.COMPETENCIES.map(element => {
            return element.CRITERIA.map(el => {
              if (el.SIGNS.filter(el1 => el1.selected).length > 0) {
                return el.SIGNS.filter(el1 => el1.selected).map(el1 => el1.weight).reduce((pr, nx) => pr + nx);
              }
              else {
                return 0;
              }
            }).map(el2 => el2);
          }).map(element => {
            return element.reduce((pr, nx) => {
              return Number(pr) + Number(nx);
            }, 0) / element.length;
          });
        });

        let calculatedValue = criteria[0].map((element, i) => {
          return criteria.map((el, j) => {
            return criteria[j][i];
          });
        }).map(value => {
          return value.reduce((previous, next) => {
            return previous + next;
          }) / value.length;
        });

        let mainData = [{
          label: 1,
          data: calculatedValue,
          backgroundColor: Array(competencies.length).fill('rgba(3, 132, 252, 0.25)'),
          borderColor: Array(competencies.length).fill('rgba(3, 132, 252, 1)'),
          borderWidth: 1
        }];

        let colorsForDisplayInfo = [
          '3, 132, 252',
          '255, 196, 0',
          '0, 255, 38',
          '151, 66, 255',
          '255, 89, 0'
        ];

        let apartData = criteria.map((criterion, i) => {
          return {
            label: '',
            data: criterion,
            backgroundColor: Array(competencies.length).fill(`rgba(${colorsForDisplayInfo[i % 5]}, 0.25)`),
            borderColor: Array(competencies.length).fill(`rgba(${colorsForDisplayInfo[i % 5]}, 1)`),
            borderWidth: 1
          }
        });

        var ctx = refDiagram.current;
        myChart = new Chart(ctx, {
          type: 'radar',
          data: {
            labels: competencies,
            datasets: chartType == 1 ? apartData : mainData,
          },
          options: {
            scale: {
              ticks: {
                beginAtZero: true,
                min: 0,
                max: 100,
                stepSize: 20
              },
              pointLabels: {
                fontSize: 12.5
              }
            },
            animation: false
          }
        });
      }
    }
    return () => {
      if (myChart) {
        myChart.destroy();
      }
    };
  });

  return (
    <div className={`staff-assessment-place-punkts${resultsState.selectedPunkt == 2 ? '' : ' display-none'}`}>
      <div className='work-positions-area'>
        <div className='redact-criteria-signs-place-menu-select-competence'>
          <div className='redact-criteria-signs-place-menu-select-competence-area night-mode-form-bg'>
            <select value={chartType} onChange={(e) => { changechartType(e.target.value) }} className='form-container-select form-container-column w-100 night-mode-text night-mode-input'>
              <option value={0}>Общий средний результат</option>
              <option value={1}>Вывод результатов отдельно</option>
            </select>
            <div className='work-positions-area-col-header night-mode-text'>Список оценщиков</div>
            <div className='redact-criteria-signs-place-menu-select-competence-area-list night-mode-input'>
              <div className='redact-criteria-signs-place-menu-select-competence-area-list-item night-mode-input'>
                {
                  resultsState.selectedUsersForCheckRes.filter(user => {
                    return user.user_to.id == resultsState.resultid_user_to;
                  }).map(user => {
                    return (
                      <div className='item-in-list' key={user.user_from.id + shortid.generate()}>
                        <div className='item-in-list-data-block night-mode-text'>
                          {`${user.user_from.surename} ${user.user_from.name} ${user.user_from.patronymic}`}
                        </div>
                        <div className='item-in-list-buttons-container'>
                          <div className='item-in-list-buttons-container-radio-block'>
                            <input
                              type='checkbox'
                              id={`user-${user.user_from.id}`}
                              className='item-in-list-buttons-container-radio-block-input'
                              checked={user.selectedForDisplayResults}
                              onChange={(e) => { user.selectedForDisplayResults = e.target.checked }}
                            />
                            <label className='item-in-list-buttons-container-radio-block-label night-mode-form-bg' htmlFor={`user-${user.user_from.id}`}></label>
                          </div>
                        </div>
                      </div>
                    );
                  })
                }
              </div>
            </div>
          </div>
        </div>
        <div className='redact-criteria-signs-place-menu-change-signs'>
          <div className='redact-criteria-signs-place-menu-change-signs-area night-mode-form-bg'>
            <div className='main-result-block'>
              <div className='form-container-label night-mode-text'>{`${resultsState.resultsList.filter(info => info.USER_TO.id == resultsState.resultid_user_to).map(info => info.USER_TO.surename + ' ' + info.USER_TO.name + ' ' + info.USER_TO.patronymic)[0]}`}</div>
              <div className='form-container-label night-mode-text'>{wp}</div>
              <div className='night-mode-form-bg chart-container' style={{ overflowY: 'auto' }}>
                {
                  resultsState.showResMode == true ? (
                    <div className={`night-mode-form-bg ${stylesConfig.nightMode ? 'chart-container-block' : 'chart-container-block'}`}>
                      <canvas id='myChart' ref={refDiagram}></canvas>
                    </div>
                  ) : (
                      null
                    )
                }
              </div>
              <div className='result-as-table'>
                {resultsState.resultsList.filter((element) => {
                  return element.USER_TO.id == resultsState.resultid_user_to && resultsState.selectedUsersForCheckRes.filter(el => el.user_to.id == resultsState.resultid_user_to).filter(el => el.selectedForDisplayResults).map(el => el.user_from.id).includes(element.USER_FROM.id);
                }).length > 0 ? (
                    resultsState.resultsList.filter((element) => {
                      return element.USER_TO.id == resultsState.resultid_user_to && resultsState.selectedUsersForCheckRes.filter(el => el.user_to.id == resultsState.resultid_user_to).filter(el => el.selectedForDisplayResults).map(el => el.user_from.id).includes(element.USER_FROM.id);
                    })[0].COMPETENCIES.map(element => {
                      return (
                        <div className='result-as-table-block'>
                          <div className='result-as-table-block-header night-mode-text'>{element.COMPETENCE.name}</div>
                          <div className='result-as-table-block-rows-block'>
                            <table className='result-as-table-block-rows-block-criteria'>
                              <tbody>
                                <tr>
                                  <td style={{ width: '100%' }}></td>
                                  {
                                    resultsState.selectedUsersForCheckRes.filter(user => {
                                      return user.user_to.id == resultsState.resultid_user_to;
                                    }).map(user => {
                                      return (
                                        <td className='night-mode-text' key={`${user.user_from.id}-${user.user_to.id}`}>
                                          {`${user.user_from.surename} ${user.user_from.name.split('')[0] + '.'} ${user.user_from.patronymic.split('')[0] + '.'}`}
                                        </td>
                                      );
                                    })
                                  }
                                </tr>
                                {
                                  element.CRITERIA.map(criterion => {
                                    return (
                                      <tr className='result-as-table-block-rows-block-criteria-row'>
                                        {
                                          <React.Fragment>
                                            <td className='night-mode-text'>{criterion.CRITERION.text}</td>
                                            {
                                              resultsState.selectedUsersForCheckRes.filter(user => {
                                                return user.user_to.id == resultsState.resultid_user_to;
                                              }).map(user => {
                                                return (
                                                  resultsState.resultsList.filter((element) => {
                                                    return element.USER_TO.id == user.user_to.id && element.USER_FROM.id == user.user_from.id;
                                                  }).map((el, i) => {
                                                    return (
                                                      <td className='night-mode-text'>
                                                        {
                                                          el.COMPETENCIES.map(competence => {
                                                            return competence.CRITERIA.filter(criterion_info => {
                                                              return criterion.CRITERION.id == criterion_info.CRITERION.id;
                                                            }).map(criterion_info => {
                                                              return criterion_info.SIGNS.filter(sign => {
                                                                return sign.selected;
                                                              }).map(sign => {
                                                                return sign.weight;
                                                              });
                                                            });
                                                          }).filter(data => {
                                                            return data.length > 0;
                                                          })[0][0].length > 0 ? (
                                                              `${el.COMPETENCIES.map(competence => {
                                                                return competence.CRITERIA.filter(criterion_info => {
                                                                  return criterion.CRITERION.id == criterion_info.CRITERION.id;
                                                                }).map(criterion_info => {
                                                                  return criterion_info.SIGNS.filter(sign => {
                                                                    return sign.selected;
                                                                  }).map(sign => {
                                                                    return sign.weight;
                                                                  });
                                                                });
                                                              }).filter(data => {
                                                                return data.length > 0;
                                                              })[0][0].reduce((prev, next) => prev + next)}%`
                                                            ) : (
                                                              `0%`
                                                            )
                                                        }
                                                      </td>
                                                    );
                                                  })
                                                );
                                              })
                                            }
                                          </React.Fragment>
                                        }
                                      </tr>
                                    );
                                  })
                                }
                              </tbody>
                            </table>
                          </div>
                        </div>
                      );
                    })
                  ) : (null)
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

export default MoreInfo;