import React, { useState } from "react";
import { observer } from "mobx-react";
import { fillingAFstate, fillingAFstateOperations } from "../appstate/fillingassessmentsformsState";
import shortid from "shortid";

const FillingAssessmentForm = observer(() => {
  let [resSetResults, changeResSetResults] = useState('');

  return (
    <form className={`staff-assessment-place-punkts${fillingAFstate.selectedPunkt == 2 ? '' : ' display-none'}`} onSubmit={(e) => { fillingAFstateOperations.sendResultsForm(e, changeResSetResults) }}>
      {
        fillingAFstate.assessmentFormInfo.WP && fillingAFstate.selectedInvite != null ? (
          <div className='fixed-functional-buttons-block-buttons'>
            <button type='submit' className='mini-functional-button mini-add-button mini-success-button'></button>
          </div>
        ) : (
            null
          )
      }
      {
        fillingAFstate.assessmentFormInfo.WP && fillingAFstate.selectedInvite != null ? (
          <div className='fill-assessment-form-list'>
            <div className='fill-assessment-form-list-container'>
              <div className='item-criterion night-mode-form-bg dangerColor'>{resSetResults}</div>
              <div className='item-criterion night-mode-form-bg night-mode-text'>{fillingAFstate.assessmentFormInfo.WP.name}</div>
              {
                fillingAFstate.assessmentFormInfo.COMPETENCIES.map(competence => {
                  return (
                    <div className='item-criterion night-mode-form-bg' key={`${competence.COMPETENCE.id}${shortid.generate()}`}>
                      <div className='item-criterion-criterion-text night-mode-text fill-assessment-form-list-container-competence-container'>
                        {
                          competence.COMPETENCE.name
                        }
                      </div>
                      <div>
                        {
                          competence.CRITERIA.map(criterion_info => {
                            return (
                              <div className='fill-assessment-form-list-container-criteria-sign-container' key={`${criterion_info.CRITERION.id}${shortid.generate()}`}>
                                <div className='item-criterion-criterion-text item-criterion-criterion-text-bg night-mode-text fill-assessment-form-list-container-criteria-container'>
                                  {
                                    criterion_info.CRITERION.text
                                  }
                                </div>
                                <div className='item-criterion-criterion-text night-mode-text fill-assessment-form-list-container-competence-container'>{criterion_info.CRITERION.typecheckbox ? 'Выберите несколько вариантов ответа.' : 'Выберите один вариант ответа'}</div>
                                <div className='item-criterion-signs night-mode-text'>
                                  {
                                    criterion_info.SIGNS.map(sign_info => {
                                      return (
                                        <div className='item-criterion-signs-sign' key={`${sign_info.id}${shortid.generate()}`}>
                                          <div className='item-in-list-buttons-container'>
                                            <div className='item-in-list-buttons-container-radio-block'>
                                              <input
                                                id={'sign-' + sign_info.id}
                                                type={criterion_info.CRITERION.typecheckbox ? 'checkbox' : 'radio'} name={criterion_info.CRITERION.id}
                                                className='item-in-list-buttons-container-radio-block-input'
                                                name={criterion_info.CRITERION.id}
                                                checked={sign_info.selected ? true : false}
                                                onChange={(e) => {
                                                  fillingAFstateOperations.changeSelectedSign(criterion_info, sign_info, e.target);
                                                }}
                                              />
                                              <label className='item-in-list-buttons-container-radio-block-label night-mode-form-bg' htmlFor={'sign-' + sign_info.id}></label>
                                            </div>
                                          </div>
                                          <div className='item-criterion-signs-sign-text night-mode-text fill-assessment-form-list-container-sign-container'>
                                            {
                                              sign_info.text
                                            }
                                          </div>
                                        </div>
                                      );
                                    })
                                  }
                                </div>
                              </div>
                            );
                          })
                        }
                      </div>
                    </div>
                  );
                })
              }
            </div>
          </div>
        ) : (
            <div className='item-criterion night-mode-form-bg night-mode-text'>Выберите приглашение</div>
          )
      }
    </form>
  );
});

export default FillingAssessmentForm;