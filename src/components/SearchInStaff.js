import React from "react";
import { observer } from "mobx-react";

const SearchInStaff = observer(props => {

  let handlerSearchForm = (e) => {    
    e.preventDefault();
  }

  return (
    <form
      className='standart-form horizontal-form reg-aut-user-form night-mode-form-bg'
      onSubmit={(e) => { handlerSearchForm(e) }}
      ref={props.forRef}
    >
      <div className='form-container input-container form-container-column'>
        <label
          className='form-container-label night-mode-text'
          htmlFor='searchByFullname'
        >
          Фамилия / Имя / Отчество
          </label>
        <input
          id='searchByFullname'
          className='form-container-input night-mode-text night-mode-input'
          type='text'
          value={props.fullname}
          onChange={(e) => { props.changeFullname(e.target.value) }}
          placeholder={`Начните вводить данные...`}
        />
      </div>
    </form>
  );
});

export default SearchInStaff;