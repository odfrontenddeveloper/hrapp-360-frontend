import React from "react";
import { observer } from "mobx-react";
import { resultsState } from "../appstate/showResultsState";
import ListOfAssessments from "./ListOfAssessments";
import UsersListForResults from "./UsersListForResults";

const AssessmentsList = observer((props) => {
  return (
    <div className={`staff-assessment-place-punkts${resultsState.selectedPunkt == 1 ? '' : ' display-none'}`}>
      <div className='work-positions-area'>
        <ListOfAssessments />
        <UsersListForResults/>
      </div>
    </div>
  );
});

export default AssessmentsList;