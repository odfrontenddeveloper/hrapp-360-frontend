import React from "react";
import axios from 'axios';
import { imageLoadOperations } from "../appstate/styles_config.js";
import globalConfig from "../appstate/global_config.js";
import RedactData from "./RedactData.js";

const ChangeSecurityInfo = (props) => {
  let blockres = React.createRef();
  let oldPassword = React.createRef();
  let newPassword = React.createRef();
  let newPasswordRepeat = React.createRef();

  let changePassword = (e) => {
    e.preventDefault();
    imageLoadOperations.openImageLoad();
    axios({
      method: 'patch',
      url: globalConfig.serverAddress + '/changeuserpassword',
      data: {
        oldPassword: oldPassword,
        newPassword: newPassword,
        newPasswordRepeat: newPasswordRepeat
      }
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      blockres.current.className = 'successColor';
      if (!response.data.status) {
        blockres.current.className = 'dangerColor';
      }
      blockres.current.innerHTML = response.data.content;
    }).catch(error => {
      console.log(error);
      imageLoadOperations.closeImageLoad();
    });
  }

  let changePasswordInput = () => {
    oldPassword.current.type = oldPassword.current.type == 'text' ? 'password' : 'text';
    newPassword.current.type = newPassword.current.type == 'text' ? 'password' : 'text';
    newPasswordRepeat.current.type = newPasswordRepeat.current.type == 'text' ? 'password' : 'text';
  }

  return (
    <RedactData forRef={props.forRef}>
      <form className='standart-form vertical-form redact-user-data-block night-mode-form-bg' onSubmit={(e) => { changePassword(e) }}>
        <div className='form-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='oldPassword'>Старый пароль:</label>
          <input
            id='oldPassword'
            ref={oldPassword}
            className='form-container-input night-mode-text night-mode-input'
            type='password'
          />
        </div>
        <div className='form-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='newPassword'>Новый пароль:</label>
          <input
            id='newPassword'
            ref={newPassword}
            className='form-container-input night-mode-text night-mode-input'
            type='password'
          />
        </div>
        <div className='form-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='newPasswordRepeat'>Повторите новый пароль:</label>
          <input
            id='newPasswordRepeat'
            ref={newPasswordRepeat}
            className='form-container-input night-mode-text night-mode-input'
            type='password'
          />
        </div>
        <div className='button-container'>
          <button type='submit' className='functional-button primary-button log-button'></button>
          <button type='button' className='functional-button warning-button eye-button' onClick={() => { changePasswordInput() }}></button>
        </div>
      </form>
      <div className='result-container redact-user-data-block'>
        <div id='result-upadate-password' ref={blockres}></div>
      </div>
    </RedactData>
  );
}

export default ChangeSecurityInfo;