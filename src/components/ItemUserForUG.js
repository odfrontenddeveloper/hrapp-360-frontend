import React from "react";
import { observer } from "mobx-react";
import globalConfig from "../appstate/global_config.js";
import { wpcstateOperations, wpCompetenciesState } from "../appstate/wpCompetenciesState.js";
import axios from "axios";
import { ugstate } from "../appstate/usersGroupsState.js";
import { imageLoadOperations } from "../appstate/styles_config.js";

const ItemUserForUG = observer(props => {
  let openRedactConnectionsList = () => {
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/searchconnectswpcompetence?wp=${props.wpInfo.id}`
    }).then(response => {
      if (response.status >= 200 && response.status < 300) {
        wpCompetenciesState.connectsList = response.data.content.map((el, i) => {
          return el.competence;
        });
        wpcstateOperations.changeState(props.wpInfo.id);
      }
    }).catch(error => {
      console.log(error);
    });
  }

  let changeUserConnectToGroup = (value) => {
    imageLoadOperations.openImageLoad();
    let data = {
      ug: ugstate.redactUGID,
      user: props.userInfo.id
    };
    axios({
      method: 'post',
      url: globalConfig.serverAddress + (value ? '/connectuserwithgroup' : '/disconnectuserwithgroup'),
      data: data
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          if (value == true) {
            ugstate.usersGroupsConnections.push(props.userInfo.id);
          }
          else {
            ugstate.usersGroupsConnections = ugstate.usersGroupsConnections.filter((el, i) => {
              return el != props.userInfo.id;
            });
          }
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  }

  let handlerChangeCheckBox = (e) => {
    if (e.target.checked) {
      changeUserConnectToGroup(true);
    }
    else {
      changeUserConnectToGroup(false);
    }
  }

  return (
    <div
      id={'list-of-wp-id-' + props.userInfo.id}
      className='item-in-list'
      style={{ animation: `${1 * 0.4}s showBlock forwards` }}
    >
      <div className='item-padding'>
        <div className='item-user-assessment-list-avatar'>
          <div className='item-user-assessment-list-avatar-block' style={{ backgroundImage: `url('${globalConfig.serverAddress}/useravatar?login=${props.userInfo.login}')` }}></div>
        </div>
      </div>
      <div className='item-in-list-data'>
        <div className='item-in-list-data-block night-mode-text'>{props.userInfo.surename}</div>
        <div className='item-in-list-data-block night-mode-text'>{props.userInfo.name}</div>
        <div className='item-in-list-data-block night-mode-text'>{props.userInfo.patronymic}</div>
      </div>
      <div className='item-in-list-data'>
        <div className='item-in-list-data-block night-mode-text'>{props.userInfo.wp ? props.userInfo.wp : 'Должность не указана'}</div>
      </div>
      <div className='item-padding'>
        {
          ugstate.redactUGMode ? (
            <div className='item-in-list-buttons-container-checkbox'>
              <input
                type='checkbox'
                id={'add-competence-to-wp-' + props.userInfo.id}
                className='item-in-list-buttons-container-checkbox-input'
                checked={ugstate.usersGroupsConnections.includes(props.userInfo.id)}
                onChange={(e) => { handlerChangeCheckBox(e) }}
              />
              <label
                htmlFor={'add-competence-to-wp-' + props.userInfo.id}
                className='item-in-list-buttons-container-checkbox-label night-mode-form-bg'>
              </label>
            </div>
          ) : (false)
        }
      </div>
      <div className='item-in-list-buttons-container'></div>
    </div>
  );
});

export default ItemUserForUG;