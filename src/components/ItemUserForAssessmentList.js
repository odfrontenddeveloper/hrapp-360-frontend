import React from "react";
import { assessmentConfigStateOperations } from "../appstate/assessmentConfigState";
import globalConfig from "../appstate/global_config";

const ItemUserForAssessmentList = (props) => {
  return (
    <div className={`item-user-assessment-list${props.user.wp != null ? '' : ' lightRed'}`} key={props.user.id}>
      <div className='item-user-assessment-list-avatar'>
        <div className='item-user-assessment-list-avatar-block' style={{ backgroundImage: `url('${globalConfig.serverAddress}/useravatar?login=${props.user.login}')` }}></div>
      </div>
      <div className='item-user-assessment-list-info'>
        <div className='item-user-assessment-list-info-text night-mode-text'>{props.user.surename}</div>
        <div className='item-user-assessment-list-info-text night-mode-text'>{props.user.name}</div>
        <div className='item-user-assessment-list-info-text night-mode-text'>{props.user.patronymic}</div>
      </div>
      <div className='item-user-assessment-list-info'>
        <div className='item-user-assessment-list-info-text night-mode-text'>{props.user.wp ? props.user.wp : `Укажите должность.`}</div>
      </div>
      <div className={`item-user-assessment-list-button${props.selectedUsers ? '' : ' flex-portal-to-first'}`}>
        <button
          type='button'
          className={`mini-functional-button ${props.selectedUsers ? 'mini-success-button mini-add-button' : 'mini-danger-button mini-close-button'}`}
          onClick={() => { assessmentConfigStateOperations.changeUserSelectedToAssessment(props.user.id, props.user.wp) }}
        ></button>
      </div>
    </div>
  );
}

export default ItemUserForAssessmentList;