import React, { useState, useEffect } from "react";
import axios from 'axios';
import { stylesConfig, imageLoadOperations } from "../appstate/styles_config.js";
import globalConfig from "../appstate/global_config.js";
import authOperations from "../appstate/userauthOperations.js";
import { Redirect } from "react-router-dom";
import { observer } from "mobx-react";

const LoginForm = observer((props) => {

  let [autLogin, changeautLogin] = useState('');
  let [autPassword, changeautPassword] = useState('');
  let [showPassword, changeshowPassword] = useState(false);
  let [goToAuth, changegoToAuth] = useState(false);

  let [resLogin, changeresLogin] = useState(null);
  let [resPassword, changeresPassword] = useState(null);
  let [resAuth, changeresAuth] = useState(null);

  useEffect(() => {
    authOperations.resetState();
  }, []);

  let handlerLoginForm = (event) => {
    event.preventDefault();
    let userAutData = {
      login: autLogin,
      password: autPassword
    };
    imageLoadOperations.openImageLoad();

    changeresAuth(null);
    axios({
      method: 'post',
      url: globalConfig.serverAddress + '/authuser',
      data: userAutData
    }).then(async response => {
      if (response.status >= 200 && response.status < 300) {
        changeresLogin(response.data.login.join(''));
        changeresPassword(response.data.password.join(''));
        if (response.data.status == true) {
          if (await authOperations.logIn(response.data.loginvalue, response.data.token, response.data.usertype)) {
            axios.defaults.headers.common['userLogin'] = authOperations.getCookie('userLogin');
            axios.defaults.headers.common['userToken'] = authOperations.getCookie('userToken');
            changegoToAuth(true);
          }
        }
        else {
          changeresAuth(response.data.total);
        }
      }
      imageLoadOperations.closeImageLoad();
    }).catch(error => {
      console.log(error);
      imageLoadOperations.closeImageLoad();
    });
  }

  let validateForm = (event) => {
    event.target.style.boxShadow = 'none';
    let errorsList = [];
    switch (event.target.id) {
      case 'autLogin':
        changeresLogin(null);
        if (event.target.value.length != 0) {
          if (event.target.value.length < 6) {
            errorsList.push('Логин должен состоять не менее чем из 6 символов.');
          }
          if (event.target.value.length > 30) {
            errorsList.push('Логин должен состоять не более чем из 30 символов.');
          }
          if (!event.target.value.match(/^[A-Za-z0-9_]+$/)) {
            errorsList.push('Логин может состоять из букв латинского алфавита, цифр и знака подчёркивания.');
          }
          if (errorsList.length > 0) {
            event.target.style.boxShadow = '0px 0px 0px 1px ' + stylesConfig.dangerColor;
          }
          changeresLogin(errorsList.join('\n'));
        }
        break;
      case 'autPassword':
        changeresPassword(null);
        if (event.target.value.length != 0) {
          if (event.target.value.length < 6) {
            errorsList.push('Пароль должен состоять не менее чем из 6 символов.');
          }
          if (event.target.value.length > 30) {
            errorsList.push('Пароль должен состоять не более чем из 30 символов.');
          }
          if (!event.target.value.match(/^[A-Za-z0-9_]+$/)) {
            errorsList.push('Пароль может состоять из букв латинского алфавита, цифр и знака подчёркивания.');
          }
          if (errorsList.length > 0) {
            event.target.style.boxShadow = '0px 0px 0px 1px ' + stylesConfig.dangerColor;
          }
          changeresPassword(errorsList.join('\n'));
        }
        break;
    }
  }

  let handlerBlurInput = (event) => {
    validateForm(event);
  }

  let handlerChangeInput = (event) => {
    if (event.target.style.boxShadow == stylesConfig.dangerColor + ' 0px 0px 0px 1px') {
      validateForm(event);
    }
  }

  let changePasswordInput = () => {
    changeshowPassword(!showPassword);
  }

  return (
    <div>
      <form
        className='standart-form vertical-form reg-aut-user-form night-mode-form-bg'
        onSubmit={(e) => { handlerLoginForm(e) }}
        ref={props.forRef}
      >
        {goToAuth ? (
          <Redirect to='/dashboard' />
        ) : (
            null
          )}
        <div className='button-container'>
          <button
            type='button'
            className='href-button'
            onClick={() => { props.changeForm() }}
            ref={props.forButton}>
            У меня нет учётной записи
            </button>
        </div>
        <div className='form-container form-container-column'>
          <label
            className='form-container-label night-mode-text'
            htmlFor='autLogin'
          >
            Логин:
            </label>
          <input
            type='text'
            id='autLogin'
            className='form-container-input night-mode-text night-mode-input'
            value={autLogin}
            onChange={(e) => { changeautLogin(e.target.value); handlerChangeInput(e); }}
            onBlur={(e) => { handlerBlurInput(e) }}
          />
        </div>
        <div className='form-container form-container-column'>
          <label
            className='form-container-label night-mode-text'
            htmlFor='autPassword'
          >
            Пароль:
            </label>
          <input
            type={showPassword ? 'text' : 'password'}
            id='autPassword'
            className='form-container-input night-mode-text night-mode-input'
            value={autPassword}
            onChange={(e) => { changeautPassword(e.target.value); handlerChangeInput(e); }}
            onBlur={(e) => { handlerBlurInput(e) }}
          />
        </div>
        <div className='button-container'>
          <button
            type='submit'
            className='functional-button primary-button log-button'
          ></button>
          <button
            type='button'
            className='functional-button warning-button eye-button'
            onClick={() => { changePasswordInput() }}
          ></button>
        </div>
      </form>
      <div className='result-container form-center'>
        <div id='result-input-login-auth' className='dangerColor'>{resLogin}</div>
        <div id='result-input-password-auth' className='dangerColor'>{resPassword}</div>
        <div id='result-auth'>{resAuth}</div>
      </div>
    </div>
  );
});

export default LoginForm;