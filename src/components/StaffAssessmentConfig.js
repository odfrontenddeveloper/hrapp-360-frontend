import React from "react";
import CheckAuth from "./CheckAuth";
import UserAreaHeader from "./UserAreaHeader";
import { observer } from "mobx-react";
import { assessmentConfigState, assessmentConfigStateOperations, assessmentConfigStoreOperations, assessmentConfigStore } from "../appstate/assessmentConfigState";
import SelectUsersForAssessment from "./SelectUsersForAssessment";
import FormInputNameAssessment from "./FormInputNameAssessment";
import InvitesConfigTable from "./InvitesConfigTable";
import ConfigTableMenu from "./ConfigTableMenu";
import ConfirmationAssessment from "./ConfirmationAssessment";

const StaffAssessmentConfig = observer(() => {

  let refArea = React.createRef();

  return (
    <div className={`staff-assessment-place${assessmentConfigState.selectedPunkt == 2 ? ` staff-assessment-place-redact-users-list` : ''} night-mode-bg-block`} ref={refArea}>
      <CheckAuth />
      <UserAreaHeader
        logout={refArea}
        backButton={true}
      />
      <div className='staff-assessment-place-buttons'>
        <button type='button' className='staff-assessment-place-buttons-button staff-assessment-place-buttons-button-hover night-mode-form-bg night-mode-form-bg-hover night-mode-text' onClick={() => { assessmentConfigStateOperations.changeSelectedPunkt(1) }}>1 Придумайте характерное название мероприятия</button>
        <button type='button' className='staff-assessment-place-buttons-button staff-assessment-place-buttons-button-hover night-mode-form-bg night-mode-form-bg-hover night-mode-text' onClick={() => { assessmentConfigStateOperations.changeSelectedPunkt(2) }}>2 Укажите, какие сотрудники будут принимать участие в мероприятии</button>
        <button type='button' className='staff-assessment-place-buttons-button staff-assessment-place-buttons-button-hover night-mode-form-bg night-mode-form-bg-hover night-mode-text' onClick={() => { assessmentConfigStateOperations.changeSelectedPunkt(3) }}>3 Укажите, кто кого должен оценить</button>
        <button type='button' className='staff-assessment-place-buttons-button staff-assessment-place-buttons-button-hover night-mode-form-bg night-mode-form-bg-hover night-mode-text' onClick={() => { assessmentConfigStateOperations.changeSelectedPunkt(4) }}>4 Подтвердите создание мероприятия</button>
      </div>
      {
        assessmentConfigState.selectedPunkt == 3 ? (
          <ConfigTableMenu />
        ) : (
            <div className='staff-assessment-place-info night-mode-text'>При выходе из раздела все данные в формах сохраняются.</div>
          )
      }
      <FormInputNameAssessment />
      <SelectUsersForAssessment />
      <InvitesConfigTable />
      <ConfirmationAssessment />
    </div>
  )
});

export default StaffAssessmentConfig;