import React, { useState, useEffect } from "react";
import { imageLoadOperations } from "../appstate/styles_config.js";
import axios from "axios";
import globalConfig from "../appstate/global_config.js";
import { observer } from "mobx-react";
import { wpCompetenciesState, wpcstateOperations } from "../appstate/wpCompetenciesState.js";
import { ugstate, ugstateOperations } from "../appstate/usersGroupsState.js";
import authOperations from "../appstate/userauthOperations.js";
import ImportUG from "./ImportUG.js";

const SearchUsersUGForm = observer((props) => {
  let [sortByAlphabet, changesortByAlphabet] = useState(false);
  let refForm = React.createRef();

  let openImportUGForm = () => {
    refForm.current.style.display = 'flex';
  }

  let downloadUsersList = (sParam = '') => {
    if (props.field != 'ugu') {
      if (props.setBaseCounter) {
        props.setBaseCounter();
      }
    }
    imageLoadOperations.openImageLoad();

    let searchHref = {
      'ug': `/searchug?nameUG=${sParam}&sortabc=${sortByAlphabet}`,
      'ugu': `/getuserslist?admin=${authOperations.getCookie('userLogin')}`
    };

    if (searchHref[props.field]) {
      axios({
        method: 'get',
        url: globalConfig.serverAddress + searchHref[props.field]
      }).then(response => {
        imageLoadOperations.closeImageLoad();
        if (props.field == 'ug') {
          if (response.status >= 200 && response.status < 300) {
            if (response.data.status == true) {
              ugstate.listOfUG = response.data.content;
            }
          }
        }
        else if (props.field == 'ugu') {
          if (response.status >= 200 && response.status < 300) {
            if (response.data.status == true) {
              ugstate.userslistForUG = response.data.content;
            }
          }
        }
      }).catch(error => {
        imageLoadOperations.closeImageLoad();
        console.log(error);
      });
    }
  }

  useEffect(() => {
    if (props.field != 'ugu') {
      downloadUsersList();
    } else {
      ugstateOperations.getUserListForUG();
    }
  }, []);

  let submitSearchForm = (e) => {
    e.preventDefault();
    downloadUsersList(searchValue);
  }

  let sortByAlphabetState = () => {
    changesortByAlphabet(!sortByAlphabet);
  }

  return (
    <form
      className='standart-form horizontal-form night-mode-form-bg'
      onSubmit={(e) => { submitSearchForm(e) }}
    >
      <div className='form-container input-container form-container-column'>
        <label
          className='form-container-label night-mode-text'
          htmlFor={`searchform${props.field}${props.addfromug ? 'addfromug' : ''}`}
        >
          Ф.И.О.
         </label>
        <input
          type='text'
          id={`searchform${props.field}${props.addfromug ? 'addfromug' : ''}`}
          className='form-container-input night-mode-text night-mode-input'
          value={props.searchValue}
          onChange={(e) => { props.changeSearchValue(e.target.value) }}
        />
      </div>
      <div className='form-container input-container form-container-column'>
        <label
          className='form-container-label night-mode-text'
          htmlFor={`searchformwp${props.field}${props.addfromug ? 'addfromug' : ''}`}
        >
          Должность
         </label>
        <input
          type='text'
          id={`searchformwp${props.field}${props.addfromug ? 'addfromug' : ''}`}
          className='form-container-input night-mode-text night-mode-input'
          value={props.searchValueWP}
          onChange={(e) => { props.changeSearchValueWP(e.target.value) }}
        />
      </div>
      <div className='button-container'>
        {
          props.field != 'ugu' && props.field != 'uassessment' ? (
            <React.Fragment>
              <button
                type='button'
                className={`mini-functional-button mini-${sortByAlphabet ? ' success' : ' dark'}-button mini-sortabc-button`}
                onClick={() => { sortByAlphabetState() }}
              ></button>
              <button
                type='submit'
                className='mini-functional-button mini-primary-button mini-search-button'
              ></button>
            </React.Fragment>
          ) : (null)
        }
        {
          props.addfromug ? (
            <button
              type='button'
              className='mini-functional-button mini-cyan-button mini-add-button'
              onClick={() => {openImportUGForm()}}
            ></button>
          ) : (
              null
            )
        }
        {
          (wpCompetenciesState.redactCompetenciesListForWP && props.field == 'competence') ||
            (ugstate.redactUGMode && props.field == 'ugu') ? (
              <button
                type='button'
                className={`mini-functional-button mini-${props.field == 'competence' ? (wpCompetenciesState.searchStateFilter ? ' warning' : ' dark') : (ugstate.searchStateFilter ? ' warning' : ' dark')}-button mini-filter-button`}
                onClick={() => { props.field == 'competence' ? wpcstateOperations.changeSearchState() : ugstateOperations.changeSearchState() }}
              ></button>
            ) : (
              false
            )
        }
        {
          props.field == 'uassessment' && props.addfromug ? (
            <ImportUG refForm={refForm}/>
          ) : (
              null
            )
        }
      </div>
    </form>
  );
});

export default SearchUsersUGForm;