import React, { useEffect } from "react";
import CheckAuth from "./CheckAuth";
import UserAreaHeader from "./UserAreaHeader";
import { observer } from "mobx-react";
import FillingAssessmentForm from "./FillingAssessmentForm";
import GetInvitesPage from "./GetInvitesPage";
import { fillingAFstateOperations } from "../appstate/fillingassessmentsformsState";

const GetInvites = observer(() => {
  
  let refArea = React.createRef();

  useEffect(() => {
    fillingAFstateOperations.getInvitesList();
  }, []);

  return (
    <div className={`staff-assessment-place night-mode-bg-block`} ref={refArea}>
      <CheckAuth />
      <UserAreaHeader
        logout={refArea}
        backButton={true}
      />
      <div className='staff-assessment-place-buttons'>
        <button type='button' className='staff-assessment-place-buttons-button staff-assessment-place-buttons-button-hover night-mode-form-bg night-mode-form-bg-hover night-mode-text' onClick={() => { fillingAFstateOperations.changeSelectedPunkt(1) }}>Список приглашений</button>
        <button type='button' className='staff-assessment-place-buttons-button staff-assessment-place-buttons-button-hover night-mode-form-bg night-mode-form-bg-hover night-mode-text' onClick={() => { fillingAFstateOperations.changeSelectedPunkt(2) }}>Заполнение бланка</button>
      </div>
      <GetInvitesPage />
      <FillingAssessmentForm />
    </div>
  )
});

export default GetInvites;