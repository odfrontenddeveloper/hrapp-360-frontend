import React, { useState, useEffect } from "react";
import { observer } from "mobx-react";
import ItemWorkPosition from "./ItemWorkPosition.js";
import SearchWPCompetenceForm from "./SearchWPCompetenceForm.js";
import DeleteWorkPositionCompetenceForm from "./DeleteWorkPositionCompetenceForm.js";
import AddRenameWorkPositionForm from "./AddRenameWorkPositionForm.js";
import AddCopyWorkPosition from "./AddCopyWorkPosition.js";
import { ugstate, ugstateOperations } from "../appstate/usersGroupsState.js";

const RedactUsersGroups = observer(() => {
  let formAddUG = React.createRef();
  let formDeleteUG = React.createRef();
  let formRenameUG = React.createRef();
  let formAddCopyUG = React.createRef();
  let refInputName = React.createRef();
  let refRedactName = React.createRef();

  let counterDisplayItemsInList = 0;

  let [deleteIdUG, changedeleteIdUG] = useState('');
  let [deleteNameUG, changedeleteNameUG] = useState('');
  let [redactIdUG, changeredactIdUG] = useState('');
  let [redactNameUG, changeredactNameUG] = useState('');
  let [copyIdUG, changecopyIdUG] = useState('');
  let [copyNameUG, changecopyNameUG] = useState('');
  let [counterDisplayItems, changecounterDisplayItems] = useState(30);

  useEffect(() => {
    ugstateOperations.getUGList();
  }, []);

  let setBaseCounterDisplayItems = () => {
    changecounterDisplayItems(30);
  }

  let incrementCounterDisplayItems = () => {
    changecounterDisplayItems(counterDisplayItems + 30);
  }

  let openAddUGForm = () => {
    formAddUG.current.style.display = 'flex';
  }

  let openDeleteUGForm = (id, name) => {
    formDeleteUG.current.style.display = 'flex';
    changedeleteIdUG(id);
    changedeleteNameUG(name);
  }

  let openRenameUGForm = (id, name) => {
    formRenameUG.current.style.display = 'flex';
    refRedactName.current.value = name;
    changeredactIdUG(id);
    changeredactNameUG(name);
  }

  let openAddCopyUGForm = (id, name) => {
    formAddCopyUG.current.style.display = 'flex';
    changecopyIdUG(id);
    changecopyNameUG(name);
  }

  return (
    <div className='work-positions-area-col work-positions-area-workpositions night-mode-form-bg'>
      <div className='work-positions-area-col-header night-mode-text'>Список подразделений</div>
      <SearchWPCompetenceForm
        setBaseCounter={setBaseCounterDisplayItems}
        field={'ug'}
      />
      <div className='work-positions-area-col-list night-mode-input'>
        <div
          id='list-of-work-positions'
          className='list-of-items'
        >
          {
            ugstate.listOfUG.length > 0 ? (
              ugstate.listOfUG.map((el, i) => {
                if (counterDisplayItemsInList == counterDisplayItems) {
                  counterDisplayItemsInList += 1;
                  return (
                    <div>
                      <button
                        key={'show-more-items-button'}
                        className='mini-functional-button mini-fiolet-button mini-more-button'
                        onClick={() => { incrementCounterDisplayItems() }}
                      ></button>
                    </div>
                  );
                }
                if (counterDisplayItemsInList > counterDisplayItems) {
                  return;
                }
                counterDisplayItemsInList += 1;
                return (
                  <ItemWorkPosition
                    key={'item-ug-' + el.id}
                    ug={true}
                    counterIterator={i}
                    wpInfo={el}
                    onDelete={openDeleteUGForm}
                    onRename={openRenameUGForm}
                    addCopy={openAddCopyUGForm}
                  />
                );
              })
            ) : (
                <div className='null-list-message night-mode-text'>Ничего не найдено.</div>
              )
          }
        </div>
      </div>
      <button
        type='button'
        className='mini-functional-button mini-success-button mini-add-button form-list-add-button'
        onClick={() => { openAddUGForm() }}
      ></button>
      <AddRenameWorkPositionForm
        field={'ug'}
        modeAdd={true}
        refForm={formAddUG}
        refInputName={refInputName}
      />
      <AddRenameWorkPositionForm
        field={'ug'}
        modeAdd={false}
        refForm={formRenameUG}
        redactIdUG={redactIdUG}
        redactNameUG={redactNameUG}
        refInputName={refRedactName}
      />
      <DeleteWorkPositionCompetenceForm
        field={'ug'}
        refForm={formDeleteUG}
        deleteId={deleteIdUG}
        deleteName={deleteNameUG}
      />
    </div>
  );
});

export default RedactUsersGroups;