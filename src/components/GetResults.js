import React, { useEffect } from "react";
import CheckAuth from "./CheckAuth";
import UserAreaHeader from "./UserAreaHeader";
import { observer } from "mobx-react";
import { fillingAFstateOperations } from "../appstate/fillingassessmentsformsState";
import { resultsStateOperations, resultsState } from "../appstate/showResultsState";
import AssessmentsList from "./AssessmentsList";
import MoreInfo from "./MoreInfo";

const GetResults = observer(() => {

  let refArea = React.createRef();

  useEffect(() => {
    fillingAFstateOperations.getInvitesList();
  }, []);

  return (
    <div className={`staff-assessment-place night-mode-bg-block`} ref={refArea}>
      <CheckAuth />
      <UserAreaHeader
        logout={refArea}
        backButton={true}
      />
      <div className='staff-assessment-place-buttons'>
        <button type='button' className='staff-assessment-place-buttons-button staff-assessment-place-buttons-button-hover night-mode-form-bg night-mode-form-bg-hover night-mode-text' onClick={() => { resultsStateOperations.changeSelectedPunkt(1) }}>Список мероприятий</button>
        {
          resultsState.showResMode == true ? (
            <button type='button' className='staff-assessment-place-buttons-button staff-assessment-place-buttons-button-hover night-mode-form-bg night-mode-form-bg-hover night-mode-text' onClick={() => { resultsStateOperations.changeSelectedPunkt(2) }}>Подробные результаты</button>
          ) : (
            null
          )
        }
      </div>
      <AssessmentsList />
      <MoreInfo />
    </div>
  )
});

export default GetResults;