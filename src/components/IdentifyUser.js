import React, { useState, useEffect } from "react";
import { observer } from "mobx-react";
import { Redirect } from "react-router-dom";
import regPageState from "../appstate/RegPageState.js";
import { stylesConfig } from "../appstate/styles_config.js";
import LoginForm from "./LoginForm.js";
import RegForm from "./RegForm.js";
import authState from "../appstate/authstate.js";

let IdentifyUser = observer(props => {
  let refForm = React.createRef();
  let changeButton = React.createRef();

  let [isUserAuth, setUserAuth] = useState(false);

  useEffect(() => {
    setUserAuth(authState.auth);
  });

  let resetChangeButton = () => {
    changeButton.current.disabled = !changeButton.current.disabled;
  }

  let changeForm = () => {
    resetChangeButton();
    refForm.current.style.animation = stylesConfig.animationHideElement;
    setTimeout(() => {
      resetChangeButton();
      regPageState.formRegistration = !regPageState.formRegistration;
    }, stylesConfig.hideElementsTiming - 10);
  }

  return (
    <div className='reg-and-auth-user-page night-mode-bg-block'>
      <div>
        {
          isUserAuth ? (
            <Redirect to='/dashboard' />
          ) : (
              null
            )
        }
      </div>
      {
        regPageState.formRegistration ? (
          <RegForm forRef={refForm} forButton={changeButton} changeForm={changeForm} />
        ) : (
            <LoginForm forRef={refForm} forButton={changeButton} changeForm={changeForm} />
          )
      }
    </div>
  );
});

export default IdentifyUser;