import React, { useState, useEffect } from "react";
import { imageLoadOperations } from "../appstate/styles_config.js";
import axios from "axios";
import globalConfig from "../appstate/global_config";
import RedactData from "./RedactData";
import { userslist } from "../appstate/redactStaffState.js";

const ChangeUserInfo = (props) => {

  const [stateName, changeStateName] = useState(false);
  const [stateSurename, changeStateSurename] = useState(false);
  const [statePatronymic, changeStatePatronymic] = useState(false);

  let blockres = React.createRef();

  useEffect(() => {
    axios({
      method: 'get',
      url: globalConfig.serverAddress + '/getuserfullname'
    }).then(response => {
      changeStateName(response.data.content.name);
      changeStateSurename(response.data.content.surename);
      changeStatePatronymic(response.data.content.patronymic);
      userslist.list = [];
    }).catch(error => {
      console.log(error);
    });
  }, []);

  let updateUserFullName = (e, field) => {
    e.preventDefault();
    imageLoadOperations.openImageLoad();
    let updatefield = {
      'name': stateName,
      'surename': stateSurename,
      'patronymic': statePatronymic
    };
    axios({
      method: 'patch',
      url: globalConfig.serverAddress + '/changeusername',
      data: {
        updatename: updatefield[field],
        updatefield: field
      }
    }).then(response => {
      props.ifUpdate();
      blockres.current.className = 'successColor';
      if (!response.data.status) {
        blockres.current.className = 'dangerColor';
      }
      blockres.current.innerHTML = response.data.content;
      imageLoadOperations.closeImageLoad();
    }).catch(error => {
      console.log(error);
      imageLoadOperations.closeImageLoad();
    });
  }

  return (
    <RedactData forRef={props.forRef}>
      <form className='standart-form horizontal-form redact-user-data-block night-mode-form-bg' onSubmit={(e) => { updateUserFullName(e, 'name') }}>
        <div className='form-container input-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='changeName'>Имя:</label>
          <input id='changeName' className='form-container-input night-mode-text night-mode-input' type='text' value={stateName} onChange={(e) => { changeStateName(e.target.value) }} />
        </div>
        <div className='form-container button-container form-container-column'>
          <button type='submit' className='mini-functional-button mini-warning-button mini-redact-button'></button>
        </div>
      </form>
      <form className='standart-form horizontal-form redact-user-data-block night-mode-form-bg' onSubmit={(e) => { updateUserFullName(e, 'surename') }}>
        <div className='form-container input-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='changeSurename'>Фамилия:</label>
          <input id='changeSurename' className='form-container-input night-mode-text night-mode-input' type='text' value={stateSurename} onChange={(e) => { changeStateSurename(e.target.value) }} />
        </div>
        <div className='form-container button-container form-container-column'>
          <button type='submit' className='mini-functional-button mini-warning-button mini-redact-button'></button>
        </div>
      </form>
      <form className='standart-form horizontal-form redact-user-data-block night-mode-form-bg' onSubmit={(e) => { updateUserFullName(e, 'patronymic') }}>
        <div className='form-container input-container form-container-column'>
          <label className='form-container-label night-mode-text' htmlFor='changePatronymic'>Отчество:</label>
          <input id='changePatronymic' className='form-container-input night-mode-text night-mode-input' type='text' value={statePatronymic} onChange={(e) => { changeStatePatronymic(e.target.value) }} />
        </div>
        <div className='form-container button-container form-container-column'>
          <button type='submit' className='mini-functional-button mini-warning-button mini-redact-button'></button>
        </div>
      </form>
      <div className='result-container redact-user-data-block'>
        <div id='result-upadate-info' ref={blockres}></div>
      </div>
    </RedactData>
  );
}

export default ChangeUserInfo;