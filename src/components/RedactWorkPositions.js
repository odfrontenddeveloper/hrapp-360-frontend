import React, { useState } from "react";
import { observer } from "mobx-react";
import listsofdata from "../appstate/listsofdata.js";
import ItemWorkPosition from "./ItemWorkPosition.js";
import SearchWPCompetenceForm from "./SearchWPCompetenceForm.js";
import DeleteWorkPositionCompetenceForm from "./DeleteWorkPositionCompetenceForm.js";
import AddRenameWorkPositionForm from "./AddRenameWorkPositionForm.js";
import AddCopyWorkPosition from "./AddCopyWorkPosition.js";

const RedactWorkPositions = observer(() => {
  let formAddWP = React.createRef();
  let formDeleteWP = React.createRef();
  let formRenameWP = React.createRef();
  let formAddCopyWP = React.createRef();
  let refInputName = React.createRef();
  let refRedactName = React.createRef();

  let counterDisplayItemsInList = 0;

  let [deleteIdWP, changedeleteIdWP] = useState('');
  let [deleteNameWP, changedeleteNameWP] = useState('');
  let [redactIdWP, changeredactIdWP] = useState('');
  let [redactNameWP, changeredactNameWP] = useState('');
  let [copyIdWP, changecopyIdWP] = useState('');
  let [copyNameWP, changecopyNameWP] = useState('');
  let [counterDisplayItems, changecounterDisplayItems] = useState(30);

  let setBaseCounterDisplayItems = () => {
    changecounterDisplayItems(30);
  }

  let incrementCounterDisplayItems = () => {
    changecounterDisplayItems(counterDisplayItems + 30);
  }

  let openAddWPForm = () => {
    formAddWP.current.style.display = 'flex';
  }

  let openDeleteWPForm = (id, name) => {
    formDeleteWP.current.style.display = 'flex';
    changedeleteIdWP(id);
    changedeleteNameWP(name);
  }

  let openRenameWPForm = (id, name) => {
    formRenameWP.current.style.display = 'flex';
    refRedactName.current.value = name;
    changeredactIdWP(id);
    changeredactNameWP(name);
  }

  let openAddCopyWPForm = (id, name) => {
    formAddCopyWP.current.style.display = 'flex';
    changecopyIdWP(id);
    changecopyNameWP(name);
  }

  return (
    <div className='work-positions-area-col work-positions-area-workpositions night-mode-form-bg'>
      <div className='work-positions-area-col-header night-mode-text'>Список должностей</div>
      <SearchWPCompetenceForm
        setBaseCounter={setBaseCounterDisplayItems}
        field={'wp'}
      />
      <div className='work-positions-area-col-list night-mode-input'>
        <div
          id='list-of-work-positions'
          className='list-of-items'
        >
          {
            listsofdata.listOfWP.length > 0 ? (
              listsofdata.listOfWP.map((el, i) => {
                if (counterDisplayItemsInList == counterDisplayItems) {
                  counterDisplayItemsInList += 1;
                  return (
                    <div>
                      <button
                        key={'show-more-items-button'}
                        className='mini-functional-button mini-fiolet-button mini-more-button'
                        onClick={() => { incrementCounterDisplayItems() }}
                      ></button>
                    </div>
                  );
                }
                if (counterDisplayItemsInList > counterDisplayItems) {
                  return;
                }
                counterDisplayItemsInList += 1;
                return (
                  <ItemWorkPosition
                    key={'item-wp-' + el.id}
                    counterIterator={i}
                    wpInfo={el}
                    onDelete={openDeleteWPForm}
                    onRename={openRenameWPForm}
                    addCopy={openAddCopyWPForm}
                  />
                );
              })
            ) : (
                <div className='null-list-message night-mode-text'>Ничего не найдено.</div>
              )
          }
        </div>
      </div>
      <button
        type='button'
        className='mini-functional-button mini-success-button mini-add-button form-list-add-button'
        onClick={() => { openAddWPForm() }}
      ></button>
      <AddCopyWorkPosition
        idWP={copyIdWP}
        nameWP={copyNameWP}
        refForm={formAddCopyWP}
      />
      <AddRenameWorkPositionForm
        field={'wp'}
        modeAdd={true}
        refForm={formAddWP}
        refInputName={refInputName}
      />
      <AddRenameWorkPositionForm
        field={'wp'}
        modeAdd={false}
        refForm={formRenameWP}
        redactIdWP={redactIdWP}
        redactNameWP={redactNameWP}
        refInputName={refRedactName}
      />
      <DeleteWorkPositionCompetenceForm
        field={'wp'}
        refForm={formDeleteWP}
        deleteId={deleteIdWP}
        deleteName={deleteNameWP}
      />
    </div>
  );
});

export default RedactWorkPositions;