import React, { useEffect } from "react";
import globalConfig from "../appstate/global_config";
import { observer } from "mobx-react";

const ItemUser = observer(props => {
  let refImage = React.createRef();
  const usersTypes = [
    'administrator',
    'moderator',
    'user'
  ];

  useEffect(() => {
    refImage.current.style.backgroundImage = `url('${globalConfig.serverAddress}/useravatar?login=${props.userData.login}')`;
  });

  return (
    <div className='redact-staff-place-display-info-block night-mode-form-bg-lite'>
      <div className='redact-staff-place-display-info-block-avatar'>
        <div className='redact-staff-place-display-info-block-avatar-image' ref={refImage}></div>
      </div>
      <div className='redact-staff-place-display-info-block-info'>
        <div className='redact-staff-place-display-info-block-info-text night-mode-text'>
          <button
            type='button'
            className='mini-functional-button mini-primary-button mini-settings-button'
            onClick={() => { props.onRedact(props.userData) }}
          ></button>
          <button
            type='button'
            className='mini-functional-button mini-success-button mini-work-button'
            onClick={() => { props.onRedactWP(props.userData.wp, props.userData.id, props.userData.login) }}
          ></button>
          {
            usersTypes[props.userData.type - 1] == 'administrator' ? (
              null
            ) : (
                <button
                  type='button'
                  className='mini-functional-button mini-danger-button mini-delete-button'
                  onClick={() => { props.onDelete(props.userData.id, props.userData.login) }}
                ></button>
              )
          }
        </div>
        <div className='redact-staff-place-display-info-block-info-text night-mode-text'>{
          props.userData.login
        }</div>
        <div className='redact-staff-place-display-info-block-null-line night-mode-form-bg'></div>
        <div className='redact-staff-place-display-info-block-info-text night-mode-text'>{
          props.userData.surename
        }</div>
        <div className='redact-staff-place-display-info-block-info-text night-mode-text'>{
          props.userData.name
        }</div>
        <div className='redact-staff-place-display-info-block-info-text night-mode-text'>{
          props.userData.patronymic
        }</div>
        <div className='redact-staff-place-display-info-block-null-line night-mode-form-bg'></div>
        <div className='redact-staff-place-display-info-block-info-text bold-italic night-mode-text'>{
          <span className={`block-type-user bold-italic${(usersTypes[props.userData.type - 1] == 'administrator' ? ' dangerColor' : '')} ${(usersTypes[props.userData.type - 1] == 'moderator' ? ' successColor' : '')} ${(usersTypes[props.userData.type - 1] == 'user' ? ' primaryColor' : '')}`}>
            {usersTypes[props.userData.type - 1]}
          </span>
        }</div>
        <div className='redact-staff-place-display-info-block-null-line night-mode-form-bg'></div>
        <div className='redact-staff-place-display-info-block-info-text night-mode-text'>{
          props.userData.wp ? props.userData.wp : 'Должность не указана'
        }</div>
      </div>
    </div>
  );
});

export default ItemUser;