import React, { useEffect, useState } from "react";
import RedactData from "./RedactData";
import { ugstateOperations, ugstate } from "../appstate/usersGroupsState";
import { observer } from "mobx-react";
import ItemUGToImport from "./ItemUGToImport.js";

const ImportUG = observer((props) => {

  let [sv, changesv] = useState('');

  useEffect(() => {
    ugstateOperations.getUGListForAssessment();
  }, []);

  return (
    <RedactData forRef={props.refForm}>
      <form className='standart-form vertical-form form-center night-mode-form-bg' onSubmit={(e) => { handlerAddCriteriaForm(e) }}>
        <div className='input-container form-container-column'>
          <label
            className='form-container-label night-mode-text'
          >Импорт сотрудников из подразделения:</label>
          <input
            type='text'
            className='form-container-input night-mode-input night-mode-text'
            value={sv}
            onChange={(e) => { changesv(e.target.value) }}
            placeholder='Название подразделения'
          />
        </div>
        <div className='redact-user-wp-list-of-wp night-mode-input'>
          {
            ugstate.listOfUGForAssessment.map(element => {
              if (element.name.indexOf(sv) == -1) {
                return;
              }
              return <ItemUGToImport key={element.id} data={element} />
            })
          }
        </div>
      </form>
    </RedactData>
  );
});

export default ImportUG;