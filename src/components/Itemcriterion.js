import React from "react";
import { observer } from "mobx-react";

const Itemcriterion = observer(props => {
  return (
    <div className='item-criterion night-mode-form-bg'>
      <div className='item-criterion-num night-mode-text'>
        {props.iteratorCount}
      </div>
      <div className='item-criterion-criterion night-mode-text'>
        <div className='item-criterion-criterion-type night-mode-text'>
          <div className='item-criterion-criterion-text night-mode-text'>
            {props.criterionInfo.criterion.typecheckbox ? (
              `Выбор нескольких ответов (ckeckbox)`
            ) : (
                `Выбор одного ответа (radio button)`
              )}
          </div>
        </div>
        <div className='item-criterion-criterion-text item-criterion-criterion-text-bg night-mode-text'>
          {props.criterionInfo.criterion.text}
        </div>
      </div>
      <div className='item-criterion-signs night-mode-text'>
        {
          props.criterionInfo.signs.map(sign => {
            return (
              <div
                className={`item-criterion-signs-sign`}
                key={sign.id}
              >
                <div className='item-criterion-signs-sign-text night-mode-text'>{sign.text}</div>
                <div className='item-criterion-signs-sign-percents night-mode-text'>
                  <div className='item-criterion-signs-sign-percents-container night-mode-form-bg'>
                    <div className='item-criterion-signs-sign-percents-container-value'>{`${sign.weight}%`}</div>
                    <div
                      style={{
                        width: `${sign.weight}%`
                      }}
                      className='item-criterion-signs-sign-percents-container-progress'
                    ></div>
                  </div>
                </div>
              </div>
            );
          })
        }
      </div>
      <div className='item-criterion-buttons'>
        <button type='button' className='mini-functional-button mini-warning-button mini-redact-button' onClick={() => {props.openRedactCriteriaForm(props.criterionInfo.criterion.id)}}></button>
        <button type='button' className='mini-functional-button mini-danger-button mini-delete-button' onClick={() => {props.openDeleteCriteriaForm(props.criterionInfo.criterion.id)}}></button>
      </div>
    </div>
  );
});

export default Itemcriterion;