import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { stylesConfig } from "../appstate/styles_config.js";
import { observer } from "mobx-react";

const RedactData = observer((props) => {

  useEffect(() => {
    let clickEvent = (e) => {
      if (e.target.className.split(' ').includes('redact-user-data')) {
        closeWindow.call(props.forRef.current);
      }
    };
    let keydownEvent = (e) => {
      if (e.code == 'Escape') {
        if (props.forRef.current.style.display == 'flex') {
          closeWindow.call(props.forRef.current);
        }
      }
    };
    document.addEventListener('click', clickEvent);
    document.addEventListener('keydown', keydownEvent);

    return () => {
      document.removeEventListener('click', clickEvent);
      document.removeEventListener('keydown', keydownEvent);
    };
  });

  function closeWindow() {
    this.style.animation = stylesConfig.animationHideElement;
    setTimeout(() => {
      this.style.display = 'none';
      this.style.animation = '';
      if (props.onCloseForm) {
        props.onCloseForm();
      }
    }, stylesConfig.hideElementsTiming - 10);
  }

  return (
    ReactDOM.createPortal(
      <div className={`redact-user-data${stylesConfig.nightMode ? ' night-mode-app' : ''}`} ref={props.forRef}>
        <button type='button' className='mini-functional-button mini-red-orange-button mini-close-button close-absolute-window' onClick={() => { closeWindow.call(props.forRef.current) }}></button>
        {props.children}
      </div>,
      document.getElementById('root')
    )
  );
});

export default RedactData;