import React, { useState } from "react";
import RedactData from "./RedactData";
import { stylesConfig, imageLoadOperations } from "../appstate/styles_config";
import globalConfig from "../appstate/global_config";
import axios from "axios";
import { userslist } from "../appstate/redactStaffState.js";

let DeleteUserFromSystem = props => {
  let [resultDeleteUser, changeresultDeleteUser] = useState(null);

  let closeDeleteForm = () => {
    const element = props.refFormDelete.current;
    element.style.animation = stylesConfig.animationHideElement;
    setTimeout(() => {
      element.style.display = 'none';
      element.style.animation = '';
    }, stylesConfig.hideElementsTiming - 10);
  }

  let handlerDeleteForm = (e) => {
    imageLoadOperations.openImageLoad();
    e.preventDefault();
    let deleteId = props.refDeleteButton.current.value;
    let data = {
      deleteId: deleteId
    };
    axios({
      method: 'delete',
      url: globalConfig.serverAddress + '/deleteuser',
      data: data
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.data.status == true) {
        changeresultDeleteUser(null);
        closeDeleteForm();
        userslist.list = userslist.list.filter((el, i) => {
          return el.id != data.deleteId;
        });
      }
      else {
        changeresultDeleteUser(response.data.content);
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
    });
  }

  let onCloseForm = () => {
    changeresultDeleteUser(null);
  }

  return (
    <RedactData forRef={props.refFormDelete} onCloseForm={onCloseForm}>
      <form
        className='standart-form vertical-form form-center night-mode-form-bg'
        onSubmit={(e) => { handlerDeleteForm(e) }}
      >
        <div className='form-container input-container form-container-column'>
          <label
            id='info-delete-user'
            className='form-container-label night-mode-text'
            ref={props.infoDeleteUser}
          ></label>
        </div>
        <div className='button-container'>
          <button
            type='submit'
            className='mini-functional-button mini-success-button mini-complete-button'
            ref={props.refDeleteButton}
          ></button>
          <button
            type='button'
            className='mini-functional-button mini-danger-button mini-close-button'
            onClick={() => closeDeleteForm()}
          ></button>
        </div>
      </form>
      <div
        id='result-delete-user'
        className='result-container dangerColor res-form-center'
      >{resultDeleteUser}</div>
    </RedactData>
  );
}

export default DeleteUserFromSystem;