import React, { useState, useEffect } from "react";
import { observer } from "mobx-react";
import ItemUserForUG from "./ItemUserForUG.js";
import { ugstate } from "../appstate/usersGroupsState.js";
import SearchUsersUGForm from "./SearchUsersUGForm.js";

const RedactUsersListForUG = observer(() => {
  let [searchValue, changeSearchValue] = useState('');
  let [searchValueWP, changeSearchValueWP] = useState('');

  useEffect(() => {
    ugstate.redactUGMode = false;
    ugstate.redactUGID = false;
  }, []);

  return (
    <div className='work-positions-area-col work-positions-area-workpositions night-mode-form-bg'>
      <div className='work-positions-area-col-header night-mode-text'>Список сотрудников</div>
      <SearchUsersUGForm
        field={'ugu'}
        searchValue={searchValue}
        changeSearchValue={changeSearchValue}
        searchValueWP={searchValueWP}
        changeSearchValueWP={changeSearchValueWP}
      />
      <div className='work-positions-area-col-list night-mode-input'>
        <div
          id='list-of-work-positions'
          className='list-of-items'
        >
          {
            ugstate.userslistForUG.length > 0 ? (
              ugstate.userslistForUG.map((el, i) => {
                if (ugstate.searchStateFilter) {
                  if (!ugstate.usersGroupsConnections.includes(el.id)) {
                    return;
                  }
                }
                if ([el.surename.toLowerCase(), el.name.toLowerCase(), el.patronymic.toLowerCase()].join(' ').indexOf(searchValue.toLowerCase()) == -1) {
                  return;
                }
                if ((el.wp ? el.wp : 'Должность не указана').toLowerCase().indexOf(searchValueWP.toLowerCase()) == -1) {
                  return;
                }
                return (
                  <ItemUserForUG
                    key={'item-ug-' + el.id}
                    counterIterator={i}
                    userInfo={el}
                  />
                );
              })
            ) : (
                <div className='null-list-message night-mode-text'>Ничего не найдено.</div>
              )
          }
        </div>
      </div>
    </div>
  );
});

export default RedactUsersListForUG;