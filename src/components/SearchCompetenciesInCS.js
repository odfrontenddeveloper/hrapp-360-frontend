import React from "react";
import { criteriaSignsState, criteriaSignsOperations } from "../appstate/criteriasignsState.js";
import { observer } from "mobx-react";

const SearchCompetenciesInCS = observer((props) => {
  return (
    <form className='standart-form vertical-form night-mode-form-bg'>
      <div className='input-container form-container-column'>
        <label
          className='form-container-label night-mode-text'
          htmlFor='searchWPForCriteria'
        >Поиск должности</label>
      </div>
      <div className='input-container form-container-column'>
        <input
          type='text'
          id='searchWPForCriteria'
          className='form-container-input night-mode-text night-mode-input'
          value={props.searchWPValue}
          onChange={(e) => { props.changeSearchWPValue(e.target.value) }}
        />
      </div>
      <div className='input-container form-container-column form-container-padding'>
        <select className='form-container-select form-container-column w-100 night-mode-text night-mode-input' onChange={(e) => { criteriaSignsOperations.getWpCompetenceConnectsCS(e.target.value) }}>
          <option key={`default`} value={0}>{`-- Выберите должность для поиска --`}</option>
          {
            criteriaSignsState.wpListForCS.map((el, i) => {
              if (el.name.indexOf(props.searchWPValue) == -1) {
                return;
              }
              return (
                <option key={el.id} value={el.id}>{el.name}</option>
              );
            })
          }
        </select>
      </div>
      <div className='input-container form-container-column'>
        <label
          className='form-container-label night-mode-text'
          htmlFor='searchCompetenceForCriteria'
        >Поиск компетенции</label>
        <input
          id='searchCompetenceForCriteria'
          className='form-container-input night-mode-text night-mode-input'
          type='text'
          value={props.searchValue}
          onChange={(e) => { props.changeSearchValue(e.target.value) }}
        />
      </div>
    </form>
  );
});

export default SearchCompetenciesInCS;