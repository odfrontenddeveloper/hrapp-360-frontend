import React from "react";
import { criteriaSignsOperations } from "../appstate/criteriasignsState.js";

const ItemCompetenceInCS = (props) => {
  return (
    <div
      className='item-in-list'
      style={{ animation: `${1 * 0.4}s showBlock forwards` }}
    >
      <div className='item-in-list-data'>
        <div className='item-in-list-data-block night-mode-text'>{props.el.name}</div>
      </div>
      <div className='item-in-list-buttons-container night-mode-text'>
        <button
          type='button'
          className={`mini-functional-button mini-${props.el.selectedCompetence ? 'red-orange' : 'primary'}-button mini-settings-button`}
          onClick={() => { criteriaSignsOperations.changeRedactCompetenceMode(props.el.id) }}
        ></button>
      </div>
    </div>
  );
}

export default ItemCompetenceInCS;