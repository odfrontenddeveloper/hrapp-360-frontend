import React, { useState, useEffect } from "react";
import { criteriaSignsState, criteriaSignsOperations } from "../appstate/criteriasignsState.js";
import { observer } from "mobx-react";
import RedactData from "./RedactData.js";
import SignRangeInput from "./SignRangeInput.js";
import shortid from "shortid";

const AddRedactCriteriaForm = observer((props) => {
  let [typeCheckBox, changeTypeCheckBox] = useState(false);
  // let [signsSum, changeSignsSum] = useState(0);
  let [criterion, changeCriterion] = useState('');
  let [successColor, changeColor] = useState(true);
  let [resAdd, changeResAdd] = useState(null);

  useEffect(() => {
    Object.keys(criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact']).map((el) => {
      criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][el].value = 0;
    });
    // changeSignsSum(0);
  }, [typeCheckBox]);

  let onCloseForm = () => { changeResAdd(''); }

  let changeRange = (value, name) => {
    if (props.modeAdd ? (Number(typeCheckBox) === 1) : (Number(criteriaSignsState.typeCheckboxRedact) === 1)) {
      let sum = Object.keys(criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact']).reduce((el1, el2) => {
        return el1 + (el2 == name ? Number(value) : criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][el2].value);
      }, 0);

      let sumWithoutValue = sum - Number(value);

      if (sum <= 100) {
        criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][name].value = Number(value);
        // changeSignsSum(sum);
      }
      else {
        criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][name].value = 100 - sumWithoutValue;
        // changeSignsSum(100);
      }
    }
    else {
      criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][name].value = Number(value);
    }
  }

  let renderSignsList = () => {
    let arrayOfSigns = Object.keys(criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact']).map(el => {
      return (
        <SignRangeInput
          keyItem={el}
          typecheckbox={typeCheckBox}
          signname={criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][el].name}
          signvalue={criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][el].value}
          deleteSign={deleteSign}
          changeRange={changeRange}
          changeSignInput={changeSignInput}
          modeAdd={props.modeAdd}
          key={el}
          sum={Object.keys(criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact']).reduce((el1, el2) => {
            return el1 + (el2 == name ? Number(value) : criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][el2].value);
          }, 0)}
        />
      );
    });

    return arrayOfSigns;
  }

  let deleteSign = (keyItem) => {
    let sum = 0;
    criteriaSignsOperations.deleteSignField(keyItem, props.modeAdd ? true : false);
    Object.keys(criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact']).map((el) => {
      sum = sum + criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][el].value;
    });
    // changeSignsSum(sum);
  }

  let changeSignInput = (value, keyItem) => {
    criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][keyItem].name = value;
  }

  let changeTypeForm = (value) => {
    if (props.modeAdd) {
      if (value == 1) {
        changeTypeCheckBox(true);
      }
      else {
        changeTypeCheckBox(false);
      }
    }
    else {
      if (value == 1) {
        criteriaSignsState.typeCheckboxRedact = true;
      }
      else {
        criteriaSignsState.typeCheckboxRedact = false;
      }
      Object.keys(criteriaSignsState.rangesRedact).map((el) => {
        criteriaSignsState.rangesRedact[el].value = 0;
      });
    }
  }

  let resetAddCriteriaForm = () => {
    let rangesIdList = [
      shortid.generate(),
      shortid.generate()
    ];
    changeTypeCheckBox(false);
    changeCriterion('');
    criteriaSignsState.ranges = {
      [rangesIdList[0]]: { name: '', value: 0 },
      [rangesIdList[1]]: { name: '', value: 0 }
    };
  }

  let handlerAddCriteriaForm = (e) => {
    e.preventDefault();
    let data = {
      competence: criteriaSignsState.redactCompetenceModeID,
      criterionTypeCheckbox: props.modeAdd ? typeCheckBox : criteriaSignsState.typeCheckboxRedact,
      criterion: props.modeAdd ? criterion : criteriaSignsState.criterionRedact,
      signs: Object.keys(criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact']).map(element => {
        return {
          name: criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][element].name,
          value: criteriaSignsState[props.modeAdd ? 'ranges' : 'rangesRedact'][element].value
        };
      })
    };
    if (props.modeAdd) {
      criteriaSignsOperations.addCriterion(data, changeColor, changeResAdd, resetAddCriteriaForm);
    }
    else {
      criteriaSignsOperations.redactCriterion(data, changeColor, changeResAdd, props.redactID);
    }
  }

  let changeCriterionNameInput = (value) => {
    criteriaSignsState.criterionRedact = value;
  }

  return (
    <RedactData forRef={props.refForm} onCloseForm={onCloseForm}>
      <form className='standart-form vertical-form form-add-criteria night-mode-form-bg' onSubmit={(e) => { handlerAddCriteriaForm(e) }}>
        <div className='input-container form-container-column'>
          <label
            className='form-container-label text-start night-mode-text'
            htmlFor={`selectSignType${props.modeAdd ? '' : 'Redact'}`}
          >Выберите тип критерия:</label>
        </div>
        <div className='input-container form-container-column'>
          <select
            id={`selectSignType${props.modeAdd ? '' : 'Redact'}`}
            className='form-container-select form-container-column night-mode-text night-mode-input'
            onChange={(e) => { changeTypeForm(e.target.value) }}
            value={props.modeAdd ? (typeCheckBox ? 1 : 0) : (criteriaSignsState.typeCheckboxRedact ? 1 : 0)}
          >
            <option value={0}>Выбор одного варианта ответа</option>
            <option value={1}>Выбор нескольких вариантов ответа</option>
          </select>
        </div>
        <div className='input-container form-container-column'>
          <label
            className='form-container-label night-mode-text'
            htmlFor={`criteriaName${props.modeAdd ? '' : 'Redact'}`}
          >Описание критерия:</label>
        </div>
        <div className='input-container form-container-column'>
          <textarea
            id={`criteriaName${props.modeAdd ? '' : 'Redact'}`}
            className='form-container-textarea form-container-textarea-head night-mode-text night-mode-input'
            value={props.modeAdd ? criterion : criteriaSignsState.criterionRedact}
            onChange={(e) => { props.modeAdd ? changeCriterion(e.target.value) : (changeCriterionNameInput(e.target.value)) }}
          ></textarea>
        </div>
        <div className='list-of-signs-in-add-form'>
          <div>
            {
              renderSignsList()
            }
            <div className='list-of-signs-buttons'>
              <button type='button' className='href-button' onClick={() => { criteriaSignsOperations.addSignField(props.modeAdd ? true : false) }}>Добавить признак</button>
            </div>
          </div>
        </div>
        <div className='list-of-signs-buttons'>
          <button
            type='submit'
            className={`mini-functional-button mini-success-button mini-${props.modeAdd ? `add` : `complete`}-button`}
            value={props.redactID ? props.redactID : null}
          ></button>
        </div>
        <div className={`result-container ${successColor ? 'successColor' : 'dangerColor'}`}>{resAdd}</div>
      </form>
    </RedactData>
  );
});

export default AddRedactCriteriaForm;