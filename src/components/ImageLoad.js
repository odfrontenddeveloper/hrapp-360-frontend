import React from "react";
import { observer } from "mobx-react";
import { imageLoadStatus } from "../appstate/styles_config";
import { image_path } from "../appstate/global_config";

const ImageLoad = observer(() => {
  return (
    <div>
      {imageLoadStatus.isOpen ? (
        <div className='standart-form-loader-image' id='load-image'>
          <img src={`${image_path[process.env.NODE_ENV]}/circle.gif`} />
        </div>
      ) : (
          null
        )}
    </div>
  );
});

export default ImageLoad;