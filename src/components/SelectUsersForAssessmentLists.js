import React, { useState, useEffect } from "react";
import { observer } from "mobx-react";
import { assessmentConfigState, assessmentConfigStateOperations } from "../appstate/assessmentConfigState";
import ItemUserForAssessmentList from "./ItemUserForAssessmentList";
import SearchUsersUGForm from "./SearchUsersUGForm";

const SelectUsersForAssessmentLists = observer((props) => {
  let [searchValue, changeSearchValue] = useState('');
  let [searchValueWP, changeSearchValueWP] = useState('');

  useEffect(() => {
    if (props.selectedUsers) {
      assessmentConfigStateOperations.getUsersListForAssessment();
    }
  }, []);

  return (
    <div className='users-list-area-col night-mode-form-bg'>
      <div className='users-list-area-col-header'>
        <SearchUsersUGForm
          field={'uassessment'}
          searchValue={searchValue}
          changeSearchValue={changeSearchValue}
          searchValueWP={searchValueWP}
          changeSearchValueWP={changeSearchValueWP}
          addfromug={props.addfromug}
        />
      </div>
      <div className='users-list-area-col-list night-mode-input'>
        <div className='users-list-area-col-list-items night-mode-input'>
          {
            assessmentConfigState.usersListForAssessment.filter(user => {
              return props.selectedUsers ? user.selected == false : user.selected == true;
            }).map(user => {
              if ([user.surename.toLowerCase(), user.name.toLowerCase(), user.patronymic.toLowerCase()].join(' ').indexOf(searchValue.toLowerCase()) == -1) {
                return;
              }
              if ((user.wp ? user.wp : '').toLowerCase().indexOf(searchValueWP.toLowerCase()) == -1) {
                return;
              }
              return (
                <ItemUserForAssessmentList
                  selectedUsers={props.selectedUsers}
                  user={user}
                  key={user.id}
                />
              );
            })
          }
        </div>
      </div>
    </div>
  );
});

export default SelectUsersForAssessmentLists;