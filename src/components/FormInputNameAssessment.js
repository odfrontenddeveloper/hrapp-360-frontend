import React from "react";
import { observer } from "mobx-react";
import { assessmentConfigState, assessmentConfigStateOperations } from "../appstate/assessmentConfigState";

const FormInputNameAssessment = observer(() => {
  return (
    <div className={`staff-assessment-place-punkts input-name-assessment${assessmentConfigState.selectedPunkt == 1 ? '' : ' display-none'}`}>
      <form className='standart-form vertical-form form-center night-mode-form-bg'>
        <label className='form-container-label night-mode-text' htmlFor='name_assessment'>Название мероприятия</label>
        <textarea
          id='name_assessment'
          className='form-container-input name-assessment-textarea night-mode-text night-mode-input'
          value={assessmentConfigState.assessmentName}
          onChange={(e) => { assessmentConfigStateOperations.changeAssessmentName(e.target.value) }}
        ></textarea>
      </form>
    </div>
  );
});

export default FormInputNameAssessment;