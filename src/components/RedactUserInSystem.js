import React, { useState } from "react";
import RedactData from "./RedactData";
import { imageLoadOperations } from "../appstate/styles_config";
import globalConfig from "../appstate/global_config";
import { userslist } from "../appstate/redactStaffState";
import { observer } from "mobx-react";
import axios from "axios";

const RedactUserInSystem = observer(props => {
  let [resRedactUserAccess, changeresRedactUserAccess] = useState(null);
  let changeUserType = (e) => {
    imageLoadOperations.openImageLoad();
    let userData = {
      changeUser: userslist.redactId,
      typeModerator: e.target.checked
    };
    axios({
      method: 'patch',
      url: globalConfig.serverAddress + '/changeusertype',
      data: userData
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          changeresRedactUserAccess(null);
          userslist.list = userslist.list.map((el, i) => {
            if (el.id == userData.changeUser) {
              if (response.data.content == true) {
                el.type = 2;
                el.redactforms = 0;
                el.redactstaff = 0;
                el.makeevents = 0;
                props.changeCheckRedactforms(false);
                props.changeCheckRedactstaff(false);
                props.changeCheckMakeevents(false);
                userslist.isModer = true;
                props.changeIsModer(true);
              }
              else {
                el.type = 3;
                userslist.isModer = false;
                props.changeIsModer(false);
              }
            }
            return el;
          });
        }
        else {
          changeresRedactUserAccess(response.data.content);
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  }

  let changeModeratorAccessPunkt = (checkedStatus, functional) => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'patch',
      url: globalConfig.serverAddress + `/changemoderatoraccesspunkts`,
      data: {
        checkedstatus: checkedStatus,
        functional: functional,
        moderator: userslist.redactId
      }
    }).then(response => {
      let redactUser = null;
      let redactUserData = userslist.list.some((el, i, arr) => {
        if (userslist.redactId == el.id) {
          redactUser = el;
          if (functional == 'redactforms') {
            el.redactforms = response.data.status ? checkedStatus : !checkedStatus;
            props.changeCheckRedactforms(response.data.status ? checkedStatus : !checkedStatus);
          }
          if (functional == 'redactstaff') {
            el.redactstaff = response.data.status ? checkedStatus : !checkedStatus;
            props.changeCheckRedactstaff(response.data.status ? checkedStatus : !checkedStatus);
          }
          if (functional == 'makeevents') {
            el.makeevents = response.data.status ? checkedStatus : !checkedStatus;
            props.changeCheckMakeevents(response.data.status ? checkedStatus : !checkedStatus);
          }
          return true;
        }
      });

      if (response.data.status == false) {
        changeresRedactUserAccess(response.data.content);
      }

      imageLoadOperations.closeImageLoad();
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      if (functional == 'redactforms') {
        props.refCheckRedactforms.current.checked = !checkedStatus;
      }
      if (functional == 'redactstaff') {
        props.refCheckRedactstaff.current.checked = !checkedStatus;
      }
      if (functional == 'makeevents') {
        props.refCheckMakeevents.current.checked = !checkedStatus;
      }
      console.log(error);
    });
  }

  let onCloseForm = () => {
    changeresRedactUserAccess(null);
  }

  return (
    <RedactData forRef={props.refRedactUserForm} onCloseForm={onCloseForm}>
      <form className='standart-form vertical-form form-center night-mode-form-bg-lite'>
        <div className='redact-staff-place-display-info-block-avatar'>
          <div className='redact-staff-place-display-info-block-avatar-image' ref={props.refImage}></div>
        </div>
        <div className='form-container form-container-column redact-users-staff-info-text night-mode-text'>{props.userSurename}</div>
        <div className='form-container form-container-column redact-users-staff-info-text night-mode-text'>{props.userName}</div>
        <div className='form-container form-container-column redact-users-staff-info-text night-mode-text'>{props.userPatronymic}</div>
        <div className='button-container'>
          {
            !userslist.isAdmin ? (
              <div className='checkbox-container checkbox-container-border w-100'>
                <div className='standart-checkbox-label-text night-mode-text'>Модератор</div>
                <label
                  className='standart-checkbox-label standart-checkbox-label-night-mode'
                  htmlFor='changeModeratorAccess'
                >
                  <input
                    type='checkbox'
                    id='changeModeratorAccess'
                    onChange={(e) => { changeUserType(e) }}
                    checked={props.isModer}
                  />
                  <div className='checbox-place'></div>
                </label>
              </div>
            ) : (null)
          }
        </div>
        <div id='redactModeratorAccessList' style={
          {
            display: userslist.isModer ? ('block') : ('none')
          }
        }>
          <div className='button-container'>
            <div className='checkbox-container checkbox-container-border w-100'>
              <div className='standart-checkbox-label-text night-mode-text'>
                Редактирование списка должностей и компетенций
                </div>
              <label className='standart-checkbox-label standart-checkbox-label-night-mode' htmlFor='redactforms'>
                <input type='checkbox' id='redactforms' checked={props.checkRedactforms} onChange={(e) => { changeModeratorAccessPunkt(e.target.checked, 'redactforms') }} />
                <div className='checbox-place'></div>
              </label>
            </div>
          </div>
          <div className='button-container'>
            <div className='checkbox-container checkbox-container-border w-100'>
              <div className='standart-checkbox-label-text night-mode-text'>
                Редактирование перcонала
                </div>
              <label className='standart-checkbox-label standart-checkbox-label-night-mode' htmlFor='redactstaff'>
                <input type='checkbox' id='redactstaff' checked={props.checkRedactstaff} onChange={(e) => { changeModeratorAccessPunkt(e.target.checked, 'redactstaff') }} />
                <div className='checbox-place'></div>
              </label>
            </div>
          </div>
          <div className='button-container'>
            <div className='checkbox-container checkbox-container-border w-100'>
              <div className='standart-checkbox-label-text night-mode-text'>
                Создание мероприятий по оценке
              </div>
              <label className='standart-checkbox-label standart-checkbox-label-night-mode' htmlFor='makeevents'>
                <input type='checkbox' id='makeevents' checked={props.checkMakeevents} onChange={(e) => { changeModeratorAccessPunkt(e.target.checked, 'makeevents') }} />
                <div className='checbox-place'></div>
              </label>
            </div>
          </div>
        </div>
      </form>
      <div id='res-redact-user-access' className='dangerColor result-container res-form-center'>{resRedactUserAccess}</div>
    </RedactData>
  );
});

export default RedactUserInSystem;