import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import listsofdata from "../appstate/listsofdata.js";
import ItemCompetence from "./ItemCompetence.js";
import SearchWPCompetenceForm from "./SearchWPCompetenceForm.js";
import DeleteWorkPositionCompetenceForm from "./DeleteWorkPositionCompetenceForm.js";
import AddRenameWorkPositionForm from "./AddRenameWorkPositionForm.js";
import { wpCompetenciesState } from "../appstate/wpCompetenciesState.js";

const RedactCompetencies = observer(() => {
  let formAddCompetence = React.createRef();
  let formDeleteCompetence = React.createRef();
  let formRenameCompetence = React.createRef();
  let refInputName = React.createRef();
  let refRedactName = React.createRef();
  let counterDisplayItemsInList = 0;

  let [deleteIdCompetence, changedeleteIdCompetence] = useState('');
  let [deleteNameCompetence, changedeleteNameCompetence] = useState('');
  let [redactIdCompetence, changeredactIdCompetence] = useState('');
  let [redactNameCompetence, changeredactNameCompetence] = useState('');
  let [counterDisplayItems, changecounterDisplayItems] = useState(30);

  useEffect(() => {
    wpCompetenciesState.redactCompetenciesListForWP = false;
    wpCompetenciesState.redactWPId = false;
  }, []);

  let setBaseCounterDisplayItems = () => {
    changecounterDisplayItems(30);
  }

  let incrementCounterDisplayItems = () => {
    changecounterDisplayItems(counterDisplayItems + 30);
  }

  let openAddCompetenceForm = () => {
    formAddCompetence.current.style.display = 'flex';
  }

  let openDeleteCompetenceForm = (id, name) => {
    formDeleteCompetence.current.style.display = 'flex';
    changedeleteIdCompetence(id);
    changedeleteNameCompetence(name);
  }

  let openRenameCompetenceForm = (id, name) => {
    formRenameCompetence.current.style.display = 'flex';
    refRedactName.current.value = name;
    changeredactIdCompetence(id);
    changeredactNameCompetence(name);
  }

  return (
    <div className='work-positions-area-col competencies-area-workpositions night-mode-form-bg'>
      <div className='work-positions-area-col-header night-mode-text'>Список компетенций</div>
      <SearchWPCompetenceForm
        setBaseCounter={setBaseCounterDisplayItems}
        field={'competence'}
      />
      <div className='work-positions-area-col-list night-mode-input'>
        <div
          id='list-of-competencies'
          className='list-of-items'
        >
          {
            listsofdata.listOfCompetencies.length > 0 ? (
              listsofdata.listOfCompetencies.map((el, i) => {
                if (wpCompetenciesState.searchStateFilter && wpCompetenciesState.redactCompetenciesListForWP) {
                  if (!wpCompetenciesState.connectsList.includes(el.id)) {
                    return;
                  }
                }
                if (counterDisplayItemsInList == counterDisplayItems) {
                  counterDisplayItemsInList += 1;
                  return (
                    <div>
                      <button
                        key={'show-more-items-button'}
                        className='mini-functional-button mini-fiolet-button mini-more-button'
                        onClick={() => { incrementCounterDisplayItems() }}
                      ></button>
                    </div>
                  );
                }
                if (counterDisplayItemsInList > counterDisplayItems) {
                  return;
                }
                counterDisplayItemsInList += 1;
                return (
                  <ItemCompetence
                    key={'item-competence-' + el.id}
                    counterIterator={i}
                    competenceInfo={el}
                    onDelete={openDeleteCompetenceForm}
                    onRename={openRenameCompetenceForm}
                  />
                );
              })
            ) : (
                <div className='null-list-message night-mode-text'>Ничего не найдено.</div>
              )
          }
        </div>
      </div>
      <button
        type='button'
        className='mini-functional-button mini-success-button mini-add-button form-list-add-button'
        onClick={() => { openAddCompetenceForm() }}
      ></button>
      <AddRenameWorkPositionForm
        field={'competence'}
        modeAdd={true}
        refForm={formAddCompetence}
        refInputName={refInputName}
      />
      <AddRenameWorkPositionForm
        field={'competence'}
        modeAdd={false}
        refForm={formRenameCompetence}
        redactIdCompetence={redactIdCompetence}
        redactNameCompetence={redactNameCompetence}
        refInputName={refRedactName}
      />
      <DeleteWorkPositionCompetenceForm
        field={'competence'}
        refForm={formDeleteCompetence}
        deleteId={deleteIdCompetence}
        deleteName={deleteNameCompetence}
      />
    </div>
  );
});

export default RedactCompetencies;