import React, { useState } from "react";
import RedactData from "./RedactData";
import { stylesConfig } from "../appstate/styles_config";
import { criteriaSignsOperations } from "../appstate/criteriasignsState";

const DeleteCriterionForm = props => {
  let refDeleteButton = React.createRef();
  let [resDelete, changeresDelete] = useState(null);

  let closeDeleteCriterionForm = () => {
    props.refForm.current.style.animation = stylesConfig.animationHideElement;
    changeresDelete(null);
    setTimeout(() => {
      if (props.refForm.current) {
        props.refForm.current.style.display = 'none';
        props.refForm.current.style.animation = '';
      }
    }, stylesConfig.hideElementsTiming - 10);
  }

  let handlerDeleteForm = (e) => {
    e.preventDefault();
    criteriaSignsOperations.deleteCriterion(refDeleteButton.current.value, changeresDelete, closeDeleteCriterionForm);
  }

  let onCloseForm = () => {
    changeresDelete(null);
  }

  return (
    <RedactData forRef={props.refForm} onCloseForm={onCloseForm}>
      <form
        className='standart-form vertical-form form-center night-mode-form-bg'
        onSubmit={(e) => { handlerDeleteForm(e) }}
      >
        <div className='form-container input-container form-container-column'>
          <label
            className='form-container-label night-mode-text'
          >
            {`Подтвердите удаление.`}
          </label>
        </div>
        <div className='button-container'>
          <button
            type='submit'
            className='mini-functional-button mini-success-button mini-complete-button'
            value={props.deleteId}
            ref={refDeleteButton}
          ></button>
          <button
            type='button'
            className='mini-functional-button mini-danger-button mini-close-button'
            onClick={() => closeDeleteCriterionForm()}
          ></button>
        </div>
      </form>
      <div>{resDelete}</div>
    </RedactData>
  );
}

export default DeleteCriterionForm;