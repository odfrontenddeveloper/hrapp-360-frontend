import React, { useState } from "react";
import { observer } from "mobx-react";
import { resultsState, resultsStateOperations } from "../appstate/showResultsState.js";
import shortid from "shortid";
import ItemUserResults from "./ItemUserResults.js";
import FastSearchUsers from "./FastSearchUsers.js";

const UsersListForResults = observer((props) => {

  let [searchValue, changeSearchValue] = useState('');
  let [searchValueWP, changeSearchValueWP] = useState('');

  let getUserResults = (user_from, user_to) => {
    resultsStateOperations.openUserResults(user_from, user_to);
  }

  return (
    <div className='work-positions-area-col competencies-area-workpositions night-mode-form-bg'>
      <div className='work-positions-area-col-header night-mode-text'>Список оценённых пользователей</div>
      <FastSearchUsers
        searchValue={searchValue}
        changeSearchValue={changeSearchValue}
        searchValueWP={searchValueWP}
        changeSearchValueWP={changeSearchValueWP}
      />
      <div className='work-positions-area-col-list night-mode-input'>
        <div
          id='list-of-competencies'
          className='list-of-items'
        >
          {
            resultsState.resultsList.length > 0 ? (
              resultsState.resultsList.map(element => {
                return { user_to: element.USER_TO, wp: element.WP.name };
              }).map((element) => {
                return JSON.stringify(element);
              }).unique().map(element => {
                return JSON.parse(element);
              }).map(el => {
                if ([el.user_to.surename.toLowerCase(), el.user_to.name.toLowerCase(), el.user_to.patronymic.toLowerCase()].join(' ').indexOf(searchValue.toLowerCase()) == -1) {
                  return;
                }
                if ((el.wp ? el.wp : '').toLowerCase().indexOf(searchValueWP.toLowerCase()) == -1) {
                  return;
                }
                return (
                  <ItemUserResults user={el.user_to} wp={el.wp} getUserResults={getUserResults} key={el.user_to.id + shortid.generate()} />
                );
              })
            ) : (
                <div className='null-list-message night-mode-text'>Ничего не найдено.</div>
              )
          }
        </div>
      </div>
    </div>
  );
});

export default UsersListForResults;