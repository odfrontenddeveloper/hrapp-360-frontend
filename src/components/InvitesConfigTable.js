import React from "react";
import { observer } from "mobx-react";
import { assessmentConfigState, assessmentConfigStateOperations } from "../appstate/assessmentConfigState";
import globalConfig from "../appstate/global_config";

const InvitesConfigTable = observer(() => {
  return (
    <div className={`staff-assessment-place-punkts${assessmentConfigState.selectedPunkt == 3 ? '' : ' display-none'}`}>
      <div className='users-assessment-invites'>
        <div className='users-assessment-invites-row'>
          <div className='users-assessment-invites-row-field users-assessment-invites-row-field-head-light night-mode-form-bg-lite'></div>
          {
            assessmentConfigState.usersListForAssessment.filter(user_to => {
              return user_to.selected == true;
            }).map(user_to => {
              return (
                <div className='users-assessment-invites-row-field users-assessment-invites-row-field-head-light night-mode-form-bg-lite' key={user_to.id} title={`${user_to.surename} ${user_to.name} ${user_to.patronymic}`}>
                  <button
                    type='button'
                    className='users-assessment-invites-row-field users-assessment-invites-row-field-head-light night-mode-form-bg-lite'
                    title={`${user_to.surename} ${user_to.name} ${user_to.patronymic}`}
                    onClick={() => { assessmentConfigStateOperations.changeBackgroundLight(user_to.id) }}
                  >
                    <div className='users-assessment-invites-row-field-user-avatar'>
                      <div className='users-assessment-invites-row-field-user-avatar-block' style={{ backgroundImage: `url('${globalConfig.serverAddress}/useravatar?login=${user_to.login}')` }}></div>
                    </div>
                  </button>
                </div>
              );
            })
          }
        </div>
        {
          assessmentConfigState.usersListForAssessment.filter(user_from => {
            return user_from.selected == true;
          }).map(user_from => {
            return (
              <div className='users-assessment-invites-row' key={user_from.id}>
                <button
                  type='button'
                  className='users-assessment-invites-row-field users-assessment-invites-row-field-head-light night-mode-form-bg-lite'
                  title={`${user_from.surename} ${user_from.name} ${user_from.patronymic}`}
                  onClick={() => { assessmentConfigStateOperations.changeBackgroundLight(user_from.id) }}
                >
                  <div className='users-assessment-invites-row-field-user-avatar'>
                    <div className='users-assessment-invites-row-field-user-avatar-block' style={{ backgroundImage: `url('${globalConfig.serverAddress}/useravatar?login=${user_from.login}')` }}></div>
                  </div>
                </button>
                {
                  assessmentConfigState.usersListForAssessment.filter(user_to => {
                    return user_to.selected == true;
                  }).map(user_to => {
                    return (
                      <div className='users-assessment-invites-row-field' key={user_to.id}>
                        <button
                          type='button'
                          className={`users-assessment-invites-row-field-button night-mode-form-bg night-mode-text${
                            assessmentConfigStateOperations.checkAssessmentInvite(user_from.id, user_to.id) ? ' selected-invite-button-important' : `${
                              (assessmentConfigState.backgroundLight.includes(user_from.id) || assessmentConfigState.backgroundLight.includes(user_to.id)) ? (
                                ' background-light'
                              ) : (
                                  ''
                                )
                              }`
                            }`}
                          onClick={() => {
                            assessmentConfigStateOperations.resetUserInvite(user_from.id, user_to.id);
                          }}
                        ></button>
                      </div>
                    );
                  })
                }
              </div>
            );
          })
        }
      </div>
    </div>
  );
});

export default InvitesConfigTable;