import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { fillingAFstate, fillingAFstateOperations } from "../appstate/fillingassessmentsformsState";
import globalConfig from "../appstate/global_config";

const UserInvite = observer((props) => {

  let refImage = React.createRef();

  useEffect(() => {
    refImage.current.style.backgroundImage = `url('${globalConfig.serverAddress}/useravatar?login=${props.invite.user_to.login}')`;
  });

  return (
    <form className='redact-staff-place-display-info-block night-mode-form-bg-lite' onSubmit={(e) => { fillingAFstateOperations.getForm(e, props.invite.id) }}>
      <div className='redact-staff-place-display-info-block-info-text night-mode-text'>{
        `Вам предлагается оценить сотрудника:`
      }</div>
      <div className='redact-staff-place-display-info-block-avatar'>
        <div className='redact-staff-place-display-info-block-avatar-image' ref={refImage}></div>
      </div>
      <div className='redact-staff-place-display-info-block-info'></div>
      <div className='redact-staff-place-display-info-block-null-line night-mode-form-bg'></div>
      <div className='redact-staff-place-display-info-block-info-text night-mode-text'>{
        props.invite.user_to.surename
      }</div>
      <div className='redact-staff-place-display-info-block-info-text night-mode-text'>{
        props.invite.user_to.name
      }</div>
      <div className='redact-staff-place-display-info-block-info-text night-mode-text'>{
        props.invite.user_to.patronymic
      }</div>
      <div className='button-container'>
        <button type='submit' className='mini-functional-button mini-cyan-button mini-redact-button'></button>
      </div>
    </form>
  );
});

export default UserInvite;