import React from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
import UserAreaInfo from "./UserAreaInfo.js";
import UserAreaHeader from "./UserAreaHeader.js";
import CheckAuth from "./CheckAuth.js";
import authOperations from "../appstate/userauthOperations.js";
import { image_path } from "../appstate/global_config.js";

const UserArea = observer((props) => {
  let refArea = React.createRef();

  return (
    <div className='user night-mode-bg-block'>
      <CheckAuth />
      <div className='user-area' ref={refArea}>
        <UserAreaHeader logout={refArea} backButton={false} />
        <div className='user-area-place'>
          <UserAreaInfo />
          <div className='user-area-place-work'>
            <div className='user-menu'>
              <div className='user-menu-container'>
                {
                  ['moderator','admin'].includes(authOperations.getCookie('userType')) ? (
                  <React.Fragment>
                    <Link to='/redactstaff' className='user-menu-container-item night-mode-bg'>
                      <div className='user-menu-container-item-header'>
                        <img src={`${image_path[process.env.NODE_ENV]}/menu/users.png`} />
                      </div>
                      <div className='user-menu-container-item-name night-mode-text'>Штат сотрудников</div>
                    </Link>
                    <Link to='/redactusersgroups' className='user-menu-container-item night-mode-bg'>
                      <div className='user-menu-container-item-header'>
                        <img src={`${image_path[process.env.NODE_ENV]}/menu/users.png`} />
                      </div>
                      <div className='user-menu-container-item-name night-mode-text'>Управление подразделениями</div>
                    </Link>
                    <Link to='/workpositions' className='user-menu-container-item night-mode-bg'>
                      <div className='user-menu-container-item-header'>
                        <img src={`${image_path[process.env.NODE_ENV]}/menu/constructor.png`} />
                      </div>
                      <div className='user-menu-container-item-name night-mode-text'>Списки должностей и компетенций</div>
                    </Link>
                    <Link to='/criteriasigns' className='user-menu-container-item night-mode-bg'>
                      <div className='user-menu-container-item-header'>
                        <img src={`${image_path[process.env.NODE_ENV]}/menu/constructor.png`} />
                      </div>
                      <div className='user-menu-container-item-name night-mode-text'>Списки признаков и критериев оценки</div>
                    </Link>
                    <Link to='/staffassessment' className='user-menu-container-item night-mode-bg'>
                      <div className='user-menu-container-item-header'>
                        <img src={`${image_path[process.env.NODE_ENV]}/menu/tests.png`} />
                      </div>
                      <div className='user-menu-container-item-name night-mode-text'>Создать мероприятие по оценке</div>
                    </Link>
                  </React.Fragment>
                  ) : (null)
                }
                <Link to='/getinvites' className='user-menu-container-item night-mode-bg'>
                  <div className='user-menu-container-item-header'>
                    <img src={`${image_path[process.env.NODE_ENV]}/menu/tests.png`} />
                  </div>
                  <div className='user-menu-container-item-name night-mode-text'>Мои приглашения</div>
                </Link>
                {
                  ['moderator','admin'].includes(authOperations.getCookie('userType')) ? (
                  <Link to='/results' className='user-menu-container-item night-mode-bg'>
                    <div className='user-menu-container-item-header'>
                      <img src={`${image_path[process.env.NODE_ENV]}/menu/results.png`} />
                    </div>
                    <div className='user-menu-container-item-name night-mode-text'>Анализ результатов</div>
                  </Link>
                  ) : (null)
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

export default UserArea;