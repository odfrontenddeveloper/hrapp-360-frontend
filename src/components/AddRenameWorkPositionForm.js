import React, { useState } from "react";
import RedactData from "./RedactData";
import globalConfig from "../appstate/global_config";
import { imageLoadOperations } from "../appstate/styles_config.js";
import { observer } from "mobx-react";
import axios from "axios";
import listsofdata from "../appstate/listsofdata";
import { ugstate } from "../appstate/usersGroupsState";

const AddRenameWorkPositionForm = observer((props) => {
  let refButton = React.createRef();

  let [resAddRename, changeresAddRename] = useState(null);
  let [formResColor, changeformResColor] = useState(null);

  let forms_content = {
    add_wp: 'Введите название должности для создания:',
    add_competence: 'Введите название компетенции для создания:',
    add_ug: 'Введите название группы пользователей для создания:',
    rename_wp: 'Введите новое название должности:',
    rename_competence: 'Введите новое название компетенции:',
    rename_ug: 'Введите новое название группы пользователей:'
  };

  let pathCollection = {
    'wp': {
      add: '/addwp',
      rename: '/renamewp'
    },
    'competence': {
      add: '/addcompetence',
      rename: '/renamecompetence'
    },
    'ug': {
      add: '/addug',
      rename: '/renameug'
    }
  };

  let redactIdList = {
    'wp': props.redactIdWP,
    'competence': props.redactIdCompetence,
    'ug': props.redactIdUG
  };
  
  let sendParamsAdd = {
    'wp': 'nameWP',
    'competence': 'nameCompetence',
    'ug': 'nameUG'
  };

  let sendParamsRedact = {
    'wp': 'name',
    'competence': 'nameCompetence',
    'ug': 'nameUG'
  };

  let handlerAddWPForm = (e) => {
    e.preventDefault();
    imageLoadOperations.openImageLoad();
    if (props.modeAdd) {
      let data = {};
      data[sendParamsAdd[props.field]] = props.refInputName.current.value;
      axios({
        method: 'post',
        url: globalConfig.serverAddress + pathCollection[props.field].add,
        data: data
      }).then(response => {
        imageLoadOperations.closeImageLoad();
        if (response.status >= 200 && response.status < 300) {
          if (response.data.status == true) {
            if (props.field == 'wp') {
              listsofdata.listOfWP.push(response.data.content);
              changeformResColor(true);
              changeresAddRename('Должность создана.');
            }
            else if (props.field == 'competence') {
              listsofdata.listOfCompetencies.push(response.data.content);
              changeformResColor(true);
              changeresAddRename('Компетенция создана.');
            }
            else if (props.field == 'ug') {
              ugstate.listOfUG.push(response.data.content);
              changeformResColor(true);
              changeresAddRename('Группа пользователей создана.');
            }
          }
          else {
            changeformResColor(false);
            changeresAddRename(response.data.content);
          }
        }
      }).catch(error => {
        imageLoadOperations.closeImageLoad();
        console.log(error);
      });
    }
    else {
      let data = {};

      data.id = refButton.current.value;
      data[sendParamsRedact[props.field]] = props.refInputName.current.value;

      axios({
        method: 'patch',
        url: globalConfig.serverAddress + pathCollection[props.field].rename,
        data: data
      }).then(response => {
        imageLoadOperations.closeImageLoad();
        if (response.status >= 200 && response.status < 300) {
          imageLoadOperations.closeImageLoad();
          if (response.data.status == true) {
            if (props.field == 'wp') {
              listsofdata.listOfWP = listsofdata.listOfWP.map(function (el, i) {
                if (el.id == data.id) {
                  el.name = data.name;
                }
                return el;
              });
              changeformResColor(true);
              changeresAddRename('Название должности изменено.');
            }
            else if (props.field == 'competence') {
              listsofdata.listOfCompetencies = listsofdata.listOfCompetencies.map(function (el, i) {
                if (el.id == data.id) {
                  el.name = data.nameCompetence;
                }
                return el;
              });
              changeformResColor(true);
              changeresAddRename('Название компетенции изменено.');
            }
            else if (props.field == 'ug') {
              ugstate.listOfUG = ugstate.listOfUG.map(function (el, i) {
                if (el.id == data.id) {
                  el.name = data.nameUG;
                }
                return el;
              });
              changeformResColor(true);
              changeresAddRename('Название группы пользователей изменено.');
            }
          }
          else {
            changeformResColor(false);
            changeresAddRename(response.data.content);
          }
        }
      }).catch(error => {
        imageLoadOperations.closeImageLoad();
        console.log(error);
      });
    }
  }

  let onCloseForm = () => {
    changeresAddRename(null);
  }

  return (
    <RedactData forRef={props.refForm} onCloseForm={onCloseForm}>
      <form
        className='standart-form vertical-form form-center night-mode-form-bg'
        onSubmit={(e) => { handlerAddWPForm(e) }}
      >
        <div className='form-container input-container form-container-column'>
          <label
            className='form-container-label night-mode-text'
            htmlFor={`${props.modeAdd ? 'add_' : 'rename_'}${props.field}`}
          >
            {forms_content[`${props.modeAdd ? 'add_' : 'rename_'}${props.field}`]}
          </label>
          <input
            id={`${props.modeAdd ? 'add_' : 'rename_'}${props.field}`}
            className='form-container-input night-mode-text night-mode-input'
            type='text'
            ref={props.refInputName}
          />
        </div>
        <div className='button-container'>
          <button
            type='submit'
            className={props.modeAdd ? (
              'mini-functional-button mini-success-button mini-add-button'
            ) : (
                'mini-functional-button mini-success-button mini-complete-button'
              )}
            value={!props.modeAdd ? (redactIdList[props.field]) : (null)}
            ref={refButton}
          ></button>
        </div>
      </form>
      <div className={`result-container res-form-center ${formResColor ? 'successColor' : 'dangerColor'}`} >
        {resAddRename}
      </div>
    </RedactData>
  );
});

export default AddRenameWorkPositionForm;