import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { assessmentConfigState, assessmentConfigStateOperations } from "../appstate/assessmentConfigState";
import SelectUsersForAssessmentLists from "./SelectUsersForAssessmentLists";

const SelectUsersForAssessment = observer(() => {

  useEffect(() => {
    assessmentConfigStateOperations.getUsersListForAssessment();
  }, []);

  return (
    <div className={`staff-assessment-place-punkts${assessmentConfigState.selectedPunkt == 2 ? '' : ' display-none'}`}>
      <div className='users-list'>
        <div className='users-list-area'>
          <SelectUsersForAssessmentLists selectedUsers={true}/>
          <SelectUsersForAssessmentLists selectedUsers={false} addfromug={true}/>
        </div>
      </div>
    </div>
  );
});

export default SelectUsersForAssessment;