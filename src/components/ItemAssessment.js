import React from "react";
import { observer } from "mobx-react";
import globalConfig from "../appstate/global_config.js";
import axios from "axios";
import { resultsState, resultsStateOperations } from "../appstate/showResultsState.js";

const ItemWorkPosition = observer(props => {
  let openRedactConnectionsList = (id) => {
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/assessmentresults?assessment=${id}`
    }).then(response => {
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status) {
          resultsStateOperations.changeState(id, response.data.content);
        }
      }
    }).catch(error => {
      console.log(error);
    });
  }  

  return (
    <div
      id={'list-of-wp-id-' + props.assessmentInfo.id}
      className='item-in-list'
      style={{ animation: `${1 * 0.4}s showBlock forwards` }}
    >
      <div className='item-in-list-data'>
        <div className='item-in-list-data-block night-mode-text'>{props.assessmentInfo.name}</div>
      </div>
      <div className='item-in-list-buttons-container'>
        <button
          type='button'
          className={`mini-functional-button mini-${props.assessmentInfo.id == resultsState.showId ? 'red-orange' : 'primary'}-button mini-settings-button`}
          onClick={(e) => { openRedactConnectionsList(props.assessmentInfo.id) }}
        ></button>
        <button
          type='button'
          className='mini-functional-button mini-warning-button mini-redact-button'
          onClick={(e) => { props.onRename(props.assessmentInfo.id, props.assessmentInfo.name) }}
        ></button>
        <button
          type='button'
          className='mini-functional-button mini-danger-button mini-delete-button'
          onClick={(e) => { props.onDelete(props.assessmentInfo.id, props.assessmentInfo.name) }}
        ></button>
      </div>
    </div>
  );
});

export default ItemWorkPosition;