import React, { useEffect, useState } from "react";
import AddUserToSystem from "./AddUserToSystem";
import RedactUserInSystem from "./RedactUserInSystem";
import DeleteUserFromSystem from "./DeleteUserFromSystem";
import CheckAuth from "./CheckAuth";
import UserAreaHeader from "./UserAreaHeader";
import { imageLoadOperations } from "../appstate/styles_config";
import { observer } from "mobx-react";
import globalConfig from "../appstate/global_config";
import authOperations from "../appstate/userauthOperations";
import ItemUser from "./ItemUser";
import SearchInStaff from "./SearchInStaff";
import RedactUserWP from "./RedactUserWP";
import { userslist } from "../appstate/redactStaffState";
import axios from "axios";

const RedactStaff = observer(() => {
  let refArea = React.createRef();
  let refAddUserForm = React.createRef();
  let refDeleteUserForm = React.createRef();
  let refDeleteButton = React.createRef();
  let refRedactUserForm = React.createRef();
  let refRedactUserWPForm = React.createRef();
  let redactUserWPName = React.createRef();
  let infoDeleteUser = React.createRef();
  let refWPList = React.createRef();
  let refImage = React.createRef();
  let redactUserWPLogin = React.createRef();

  let [checkRedactforms, changeCheckRedactforms] = useState(false);
  let [checkRedactstaff, changeCheckRedactstaff] = useState(false);
  let [checkMakeevents, changeCheckMakeevents] = useState(false);
  let [userwpformheader, setuserwpformheader] = useState(false)

  let [userName, changeuserName] = useState(null);
  let [userSurename, changeuserSurename] = useState(null);
  let [userPatronymic, changeuserPatronymic] = useState(null);
  let [stateModerator, changestateModerator] = useState(false);

  let [searchByFullname, changeSearchByFullname] = useState('');
  let [userwp, setuserwp] = useState(false)

  let uploadUsersData = () => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/getuserslist?admin=${authOperations.getCookie('userLogin')}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          userslist.list = response.data.content;
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  }

  useEffect(() => {
    uploadUsersData();
  }, []);

  let openRedactUserForm = async (userData) => {
    userslist.redactId = userData.id;
    refImage.current.style.backgroundImage = `url('${globalConfig.serverAddress}/useravatar?login=${userData.login}')`;
    changeuserName(userData.name);
    changeuserSurename(userData.surename);
    changeuserPatronymic(userData.patronymic);

    let redactUser = null;
    userslist.list.some((el, i, arr) => {
      if (userslist.redactId == el.id) {
        redactUser = el;
        return true;
      }
    });
    userslist.isAdmin = redactUser.type == 1;
    userslist.isModer = redactUser.type == 2;

    if (userslist.isModer) {
      changeCheckRedactforms(redactUser.redactforms);
      changeCheckRedactstaff(redactUser.redactstaff);
      changeCheckMakeevents(redactUser.makeevents);
    }

    changestateModerator(userslist.isModer);
    refRedactUserForm.current.style.display = 'flex';
  }

  let openDeleteUserForm = (id, login) => {
    refDeleteButton.current.value = id;
    infoDeleteUser.current.innerHTML = `Вы действительно хотите удалить пользователя "${login}"?`;
    refDeleteUserForm.current.style.display = 'flex';
  }

  let openAddUserForm = () => {
    refAddUserForm.current.style.display = 'flex';
  }

  let wpListForRedactUserWPList = async (wp) => {
    await axios({
      method: 'get',
      url: `${globalConfig.serverAddress}/searchwp?nameWP=&sortabc=${false}`
    }).then(response => {
      if (response.status >= 200 && response.status < 300) {
        let canSelectWP = false;
        if (response.data.content.length > 0) {
          let selectedWPForUser = response.data.content.filter((el) => {
            return el.name == wp;
          });
          // if (redactUserWPName.current) {
            if (selectedWPForUser.length > 0) {
              canSelectWP = selectedWPForUser[0].name;
              setuserwp(selectedWPForUser[0].name)
            }
            else {
              setuserwp('Должность не указана')
            }
          // }
        }
        userslist.wplistforoptions = response.data.content.map((el) => {
          let isChecked = false;
          if (canSelectWP) {
            if (canSelectWP == el.name) {
              isChecked = true;
            }
          }
          return {
            ...el,
            ch: wp == el.name
          };
        });
      }
    }).catch(error => {
      console.log(error);
    });
    return;
  }

  let openRedactUserWPForm = async (wp, id, login) => {
    refRedactUserWPForm.current.style.display = 'flex';
    userslist.redactId = id;
    await wpListForRedactUserWPList(wp);
    setuserwpformheader(`Выбор должности пользователя ${login}`)
  }

  let changeIsModer = (value) => {
    changestateModerator(value);
  }

  return (
    <div className='redact-staff-place night-mode-bg-block' ref={refArea}>
      <CheckAuth />
      <UserAreaHeader
        logout={refArea}
        backButton={true}
      />
      <div className='redact-staff-place-search night-mode-form-bg'>
        <SearchInStaff
          fullname={searchByFullname}
          changeFullname={changeSearchByFullname}
        />
      </div>
      <div className='redact-staff-place-display-info'>
        <div className='fixed-functional-buttons-block'>
          <div className='fixed-functional-buttons-block-buttons'>
            <button
              type='button'
              className='mini-functional-button mini-add-button mini-success-button'
              onClick={() => { openAddUserForm() }}
            ></button>
          </div>
        </div>
        {
          userslist.list.map((el, i) => {
            if([el.surename.toLowerCase(), el.name.toLowerCase(), el.patronymic.toLowerCase()].join(' ').indexOf(searchByFullname.toLowerCase()) == -1){
              return;
            }
            return (
              <ItemUser
                key={el.id}
                counter={i}
                userData={el}
                onDelete={openDeleteUserForm}
                onRedact={openRedactUserForm}
                onRedactWP={openRedactUserWPForm}
              />
            );
          })
        }
      </div>
      <AddUserToSystem
        refForm={refAddUserForm}
      />
      <RedactUserWP
        refRedactUserWPForm={refRedactUserWPForm}
        refWPList={refWPList}
        redactUserWPName={redactUserWPName}
        redactUserWPLogin={redactUserWPLogin}
        userwp={userwp}
        userwpformheader={userwpformheader}
      />
      <RedactUserInSystem
        refRedactUserForm={refRedactUserForm}
        checkRedactforms={checkRedactforms}
        checkRedactstaff={checkRedactstaff}
        checkMakeevents={checkMakeevents}
        changeCheckRedactforms={changeCheckRedactforms}
        changeCheckRedactstaff={changeCheckRedactstaff}
        changeCheckMakeevents={changeCheckMakeevents}
        userName={userName}
        userSurename={userSurename}
        userPatronymic={userPatronymic}
        isModer={stateModerator}
        changeIsModer={changeIsModer}
        refImage={refImage}
      />
      <DeleteUserFromSystem
        refFormDelete={refDeleteUserForm}
        refDeleteButton={refDeleteButton}
        infoDeleteUser={infoDeleteUser}
      />
    </div>
  );
});

export default RedactStaff;