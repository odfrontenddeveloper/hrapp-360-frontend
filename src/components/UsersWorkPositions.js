import React, { createRef } from "react";
import { observer } from "mobx-react";
import UserAreaHeader from "./UserAreaHeader.js";
import CheckAuth from "./CheckAuth.js";
import RedactWorkPositions from "./RedactWorkPositions.js";
import RedactCompetencies from "./RedactCompetencies.js";

const UsersWorkPositions = observer(() => {
  let refArea = createRef();

  return (
    <div ref={refArea} className='work-positions night-mode-bg-block'>
      <CheckAuth />
      <UserAreaHeader
        logout={refArea}
        backButton={true}
      />
      <div className='work-positions-area'>
        <RedactWorkPositions />
        <RedactCompetencies />
      </div>
    </div>
  );
});

export default UsersWorkPositions;