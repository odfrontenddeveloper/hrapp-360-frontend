import React from "react";
import { assessmentConfigState, assessmentConfigStateOperations, assessmentConfigStoreOperations, assessmentConfigStore } from "../appstate/assessmentConfigState";
import { observer } from "mobx-react";

const ConfigTableMenu = observer(() => {
  return (
    <div className='staff-assessment-place-menu night-mode-form-bg'>
      <div className='staff-assessment-place-menu-store-navigator'>
        <button type='button' className={`staff-assessment-place-menu-store-navigator-button staff-assessment-place-menu-store-navigator-button-${(assessmentConfigState.storeCounter - 1) >= 0 ? `enabled` : `disabled`}`} onClick={() => { assessmentConfigStoreOperations.stepInStore(assessmentConfigState.storeCounter - 1) }}></button>
        <button type='button' className={`staff-assessment-place-menu-store-navigator-button staff-assessment-place-menu-store-navigator-button-${(assessmentConfigState.storeCounter < assessmentConfigStore.length - 1) ? `enabled` : `disabled`}`} onClick={() => { assessmentConfigStoreOperations.stepInStore(assessmentConfigState.storeCounter + 1) }}></button>
      </div>
      <div className='staff-assessment-place-menu-punkt night-mode-text'>
        <span>Вид</span>
        <div className='staff-assessment-place-menu-punkt-list'>
          <button className='staff-assessment-place-menu-punkt-list-item night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.selectAll(false) }}>Отменить все выделения</button>
          <button className='staff-assessment-place-menu-punkt-list-item night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.selectAll(true) }}>Выделить все</button>
        </div>
      </div>
      <div className='staff-assessment-place-menu-punkt night-mode-text'>
        <span>Модели</span>
        <div className='staff-assessment-place-menu-punkt-list'>
          <button className='staff-assessment-place-menu-punkt-list-item night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.assessmentYourself(true) }}>Добавить модель "Самооценка"</button>
          <button className='staff-assessment-place-menu-punkt-list-item night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.assessmentAllToAll(true) }}>Добавить перекрёстную оценку</button>
          <button className='staff-assessment-place-menu-punkt-list-item night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.assessmentHorizontal(true) }}>Добавить модель "Горизонталь"</button>
          <button className='staff-assessment-place-menu-punkt-list-item border-bottom night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.assessmentVertical(true) }}>Добавить модель "Вертикаль"</button>
          <button className='staff-assessment-place-menu-punkt-list-item  night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.assessmentYourself(false) }}>Удалить модель "Самооценка"</button>
          <button className='staff-assessment-place-menu-punkt-list-item night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.assessmentAllToAll(false) }}>Удалить перекрёстную оценку</button>
          <button className='staff-assessment-place-menu-punkt-list-item night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.assessmentHorizontal(false) }}>Удалить модель "Горизонталь"</button>
          <button className='staff-assessment-place-menu-punkt-list-item border-bottom night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.assessmentVertical(false) }}>Удалить модель "Вертикаль"</button>
        </div>
      </div>
      <div className='staff-assessment-place-menu-punkt night-mode-text'>
        <span>Операции</span>
        <div className='staff-assessment-place-menu-punkt-list'>
          <button className='staff-assessment-place-menu-punkt-list-item night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.emptyInvites() }}>Удалить все приглашения</button>
          <button className='staff-assessment-place-menu-punkt-list-item night-mode-form-bg night-mode-text' onClick={() => { assessmentConfigStateOperations.cleanInvites() }}>Удалить неактивных пользователей</button>
        </div>
      </div>
    </div>
  );
});

export default ConfigTableMenu;