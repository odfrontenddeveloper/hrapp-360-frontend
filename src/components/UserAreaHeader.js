import React, { createRef } from "react";
import { stylesConfig } from "../appstate/styles_config.js";
import authOperations from "../appstate/userauthOperations.js";
import { Link } from "react-router-dom";
import { observer } from "mobx-react";
import { image_path } from "../appstate/global_config.js";

const UserAreaHeader = observer((props) => {
  let refCheck = createRef();

  let logout = () => {
    props.logout.current.style.animation = stylesConfig.animationHideElement;
    setTimeout(() => {
      authOperations.logOut();
    }, stylesConfig.hideElementsTiming);
  }

  let handlerChangeNightMode = (e) => {
    stylesConfig.nightMode = e.target.checked ? true : false;
    document.cookie = 'nightMode=' + (stylesConfig.nightMode ? 'on' : 'off');
  }

  return (
    <div className='user-area-header night-mode-bg'>
      <div className='user-area-header-logo'>
        <div className='user-area-header-section-image-container'>
          <img src={`${image_path[process.env.NODE_ENV]}/logo/logo-40.png`} />
        </div>
      </div>
      <div className='user-area-header-section-func-panel'>
        <div className='button-container'>
          <div className='checkbox-container'>
            <div className='standart-checkbox-label-text night-mode-text'>Ночной режим</div>
            <label className='standart-checkbox-label standart-checkbox-label-night-mode' htmlFor='enableNightMode'>
              <input type='checkbox' id='enableNightMode' checked={stylesConfig.nightMode} onChange={(e) => { handlerChangeNightMode(e) }} />
              <div className='checbox-place night-mode-control'></div>
            </label>
          </div>
        </div>
      </div>
      <div className='user-area-header-section-button-panel'>
        {
          props.backButton ? <Link className='functional-button primary-button back-button' to='/dashboard'></Link> : false
        }
        <button type='button' className='functional-button red-orange-button logout-button' ref={refCheck} onClick={() => { logout() }} title='Выход'></button>
      </div>
    </div>
  );
});

export default UserAreaHeader;