import React from "react";
import { observer } from "mobx-react";
import { fillingAFstate, fillingAFstateOperations } from "../appstate/fillingassessmentsformsState";
import UserInvite from "./UserInvite";
import shortid from "shortid";

const GetInvitesPage = observer(() => {
  return (
    <div className={`staff-assessment-place-punkts${fillingAFstate.selectedPunkt == 1 ? '' : ' display-none'}`}>
      <div className='invites-list'>
        {
          fillingAFstate.userInvites.length > 0 ? (
            fillingAFstate.userInvites.map(invite => {
              return (
                <UserInvite invite={invite} key={invite.user_to.login + shortid.generate()} />
              );
            })
          ) : (
            <div className='null-list-message night-mode-text'>У вас пока что нету новых приглашений.</div>
          )
        }
      </div>
    </div>
  );
});

export default GetInvitesPage;