import React, { useState } from "react";
import { observer } from "mobx-react";
import { criteriaSignsState } from "../appstate/criteriasignsState.js";
import Itemcriterion from "./Itemcriterion.js";

const ChangeCriteriaForms = observer(props => {
  let [searchCriteriaValue, changeSearchCriteriaValue] = useState('');

  return (
    <React.Fragment>
      <div className='border-right-buttons-block'>
        <button type='button' className='mini-functional-button mini-success-button mini-add-button' onClick={() => { props.openAddCriteriaForm() }}></button>
      </div>
      <div className='night-mode-text'>{`Вы редактируете компетенцию \"${criteriaSignsState.redactCompetenceModeName}\".`}</div>
      <form className='standart-form horizontal-form night-mode-form-bg'>
        <div className='form-container input-container form-container-row'>
          <label
            className='form-container-label night-mode-text'
            htmlFor='searchCriteria'
          >Поиск</label>
          <input
            id='searchCriteria'
            className='form-container-input night-mode-text night-mode-input'
            type='text'
            value={searchCriteriaValue}
            onChange={(e) => { changeSearchCriteriaValue(e.target.value) }}
          />
        </div>
      </form>
      <div className='redact-criteria-signs-place-menu-change-signs-area-list night-mode-input'>
        <div className='redact-criteria-signs-place-menu-change-signs-area-list-items'>
          {
            criteriaSignsState.criterialist.map((criterionInfo, i) => {
              if (criterionInfo.criterion.text.indexOf(searchCriteriaValue) == -1) {
                return;
              }
              return (
                <Itemcriterion
                  criterionInfo={criterionInfo}
                  iteratorCount={i + 1}
                  key={criterionInfo.criterion.id}
                  openRedactCriteriaForm={props.openRedactCriteriaForm}
                  openDeleteCriteriaForm={props.openDeleteCriteriaForm}
                />
              );
            })
          }
        </div>
      </div>
    </React.Fragment>
  );
});

export default ChangeCriteriaForms;