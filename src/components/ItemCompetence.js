import React from "react";
import { observer } from "mobx-react";
import axios from "axios";
import { imageLoadOperations } from "../appstate/styles_config.js";
import { wpCompetenciesState } from "../appstate/wpCompetenciesState.js";
import globalConfig from "../appstate/global_config.js";

const ItemCompetence = observer(props => {
  let changeCompetenceConnectToWP = (value) => {
    imageLoadOperations.openImageLoad();
    let data = {
      connect: value,
      idwp: wpCompetenciesState.redactWPId,
      idcompetence: props.competenceInfo.id
    };
    axios({
      method: 'post',
      url: globalConfig.serverAddress + '/changecompetenceconnecttowp',
      data: data
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (value == true) {
          wpCompetenciesState.connectsList.push(response.data.content.competence);
        }
        else {
          wpCompetenciesState.connectsList = wpCompetenciesState.connectsList.filter((el, i) => {
            return(el != data.idcompetence);
          });
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  }

  let handlerChangeCheckBox = (e) => {
    if (e.target.checked) {
      changeCompetenceConnectToWP(true);
    }
    else {
      changeCompetenceConnectToWP(false);
    }
  }

  return (
    <div
      id={'list-of-competence-id-' + props.competenceInfo.id}
      className='item-in-list'
      style={{ animation: `${1 * 0.4}s showBlock forwards` }}
      data-value={wpCompetenciesState.connectsList.includes(props.competenceInfo.id) ? true : false}
    >
      <div className='item-in-list-data'>
        <div className='item-in-list-data-block night-mode-text'>{props.competenceInfo.name}</div>
      </div>
      <div className='item-in-list-buttons-container'>
        {
          wpCompetenciesState.redactCompetenciesListForWP ? (
            <div className='item-in-list-buttons-container-checkbox'>
              <input
                type='checkbox'
                id={'add-competence-to-wp-' + props.competenceInfo.id}
                className='item-in-list-buttons-container-checkbox-input'
                checked={wpCompetenciesState.connectsList.includes(props.competenceInfo.id)}
                onChange={(e) => { handlerChangeCheckBox(e) }}
              />
              <label
                htmlFor={'add-competence-to-wp-' + props.competenceInfo.id}
                className='item-in-list-buttons-container-checkbox-label night-mode-form-bg'>
              </label>
            </div>
          ) : (false)
        }
        <button
          type='button'
          className='mini-functional-button mini-warning-button mini-redact-button'
          onClick={(e) => { props.onRename(props.competenceInfo.id, props.competenceInfo.name) }}
        ></button>
        <button
          type='button'
          className='mini-functional-button mini-danger-button mini-delete-button'
          onClick={(e) => { props.onDelete(props.competenceInfo.id, props.competenceInfo.name) }}
        ></button>
      </div>
    </div>
  );
});

export default ItemCompetence;