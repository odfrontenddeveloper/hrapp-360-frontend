import React, { useState } from "react";
import RedactData from "./RedactData";
import { imageLoadOperations } from "../appstate/styles_config";
import axios from "axios";
import globalConfig from "../appstate/global_config";
import listsofdata from "../appstate/listsofdata";
import { resultsState, resultsStateOperations } from "../appstate/showResultsState";

const DeleteAssessmentForm = props => {
  let refDeleteButton = React.createRef();
  let [resDelete, changeresDelete] = useState(null);

  let closeDeleteItemForm = () => {
    props.refForm.current.style.display = 'none';
  }

  let handlerDeleteForm = (e) => {
    imageLoadOperations.openImageLoad();
    e.preventDefault();
    let data = {
      id: refDeleteButton.current.value
    };
    axios({
      method: 'delete',
      url: globalConfig.serverAddress + `/deleteassessment`,
      data: data
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          changeresDelete(null);
          closeDeleteItemForm();
          if (resultsState.showMode && resultsState.showId) {
            if (data.id == resultsState.showId) {
              resultsStateOperations.disableState();
            }
          }
          listsofdata.listOfWP = listsofdata.listOfWP.filter((el, i) => {
            return el.id != data.id;
          });
          resultsState.assessmentsList = resultsState.assessmentsList.filter((el, i) => {
            return el.id != data.id;
          });
        }
        else {
          changeresDelete(response.data.content);
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  }

  let onCloseForm = () => {
    changeresDelete(null);
  }

  return (
    <RedactData forRef={props.refForm} onCloseForm={onCloseForm}>
      <form
        className='standart-form vertical-form form-center night-mode-form-bg'
        onSubmit={(e) => { handlerDeleteForm(e) }}
      >
        <div className='form-container input-container form-container-column'>
          <label
            className='form-container-label night-mode-text'
          >
            {`Вы действительно хотите удалить мероприятие "${props.deleteName}"?`}
          </label>
        </div>
        <div className='button-container'>
          <button
            type='submit'
            className='mini-functional-button mini-success-button mini-complete-button'
            value={props.deleteId}
            ref={refDeleteButton}
          ></button>
          <button
            type='button'
            className='mini-functional-button mini-danger-button mini-close-button'
            onClick={() => closeDeleteItemForm()}
          ></button>
        </div>
      </form>
      <div className='result-container res-form-center dangerColor'>{resDelete}</div>
    </RedactData>
  );
}

export default DeleteAssessmentForm;