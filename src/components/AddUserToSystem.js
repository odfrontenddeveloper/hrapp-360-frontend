import React from "react";
import RedactData from './RedactData.js';
import RegForm from './RegForm.js';

const AddUserToSystem = (props) => {
  return (
    <RedactData forRef={props.refForm}>
      <RegForm addUserByAdmin={true} />
    </RedactData>
  );
}

export default AddUserToSystem;