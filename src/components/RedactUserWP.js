import React, { useState } from "react";
import { observer } from "mobx-react";
import RedactData from "./RedactData";
import { imageLoadOperations } from "../appstate/styles_config";
import { userslist } from "../appstate/redactStaffState";
import globalConfig from "../appstate/global_config";
import axios from "axios";

const RedactUserWP = observer((props) => {
  let [inputSearch, changeinputSearch] = useState("");
  let [resultRedactUserWP, changeresultRedactUserWP] = useState(null);

  let selectNewWPForUser = (e, idwp, wpname) => {
    e.persist();
    imageLoadOperations.openImageLoad();
    axios({
      method: "post",
      url: `${globalConfig.serverAddress}/connectuserwithwp`,
      data: {
        user: userslist.redactId,
        wp: idwp,
      },
    })
      .then((response) => {
        imageLoadOperations.closeImageLoad();
        if (response.status >= 200 && response.status < 300) {
          if (response.data.status == true) {
            changeresultRedactUserWP(null);
            props.redactUserWPName.current.innerHTML = wpname;
            e.target.checked = true;
            let selectedWP = wpname;
            userslist.wplistforoptions = userslist.wplistforoptions.map(
              (el) => {
                el.ch = false;
                if (selectedWP == el.name) {
                  el.ch = true;
                }
                return el;
              }
            );

            userslist.list = userslist.list.map((el) => {
              if (el.id == userslist.redactId) {
                el.wp = wpname;
              }
              return el;
            });
          } else {
            changeresultRedactUserWP(response.data.content);
          }
        }
      })
      .catch((error) => {
        imageLoadOperations.closeImageLoad();
        console.log(error);
      });
  };

  let disconnnectWPAndUser = () => {
    imageLoadOperations.openImageLoad();
    axios({
      method: "delete",
      url: `${globalConfig.serverAddress}/disconnectuserandwp`,
      data: {
        user: userslist.redactId,
      },
    })
      .then((response) => {
        imageLoadOperations.closeImageLoad();
        if (response.status >= 200 && response.status < 300) {
          if (response.data.status == true) {
            changeresultRedactUserWP(null);
            props.redactUserWPName.current.innerHTML = "Должность не указана";

            userslist.wplistforoptions = userslist.wplistforoptions.map(
              (el) => {
                el.ch = false;
                return el;
              }
            );

            userslist.list = userslist.list.map((el) => {
              if (el.id == userslist.redactId) {
                el.wp = null;
              }
              return el;
            });
            console.log(userslist.list);
          } else {
            changeresultRedactUserWP(response.data.content);
          }
        }
      })
      .catch((error) => {
        imageLoadOperations.closeImageLoad();
        console.log(error);
      });
  };

  let handleChangeInput = (e) => {
    changeinputSearch(e.target.value);
  };

  let onCloseForm = () => {
    changeresultRedactUserWP(null);
  };

  return (
    <RedactData forRef={props.refRedactUserWPForm} onCloseForm={onCloseForm}>
      <form className="standart-form form-center vertical-form form-center night-mode-form-bg-lite">
        {userslist.wplistforoptions.length ? (
          <>
            <div>
              <div
                id="redact-user-wp-login"
                className="redact-users-staff-info-text night-mode-text"
                ref={props.redactUserWPLogin}
                style={{ padding: "5px" }}
              >
                {props.userwpformheader}
              </div>
              <div className="redact-staff-place-display-info-block-null-line night-mode-form-bg"></div>
              <div
                id="redact-user-wp-wpname"
                className="redact-users-staff-info-text night-mode-text"
                ref={props.redactUserWPName}
                style={{ padding: "5px" }}
              >
                {props.userwp}
              </div>
              <div style={{ display: "flex", flexDirection: "column" }}>
                <div className="form-container input-container form-container-column">
                  <button
                    type="button"
                    className="href-button"
                    onClick={(e) => {
                      disconnnectWPAndUser();
                    }}
                  >
                    Сбросить должность
                  </button>
                </div>
                <div className="form-container input-container form-container-column">
                  <label
                    className="form-container-label night-mode-text"
                    htmlFor="searchWPByName"
                  >
                    Начните вводить название должности:
                  </label>
                  <input
                    id="searchWPByName"
                    className="form-container-input night-mode-text night-mode-input"
                    type="text"
                    value={inputSearch}
                    onChange={(e) => {
                      handleChangeInput(e);
                    }}
                  />
                </div>
              </div>
            </div>
            <div
              className="redact-user-wp-list-of-wp night-mode-input"
              ref={props.refWPList}
            >
              {userslist.wplistforoptions.map((el) => {
                if (el.name.indexOf(inputSearch) != -1) {
                  return (
                    <div className="item-in-list" key={el.id}>
                      <div className="item-in-list-data-block night-mode-text">
                        {el.name}
                      </div>
                      <div className="item-in-list-buttons-container">
                        <div className="item-in-list-buttons-container-radio-block">
                          <input
                            type="radio"
                            id={`wp-${el.id}`}
                            className="item-in-list-buttons-container-radio-block-input"
                            name="select-user-wp"
                            checked={el.ch}
                            onChange={(e) => {
                              selectNewWPForUser(e, el.id, el.name);
                            }}
                            value={el.id}
                          />
                          <label
                            className="item-in-list-buttons-container-radio-block-label night-mode-form-bg"
                            htmlFor={`wp-${el.id}`}
                          ></label>
                        </div>
                      </div>
                    </div>
                  );
                }
              })}
            </div>
          </>
        ) : (
          <div className="form-container-label night-mode-text">
            Создайте должность, что бы присвоить её пользователю
          </div>
        )}
      </form>
      <div
        className="result-container form-center dangerColor"
        id="result-redact-user-wp"
      >
        {resultRedactUserWP}
      </div>
    </RedactData>
  );
});

export default RedactUserWP;
