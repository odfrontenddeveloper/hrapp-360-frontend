import React, { useState, useEffect, createRef } from "react";
import axios from 'axios';
import { observer } from "mobx-react";
import globalConfig from "../appstate/global_config.js";
import authOperations from "../appstate/userauthOperations.js";
import ChangeUserInfo from "./ChangeUserInfo.js";
import ChangeSecurityInfo from "./ChangeSecurityInfo.js";

const UserAreaInfo = observer(() => {
  let refImage = createRef();
  let refForm = createRef();
  let refFormSecurity = createRef();
  let refUploadAvatarForm = createRef();

  useEffect(() => {
    displayUserInfo();
  });

  let [name, changename] = useState(false);
  let [surename, changesurename] = useState(false);
  let [patronymic, changepatronymic] = useState(false);
  let [usertype, changeusertype] = useState(false);

  let showNewPhoto = () => {
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/useravatar?login=${authOperations.getCookie('userLogin')}`
    }).then(response => {
      refImage.current.style.backgroundImage = ``;
      setTimeout(() => {
        refImage.current.style.backgroundImage = `url('${globalConfig.serverAddress}/useravatar?login=${authOperations.getCookie('userLogin')}')`;
      }, 10);
    }).catch(error => {
      console.log(error);
    });
  }

  let changePhoto = (e) => {
    let uploadImageForm = new FormData(refImage.current);
    uploadImageForm.append('avatar', e.target.files[0]);
    axios({
      method: 'post',
      url: globalConfig.serverAddress + '/uploadavatar',
      data: uploadImageForm
    }).then(response => {
      refUploadAvatarForm.current.value = null;
      if (response.data.status) {
        showNewPhoto();
      }
      else if (response.data.content) {
        alert(response.data.content);
      }
      else {
        alert('Ошибка.');
      }
    }).catch(error => {
      refUploadAvatarForm.current.value = null;
      console.log(error);
    });
  }

  let displayUserInfo = () => {
    refImage.current.style.backgroundImage = `url('${globalConfig.serverAddress}/useravatar?login=${authOperations.getCookie('userLogin')}')`;
    axios({
      method: 'get',
      url: globalConfig.serverAddress + '/getuserfullname'
    }).then(response => {
      changename(response.data.content.name);
      changesurename(response.data.content.surename);
      changepatronymic(response.data.content.patronymic);
      changeusertype(response.data.content.type);
    }).catch(error => {
      console.log(error);
    });
  }

  let showChangeUserSecurityInfo = function () {
    this.style.display = 'flex';
  }

  return (
    <div className='user-area-place-info night-mode-form-bg-lite'>
      <div className='user-area-place-info-avatar'>
        <form encType="multipart/form-data" className='user-area-place-info-avatar-image' method='post' ref={refImage}>
          <label htmlFor='upload-new-avatar' className='user-area-place-info-avatar-image-upload'>
            <input
              type='file'
              id='upload-new-avatar'
              name='uploadavatar'
              onChange={(e) => { changePhoto(e) }}
              ref={refUploadAvatarForm}
            />
          </label>
        </form>
      </div>
      <div className='user-area-place-info-label bold-italic night-mode-text'>{authOperations.getCookie('userLogin') + '(' + usertype + ')'}</div>
      <div className='user-area-place-info-label bold-italic night-mode-text'>{surename}</div>
      <div className='user-area-place-info-label bold-italic night-mode-text'>{name}</div>
      <div className='user-area-place-info-label bold-italic night-mode-text'>{patronymic}</div>
      <div className='button-container'>
        <button type='button' className='mini-functional-button mini-warning-button mini-settings-button' onClick={() => { showChangeUserSecurityInfo.call(refForm.current) }}></button>
        <button type='button' className='mini-functional-button mini-success-button mini-security-button' onClick={() => { showChangeUserSecurityInfo.call(refFormSecurity.current) }}></button>
      </div>
      <ChangeUserInfo forRef={refForm} ifUpdate={displayUserInfo} />
      <ChangeSecurityInfo forRef={refFormSecurity} />
    </div>
  );
});

export default UserAreaInfo;