import React, { useEffect } from "react";
import { observer } from "mobx-react";
import authState from "../appstate/authstate";
import { Redirect } from "react-router-dom";
import axios from "axios";
import authOperations from "../appstate/userauthOperations";
import globalConfig from "../appstate/global_config";

const CheckAuth = observer(() => {

  useEffect(() => {
    axios({
      method: 'get',
      url: globalConfig.serverAddress + '/checktoken'
    }).then(response => {
      if (!response.data) {
        authOperations.logOut();
      }
    }).catch(error => {
      console.log(error);
    });
  });

  return (
    !authState.auth ? <Redirect to='/auth' /> : false
  ); 
});

export default CheckAuth;