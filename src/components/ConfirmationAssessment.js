import React, { useState } from "react";
import { assessmentConfigState, assessmentConfigStateOperations, assessmentConfigStoreOperations, assessmentConfigStore } from "../appstate/assessmentConfigState";

const ConfirmationAssessment = () => {
  let [resCrate, changeResCreate] = useState('');
  let [rescolor, changerescolor] = useState('');

  return (
    <div className={`staff-assessment-place-punkts${assessmentConfigState.selectedPunkt == 4 ? '' : ' display-none'}`}>
      <div className='users-list'>
        <div className='users-list-area'>
          <div className='users-list-area-col night-mode-form-bg'>
            <form className='users-list-area-col-header' onSubmit={(e) => { assessmentConfigStateOperations.createAssessment(e, changeResCreate, changerescolor); } }>
              <button type='submit' className='confirmation-button'>Создать мероприятие</button>
            </form>
            <div className='users-list-area-col-list night-mode-input'>
              <div className={`users-list-area-col-list-items night-mode-input${rescolor ? ' successColor' : ' dangerColor'}`}>
                {resCrate}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ConfirmationAssessment;