import React, { createRef, useState } from "react";
import RedactData from "./RedactData";
import globalConfig from "../appstate/global_config";
import { stylesConfig, imageLoadOperations } from "../appstate/styles_config.js";
import { observer } from "mobx-react";
import axios from "axios";
import listsofdata from "../appstate/listsofdata";

const AddCopyWorkPosition = observer((props) => {
  let refButton = createRef();
  let refInputName = createRef();

  let [resAddCopy, changeresAddCopy] = useState(null);
  let [formResColor, changeformResColor] = useState(null);

  let handlerAddCopyWPForm = (e) => {
    e.preventDefault();
    imageLoadOperations.openImageLoad();
    let data = {
      nameWP: refInputName.current.value,
      copyIdWP: refButton.current.value
    };
    axios({
      method: 'post',
      url: globalConfig.serverAddress + '/addwp',
      data: data
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status) {
          listsofdata.listOfWP.push(response.data.content);
          changeformResColor(true);
          changeresAddCopy('Должность создана.');
        }
        else {
          changeformResColor(false);
          changeresAddCopy(response.data.content);
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  }

  let onCloseForm = () => {
    changeresAddCopy(null);
  }

  return (
    <RedactData forRef={props.refForm} onCloseForm={onCloseForm}>
      <form
        className='standart-form vertical-form form-center night-mode-form-bg'
        onSubmit={(e) => { handlerAddCopyWPForm(e) }}
      >
        <div className='form-container input-container form-container-column'>
          <label
            className='form-container-label night-mode-text'
            htmlFor='addCopyWP'
          >
            {`Создание независимой копии должности "${props.nameWP}". Укажите название новой должности.`}
          </label>
          <input
            id='addCopyWP'
            className='form-container-input night-mode-text night-mode-input'
            type='text'
            ref={refInputName}
          />
        </div>
        <div className='button-container'>
          <button
            type='submit'
            className='mini-functional-button mini-success-button mini-add-button'
            ref={refButton}
            value={props.idWP}
          ></button>
        </div>
      </form>
      <div
        id='result-add-copy-wp'
        className={`result-container res-form-center ${formResColor ? 'successColor' : 'dangerColor'}`}
      >{
          resAddCopy
        }</div>
    </RedactData>
  );
});

export default AddCopyWorkPosition;