import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { assessmentConfigStateOperations } from "../appstate/assessmentConfigState";

const ItemUGToImport = observer((props) => {

  return (
    <button type='button' className='ug-item-button' onClick={() => { assessmentConfigStateOperations.importUGToAssessment(props.data.id)}}>
      <div className='night-mode-text'>{props.data.name}</div>
    </button>
  );
});

export default ItemUGToImport;