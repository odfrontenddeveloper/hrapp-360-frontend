import React, { useState, useEffect } from "react";
import { imageLoadOperations } from "../appstate/styles_config.js";
import listsofdata from "../appstate/listsofdata.js";
import axios from "axios";
import globalConfig from "../appstate/global_config.js";
import { observer } from "mobx-react";
import { wpCompetenciesState, wpcstateOperations } from "../appstate/wpCompetenciesState.js";
import { ugstate, ugstateOperations } from "../appstate/usersGroupsState.js";
import authOperations from "../appstate/userauthOperations.js";
import { resultsState } from "../appstate/showResultsState.js";

const SearchWPCompetenceForm = observer((props) => {
  let [searchValue, changesearchValue] = useState('');
  let [sortByAlphabet, changesortByAlphabet] = useState(false);

  let downloadUsersList = (sParam = '') => {
    if (props.field != 'ugu') {
      props.setBaseCounter();
    }
    imageLoadOperations.openImageLoad();

    let searchHref = {
      'wp': `/searchwp?nameWP=${sParam}&sortabc=${sortByAlphabet}`,
      'competence': `/searchcompetence?nameCompetence=${sParam}&sortabc=${sortByAlphabet}`,
      'ug': `/searchug?nameUG=${sParam}&sortabc=${sortByAlphabet}`,
      'ugu': `/getuserslist?admin=${authOperations.getCookie('userLogin')}`,
      'assessment': `/searchassessments?nameAssessment=${sParam}&sortabc=${sortByAlphabet}`
    };

    axios({
      method: 'get',
      url: globalConfig.serverAddress + searchHref[props.field]
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (props.field == 'wp') {
        listsofdata.listOfWP = [];
        if (response.status >= 200 && response.status < 300) {
          if (response.data.status == true) {
            listsofdata.listOfWP = response.data.content;
          }
        }
      }
      else if (props.field == 'competence') {
        if (response.status >= 200 && response.status < 300) {
          if (response.data.status == true) {
            listsofdata.listOfCompetencies = response.data.content;
          }
        }
      }
      else if (props.field == 'ug') {
        if (response.status >= 200 && response.status < 300) {
          if (response.data.status == true) {
            ugstate.listOfUG = response.data.content;
          }
        }
      }
      else if (props.field == 'ugu') {
        if (response.status >= 200 && response.status < 300) {
          if (response.data.status == true) {
            ugstate.userslistForUG = response.data.content;
          }
        }
      }
      else if (props.field == 'assessment') {
        if (response.status >= 200 && response.status < 300) {
          if (response.data.status == true) {
            resultsState.assessmentsList = response.data.content;
          }
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  }

  useEffect(() => {
    if (props.field != 'ugu') {
      downloadUsersList();
    } else {
      ugstateOperations.getUserListForUG();
    }
  }, []);

  let submitSearchForm = (e) => {
    e.preventDefault();
    downloadUsersList(searchValue);
  }

  let sortByAlphabetState = () => {
    changesortByAlphabet(!sortByAlphabet);
  }

  return (
    <form
      className='standart-form horizontal-form night-mode-form-bg'
      onSubmit={(e) => { submitSearchForm(e) }}
    >
      <div className='form-container input-container form-container-row'>
        <label
          className='form-container-label night-mode-text'
          htmlFor={`searchform${props.field}`}
        >
          Поиск
         </label>
        <input
          type='text'
          id={`searchform${props.field}`}
          className='form-container-input night-mode-text night-mode-input'
          value={searchValue}
          onChange={(e) => { changesearchValue(e.target.value) }}
        />
      </div>
      <div className='button-container'>
        {
          props.field != 'ugu' ? (
            <React.Fragment>
              <button
                type='button'
                className={`mini-functional-button mini-${sortByAlphabet ? ' success' : ' dark'}-button mini-sortabc-button`}
                onClick={() => { sortByAlphabetState() }}
              ></button>
              <button
                type='submit'
                className='mini-functional-button mini-primary-button mini-search-button'
              ></button>
            </React.Fragment>
          ) : ( null )
        }
        {
          (wpCompetenciesState.redactCompetenciesListForWP && props.field == 'competence') ||
          (ugstate.redactUGMode && props.field == 'ugu') ? (
            <button
              type='button'
              className={`mini-functional-button mini-${props.field == 'competence' ? (wpCompetenciesState.searchStateFilter ? ' warning' : ' dark') : (ugstate.searchStateFilter ? ' warning' : ' dark')}-button mini-filter-button`}
              onClick={() => { props.field == 'competence' ? wpcstateOperations.changeSearchState() : ugstateOperations.changeSearchState() }}
            ></button>
          ) : (
              false
            )
        }
      </div>
    </form>
  );
});

export default SearchWPCompetenceForm;