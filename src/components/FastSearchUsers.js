import React from "react";
import { observer } from "mobx-react";

const FastSearchUsers = observer((props) => {
  return (
    <form
      className='standart-form horizontal-form night-mode-form-bg'
      onSubmit={(e) => { submitSearchForm(e) }}
    >
      <div className='form-container input-container form-container-column'>
        <label
          className='form-container-label night-mode-text'
          htmlFor={`searchform${props.field}${props.addfromug ? 'addfromug' : ''}`}
        >
          Ф.И.О.
         </label>
        <input
          type='text'
          id={`searchform${props.field}${props.addfromug ? 'addfromug' : ''}`}
          className='form-container-input night-mode-text night-mode-input'
          value={props.searchValue}
          onChange={(e) => { props.changeSearchValue(e.target.value) }}
        />
      </div>
      <div className='form-container input-container form-container-column'>
        <label
          className='form-container-label night-mode-text'
          htmlFor={`searchformwp${props.field}${props.addfromug ? 'addfromug' : ''}`}
        >
          Должность
         </label>
        <input
          type='text'
          id={`searchformwp${props.field}${props.addfromug ? 'addfromug' : ''}`}
          className='form-container-input night-mode-text night-mode-input'
          value={props.searchValueWP}
          onChange={(e) => { props.changeSearchValueWP(e.target.value) }}
        />
      </div>
    </form>
  );
});

export default FastSearchUsers;