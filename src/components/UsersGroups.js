import React, { useEffect } from "react";
import { observer } from "mobx-react";
import UserAreaHeader from "./UserAreaHeader.js";
import CheckAuth from "./CheckAuth.js";
import RedactUsersGroups from "./RedactUsersGroups.js";
import RedactUsersListForUG from "./RedactUsersListForUG.js";
import { ugstateOperations } from "../appstate/usersGroupsState.js";

const UsersGroups = () => {
  let refArea = React.createRef();

  useEffect(() => {
    ugstateOperations.getUserListForUG();
  }, []);

  return (
    <div ref={refArea} className='work-positions night-mode-bg-block'>
      <CheckAuth />
      <UserAreaHeader
        logout={refArea}
        backButton={true}
      />
      <div className='work-positions-area'>
        <RedactUsersGroups />
        <RedactUsersListForUG />
      </div>
    </div>
  );
};

export default UsersGroups;