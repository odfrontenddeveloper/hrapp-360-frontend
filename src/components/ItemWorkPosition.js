import React from "react";
import { observer } from "mobx-react";
import globalConfig from "../appstate/global_config.js";
import { wpcstateOperations, wpCompetenciesState } from "../appstate/wpCompetenciesState.js";
import axios from "axios";
import { ugstate, ugstateOperations } from "../appstate/usersGroupsState.js";

const ItemWorkPosition = observer(props => {
  let openRedactConnectionsList = () => {
    axios({
      method: 'get',
      url: globalConfig.serverAddress + (!props.ug ? `/searchconnectswpcompetence?wp=${props.wpInfo.id}` : `/searchusersgroupsconnections?ug=${props.wpInfo.id}`)
    }).then(response => {
      if (response.status >= 200 && response.status < 300) {
        if (!props.ug) {
          wpCompetenciesState.connectsList = response.data.content.map((el, i) => {
            return el.competence;
          });
          wpcstateOperations.changeState(props.wpInfo.id);
        }
        else {
          ugstate.usersGroupsConnections = response.data.content.map(conn => {
            return conn.user;
          });
          ugstateOperations.changeRedactUGState(props.wpInfo.id);
        }
      }
    }).catch(error => {
      console.log(error);
    });
  }

  return (
    <div
      id={'list-of-wp-id-' + props.wpInfo.id}
      className='item-in-list'
      style={{ animation: `${1 * 0.4}s showBlock forwards` }}
    >
      <div className='item-in-list-data'>
        <div className='item-in-list-data-block night-mode-text'>{props.wpInfo.name}</div>
      </div>
      <div className='item-in-list-buttons-container'>
        <button
          type='button'
          className={`mini-functional-button mini-${
            !props.ug ? (wpCompetenciesState.redactCompetenciesListForWP ? (
              (props.wpInfo.id == wpCompetenciesState.redactWPId) ? 'red-orange' : 'primary') : ('primary')
            ) : (
                ugstate.redactUGMode ? (
                  (props.wpInfo.id == ugstate.redactUGID) ? 'red-orange' : 'primary') : ('primary')
              )}-button mini-settings-button`}
          onClick={(e) => { openRedactConnectionsList(e) }}
        ></button>
        {
          !props.ug ? (
            <button
              type='button'
              className={`mini-functional-button mini-fiolet-button mini-copy-button`}
              onClick={(e) => { props.addCopy(props.wpInfo.id, props.wpInfo.name) }}
            ></button>
          ) : (null)
        }
        <button
          type='button'
          className='mini-functional-button mini-warning-button mini-redact-button'
          onClick={(e) => { props.onRename(props.wpInfo.id, props.wpInfo.name) }}
        ></button>
        <button
          type='button'
          className='mini-functional-button mini-danger-button mini-delete-button'
          onClick={(e) => { props.onDelete(props.wpInfo.id, props.wpInfo.name) }}
        ></button>
      </div>
    </div>
  );
});

export default ItemWorkPosition;