// Modules
import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { observer } from "mobx-react";

// Components
import IdentifyUser from "./components/IdentifyUser.js";
import UserArea from "./components/UserArea.js";
import UsersWorkPositions from "./components/UsersWorkPositions.js";
import RedactStaff from "./components/RedactStaff.js";
import ImageLoad from "./components/ImageLoad.js";
import RedactCriteriaSigns from "./components/RedactCriteriaSigns.js";
import StaffAssessmentConfig from "./components/StaffAssessmentConfig.js";
import GetInvites from "./components/GetInvites.js";
import UsersGroups from "./components/UsersGroups.js";
import GetResults from "./components/GetResults.js";

// Project config
import { stylesConfig } from "./appstate/styles_config.js";

const Routes = observer(() => {
  return (
    <div id='app' className={`app ${stylesConfig.nightMode ? `night-mode-app` : ``}`}>
      <Router>
        <ImageLoad />
        <Route exact path="/" component={IdentifyUser} />
        <Route exact path="/auth" component={IdentifyUser} />
        <Route exact path="/dashboard" component={UserArea} />
        <Route exact path="/redactstaff" component={RedactStaff} />
        <Route exact path="/redactusersgroups" component={UsersGroups} />
        <Route exact path="/workpositions" component={UsersWorkPositions} />
        <Route exact path="/criteriasigns" component={RedactCriteriaSigns} />
        <Route exact path="/staffassessment" component={StaffAssessmentConfig} />
        <Route exact path="/getinvites" component={GetInvites} />
        <Route exact path="/results" component={GetResults} />
      </Router>
    </div>
  );
});

export default Routes;