import React from "react";
import ReactDOM from "react-dom";
import Routes from "./Router.js";
import authOperations from "./appstate/userauthOperations.js";
import axios from "axios";
import './styles/App.scss';

console.log(process.env.NODE_ENV)

axios.defaults.headers.common['userLogin'] = authOperations.getCookie('userLogin');
axios.defaults.headers.common['userToken'] = authOperations.getCookie('userToken');

(async () => {
    if (!authOperations.getCookie('nightMode')) {
        document.cookie = 'nightMode=off';
    }

    await authOperations.checkAuth();
    ReactDOM.render(<Routes />, document.getElementById("root"));
})();
