import { observable } from "mobx";
import axios from "axios";
import globalConfig from "./global_config";
import { imageLoadOperations } from "./styles_config";
import authOperations from "./userauthOperations";
import '../appstate/arrayPrototype';

export let assessmentConfigStore = observable([
  {
    ['usersListForAssessment']: [],
    ['usersInvites']: [],
    ['backgroundLight']: [],
    ['id']: 0
  }
]);

export let assessmentConfigStoreOperations = {
  ['saveToStore']: () => {
    assessmentConfigStore = assessmentConfigStore.filter(element => {
      return element.id <= Number(assessmentConfigState.storeCounter);
    });

    let newStorePage = {
      ['usersListForAssessment']: JSON.parse(JSON.stringify(assessmentConfigState.usersListForAssessment.map(element => element))),
      ['usersInvites']: JSON.parse(JSON.stringify(assessmentConfigState.usersInvites.map(element => element))),
      ['backgroundLight']: JSON.parse(JSON.stringify(assessmentConfigState.backgroundLight.map(element => element))),
      ['id']: assessmentConfigState.storeCounter + 1
    };

    assessmentConfigState.storeCounter = assessmentConfigState.storeCounter + 1;
    assessmentConfigStore.push(newStorePage);
  },
  ['stepInStore']: (id) => {
    if (id >= 0 && id < assessmentConfigStore.length) {
      let storePage = assessmentConfigStore[id];
      assessmentConfigState.usersListForAssessment = id != 0 ? (
        storePage.usersListForAssessment
      ) : (
          assessmentConfigState.usersListForAssessment.map((element, i) => {
            element.selected = false;
            return element;
          })
        );

      assessmentConfigState.backgroundLight = id != 0 ? (
        storePage.backgroundLight
      ) : (
          assessmentConfigState.backgroundLight.filter(element => {
            return selectedUsers.includes(element);
          })
        );

      assessmentConfigState.usersInvites = storePage.usersInvites;
      assessmentConfigState.storeCounter = storePage.id;

      let selectedUsers = assessmentConfigState.usersListForAssessment.filter(user => {
        return user.selected
      }).map(el => el.id);
    }
  },
  ['logOut']: () => {
    assessmentConfigStore.usersListForAssessment = [];
    assessmentConfigStore.usersInvites = [];
    assessmentConfigState.backgroundLight = [];
    assessmentConfigState.id = 0;
  }
};

export let assessmentConfigState = observable({
  ['selectedPunkt']: 1,
  ['backgroundLight']: [],
  ['usersListForAssessment']: [],
  ['usersInvites']: [],
  ['storeCounter']: 0,
  ['assessmentName']: ''
});

export let assessmentConfigStateOperations = {
  ['changeAssessmentName']: (value) => {
    assessmentConfigState.assessmentName = value;
  },
  ['changeSelectedPunkt']: (value) => {
    assessmentConfigState.selectedPunkt = value;
  },
  ['getUsersListForAssessment']: () => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/getuserslist?admin=${authOperations.getCookie('userLogin')}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          if (assessmentConfigState.usersListForAssessment.length > 0) {
            let newUsersList = response.data.content.map((element, i) => {
              element.selected = false;
              if (assessmentConfigState.usersListForAssessment[i]) {
                if (assessmentConfigState.usersListForAssessment[i].selected == true) {
                  element.selected = true;
                }
              }
              return element;
            });
            assessmentConfigState.usersListForAssessment = newUsersList;
          }
          else {
            assessmentConfigState.usersListForAssessment = response.data.content.map(element => {
              return { ...element, selected: false };
            });
          }
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['changeUserSelectedToAssessment']: (id, wp) => {
    if (wp != null) {
      assessmentConfigState.usersListForAssessment = assessmentConfigState.usersListForAssessment.map((element) => {
        if (element.id == id) {
          element.selected = !element.selected;
          if (element.selected == false) {
            assessmentConfigState.backgroundLight = assessmentConfigState.backgroundLight.filter(user => {
              return user != element.id;
            });
            assessmentConfigState.usersInvites = assessmentConfigState.usersInvites.filter(invite => {
              return invite.user_from != element.id && invite.user_to != element.id;
            });
          }
        }
        return element;
      });
    }
    else {
      assessmentConfigState.usersListForAssessment = assessmentConfigState.usersListForAssessment.map((element) => {
        if (element.id == id) {
          element.selected = false;
        }
        return element;
      });
      assessmentConfigState.usersInvites = assessmentConfigState.usersInvites.filter(invite => {
        return invite.user_from != id && invite.user_to != id;
      });
    }
    assessmentConfigStoreOperations.saveToStore();
  },
  ['resetUserInvite']: (user_from, user_to) => {
    if (assessmentConfigState.usersInvites.some(invite => {
      return (invite.user_from == user_from) && (invite.user_to == user_to);
    })) {
      assessmentConfigState.usersInvites = assessmentConfigState.usersInvites.filter(invite => {
        return !(invite.user_from == user_from && invite.user_to == user_to);
      });
    } else {
      assessmentConfigState.usersInvites.push({
        user_from: user_from,
        user_to: user_to
      });
    }
    assessmentConfigStoreOperations.saveToStore();
  },
  ['changeBackgroundLight']: (user_id) => {
    if (assessmentConfigState.backgroundLight.includes(user_id)) {
      assessmentConfigState.backgroundLight = assessmentConfigState.backgroundLight.filter(element => {
        return element != user_id;
      });
    } else {
      assessmentConfigState.backgroundLight.push(user_id);
    }
    assessmentConfigStoreOperations.saveToStore();
  },
  ['checkAssessmentInvite']: (user_from, user_to) => {
    return assessmentConfigState.usersInvites.some(invite => {
      return (invite.user_from == user_from) && (invite.user_to == user_to);
    });
  },
  ['importUGToAssessment']: (id) => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/searchusersgroupsconnections?ug=${id}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        assessmentConfigState.usersListForAssessment = assessmentConfigState.usersListForAssessment.map(element => {
          if (response.data.content.map(element1 => element1.user).includes(element.id)) {
            if (element.wp) {
              element.selected = true;
            }
            else {
              element.selected = false;
            }
          }
          return element;
        });
        assessmentConfigStoreOperations.saveToStore();
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['selectAll']: (value) => {
    if (assessmentConfigState.usersListForAssessment.some(user => user.selected == true)) {
      if (value === true) {
        assessmentConfigState.backgroundLight = assessmentConfigState.usersListForAssessment.filter(user => user.selected).map(user => user.id);
        assessmentConfigStoreOperations.saveToStore();
      }
      else {
        assessmentConfigState.backgroundLight = [];
        assessmentConfigStoreOperations.saveToStore();
      }
    }
  },
  ['assessmentYourself']: (value) => {
    if (assessmentConfigState.backgroundLight.length > 0) {
      if (value == true) {
        assessmentConfigState.backgroundLight.forEach(user_id => {
          if (!assessmentConfigState.usersInvites.some(element => element.user_from == user_id && element.user_to == user_id)) {
            assessmentConfigState.usersInvites.push({
              user_from: user_id,
              user_to: user_id
            });
          }
        });
        assessmentConfigStoreOperations.saveToStore();
      }
      else {
        assessmentConfigState.usersInvites = assessmentConfigState.usersInvites.filter(invite => {
          if (!assessmentConfigState.backgroundLight.includes(invite.user_from)) {
            return true;
          }
          return invite.user_from != invite.user_to;
        });
        assessmentConfigStoreOperations.saveToStore();
      }
    }
  },
  ['assessmentAllToAll']: (value) => {
    if (assessmentConfigState.backgroundLight.length > 0) {
      if (value === true) {
        assessmentConfigState.backgroundLight.forEach(user_from => {
          assessmentConfigState.backgroundLight.forEach(user_to => {
            if (assessmentConfigState.usersInvites.some(invite => invite.user_from == user_from && invite.user_to == user_to)) {
              return;
            }
            if (user_from == user_to) {
              return;
            }
            assessmentConfigState.usersInvites.push({
              user_from: user_from,
              user_to: user_to
            });
          });
        });
        assessmentConfigStoreOperations.saveToStore();
      }
      else {
        assessmentConfigState.usersInvites = assessmentConfigState.usersInvites.filter(invite => {
          if (!assessmentConfigState.backgroundLight.includes(invite.user_from)) {
            return true;
          }

          if (!assessmentConfigState.backgroundLight.includes(invite.user_to)) {
            return true;
          }

          return invite.user_from == invite.user_to;
        });
        assessmentConfigStoreOperations.saveToStore();
      }
    }
  },
  ['assessmentHorizontal']: (value) => {
    if (value === true) {
      assessmentConfigState.backgroundLight.forEach(user_from => {
        assessmentConfigState.usersListForAssessment.forEach(user => {
          if (assessmentConfigState.usersInvites.some(invite => invite.user_from == user_from && invite.user_to == user.id)) {
            return;
          }
          if (user_from == user.id) {
            return;
          }
          assessmentConfigState.usersInvites.push({
            user_from: user_from,
            user_to: user.id
          });
        });
      });
      assessmentConfigStoreOperations.saveToStore();
    }
    else {
      assessmentConfigState.usersInvites = assessmentConfigState.usersInvites.filter(invite => {
        if (invite.user_from == invite.user_to) {
          return true;
        }
        return !assessmentConfigState.backgroundLight.includes(invite.user_from);
      });
      assessmentConfigStoreOperations.saveToStore();
    }
  },
  ['assessmentVertical']: (value) => {
    if (value === true) {
      assessmentConfigState.backgroundLight.forEach(user_to => {
        assessmentConfigState.usersListForAssessment.forEach(user => {
          if (assessmentConfigState.usersInvites.some(invite => invite.user_from == user.id && invite.user_to == user_to)) {
            return;
          }
          if (user_to == user.id) {
            return;
          }
          assessmentConfigState.usersInvites.push({
            user_from: user.id,
            user_to: user_to
          });
        });
      });
      assessmentConfigStoreOperations.saveToStore();
    }
    else {
      assessmentConfigState.usersInvites = assessmentConfigState.usersInvites.filter(invite => {
        if (invite.user_from == invite.user_to) {
          return true;
        }
        return !assessmentConfigState.backgroundLight.includes(invite.user_to);
      });
      assessmentConfigStoreOperations.saveToStore();
    }
  },
  ['emptyInvites']: () => {
    if (assessmentConfigState.usersInvites.length > 0) {
      assessmentConfigState.usersInvites = [];
      assessmentConfigStoreOperations.saveToStore();
    }
  },
  ['cleanInvites']: () => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/getuserslist?admin=${authOperations.getCookie('userLogin')}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          if (assessmentConfigState.usersListForAssessment.length > 0) {
            let newUsersList = response.data.content.map((element, i) => {
              element.selected = false;
              if (assessmentConfigState.usersListForAssessment[i]) {
                if (assessmentConfigState.usersListForAssessment[i].selected == true) {
                  element.selected = true;
                }
              }
              return element;
            });
            assessmentConfigState.usersListForAssessment = newUsersList;
          }
          else {
            assessmentConfigState.usersListForAssessment = response.data.content.map(element => {
              return { ...element, selected: false };
            });
          }
          assessmentConfigState.usersInvites = assessmentConfigState.usersInvites.filter(user => {
            let res_from = assessmentConfigState.usersListForAssessment.map(userInfo => userInfo.id).includes(user.user_from);
            let res_to = assessmentConfigState.usersListForAssessment.map(userInfo => userInfo.id).includes(user.user_to);
            return res_from && res_to;
          });
          assessmentConfigStoreOperations.saveToStore();
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['createAssessment']: (e, changeresCreate, changerescolor) => {
    e.preventDefault();
    imageLoadOperations.openImageLoad();
    let data = {
      usersInvites: assessmentConfigState.usersInvites,
      nameAssessment: assessmentConfigState.assessmentName
    };
    axios({
      method: 'post',
      url: globalConfig.serverAddress + '/createassessment',
      data: data
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status) {
          assessmentConfigStoreOperations.logOut();
          assessmentConfigStateOperations.logOut();
          assessmentConfigStateOperations.getUsersListForAssessment();
          changeresCreate('Мероприятие запущено. Сотрудники получили приглашения.');
          changerescolor(true);
        }
        else {
          changeresCreate(response.data.content);
          changerescolor(false);
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['logOut']: () => {
    assessmentConfigState.usersListForAssessment = [];
    assessmentConfigState.usersInvites = [];
    assessmentConfigState.backgroundLight = [];
    assessmentConfigState.assessmentName = '';
  }
};