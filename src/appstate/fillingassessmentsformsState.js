import { observable } from "mobx";
import '../appstate/arrayPrototype';
import globalConfig from "./global_config";
import { imageLoadOperations } from "./styles_config";
import axios from "axios";
import authOperations from "./userauthOperations";

export let fillingAFstate = observable({
  ['userInvites']: [],
  ['assessmentFormInfo']: [],
  ['selectedInvite']: 0,
  ['selectedPunkt']: 1
});

export let fillingAFstateOperations = {
  ['getInvitesList']: () => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/getinviteslist?user=${authOperations.getCookie('userLogin')}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status) {
          fillingAFstate.userInvites = response.data.content;
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['getForm']: (e, invite_id) => {
    e.preventDefault();
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/getformbyinvite?invite=${invite_id}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status) {
          fillingAFstate.selectedPunkt = 2;
          fillingAFstate.selectedInvite = invite_id;
          fillingAFstate.assessmentFormInfo = response.data.content;
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['changeSelectedPunkt']: (num) => {
    fillingAFstate.selectedPunkt = num;
  },
  ['changeSelectedSign']: (criterion, sign, signTarget) => {
    if(signTarget.type == 'checkbox'){
      sign.selected = signTarget.checked;
    }
    else {
      criterion.SIGNS = criterion.SIGNS.map(sign_info => {
        sign_info.selected = false;
        return sign_info;
      });
      sign.selected = true;
    }
  },
  ['sendResultsForm']: (e, changeResSetResults = null) => {
    e.preventDefault();
    imageLoadOperations.openImageLoad();
    let data = {
      invite: fillingAFstate.selectedInvite,
      results: fillingAFstate.assessmentFormInfo
    };
    axios({
      method: 'post',
      url: globalConfig.serverAddress + '/setresults',
      data: data
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status) {
          fillingAFstateOperations.logOut();
          fillingAFstateOperations.getInvitesList();
          changeResSetResults('');
        }
        else {
          changeResSetResults(response.data.content);
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['logOut']: () => {
    fillingAFstate.userInvites = [];
    fillingAFstate.assessmentFormInfo = [];
    fillingAFstate.selectedInvite = [];
    fillingAFstate.selectedPunkt = 1;
  }
};