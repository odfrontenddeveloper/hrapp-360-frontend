import { observable } from "mobx";

export let wpCompetenciesState = observable({
  redactCompetenciesListForWP: false,
  redactWPId: false,
  connectsList: [],
  searchStateFilter: false
});

export let wpcstateOperations = {
  changeState: (id) => {
    if(wpCompetenciesState.redactCompetenciesListForWP){
      if(wpCompetenciesState.redactWPId == id){
        // wpCompetenciesState.connectsList = [];
        wpCompetenciesState.redactCompetenciesListForWP = false;
        wpCompetenciesState.redactWPId = false;
      }
      else {
        // wpCompetenciesState.connectsList = [];
        wpCompetenciesState.redactCompetenciesListForWP = true;
        wpCompetenciesState.redactWPId = id;
      }
    }
    else {
      wpCompetenciesState.redactCompetenciesListForWP = true;
      wpCompetenciesState.redactWPId = id;
    }
  },
  changeSearchState: () => {
    wpCompetenciesState.searchStateFilter = !wpCompetenciesState.searchStateFilter;
  },
  disableState: () => {
    wpCompetenciesState.redactCompetenciesListForWP = false;
    wpCompetenciesState.redactWPId = false;
  }
};