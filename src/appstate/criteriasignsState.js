import { observable } from "mobx";
import { imageLoadOperations } from "./styles_config";
import axios from "axios";
import globalConfig from "../appstate/global_config.js";
import shortid from "shortid";

let rangesIdList = [
  shortid.generate(),
  shortid.generate()
];

export let criteriaSignsState = observable({
  ['competenciesListForCS']: [],
  ['wpListForCS']: [],
  ['redactCompetenceMode']: false,
  ['redactCompetenceModeID']: null,
  ['redactCompetenceModeName']: null,
  ['wpCompetenceConnectsCS']: [],
  ['criterialist']: [],
  ['ranges']: {
    [rangesIdList[0]]: { name: '', value: 0 },
    [rangesIdList[1]]: { name: '', value: 0 }
  },
  ['rangesRedact']: {},
  ['typeCheckboxRedact']: false,
  ['criterionRedact']: ''
});

export const criteriaSignsOperations = {
  ['getCompetenciesListForCS']: () => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: `${globalConfig.serverAddress}/searchcompetence?nameCompetence=&sortabc=${true}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.data.status == true) {
        criteriaSignsState.competenciesListForCS = response.data.content.map((el) => {
          return { ...el, selectedCompetence: el.id == criteriaSignsState.redactCompetenceModeID }
        });
        criteriaSignsState.wpCompetenceConnectsCS = criteriaSignsState.competenciesListForCS.map(el => el.id);
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['getWpListForCS']: () => {
    axios({
      method: 'get',
      url: `${globalConfig.serverAddress}/searchwp?nameWP=&sortabc=${false}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          criteriaSignsState.wpListForCS = response.data.content;
        }
      }
      else {
        listsofdata.listOfCompetencies = [];
        if (response.status >= 200 && response.status < 300) {
          if (response.data.status == true) {
            listsofdata.listOfCompetencies = response.data.content;
          }
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['getWpCompetenceConnectsCS']: (wpid) => {
    if (wpid == 0) {
      criteriaSignsState.wpCompetenceConnectsCS = criteriaSignsState.competenciesListForCS.map(el => el.id);
      return;
    }
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/searchconnectswpcompetence?wp=${wpid}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        criteriaSignsState.wpCompetenceConnectsCS = response.data.content.map((el, i) => {
          return el.competence;
        });
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['getCriteriaList']: (competence) => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: `${globalConfig.serverAddress}/searchcriteria?competence=${competence}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.data.status == true) {
        criteriaSignsState.criterialist = response.data.content;
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['changeRedactCompetenceMode']: (competenceId) => {
    criteriaSignsState.competenciesListForCS = criteriaSignsState.competenciesListForCS.map((el, i) => {
      if (el.id == competenceId) {
        if (el.selectedCompetence == true) {
          criteriaSignsState.redactCompetenceMode = false;
          criteriaSignsState.redactCompetenceModeID = null;
          criteriaSignsState.redactCompetenceModeName = null;
          el.selectedCompetence = false;
        } else {

          criteriaSignsState.redactCompetenceMode = true;
          criteriaSignsState.redactCompetenceModeID = el.id;
          criteriaSignsState.redactCompetenceModeName = el.name;
          el.selectedCompetence = true;
          criteriaSignsOperations.getCriteriaList(competenceId);
        }
      } else {
        el.selectedCompetence = false;
      }
      return el;
    });
  },
  ['addSignField']: (modeAdd) => {
    let newSignID = shortid.generate();
    criteriaSignsState[modeAdd ? 'ranges' : 'rangesRedact'][newSignID] = { name: '', value: 0 };
  },
  ['deleteSignField']: (keyItem, modeAdd) => {
    delete criteriaSignsState[modeAdd ? 'ranges' : 'rangesRedact'][keyItem];
  },
  ['addCriterion']: (data, changeColor, changeResAdd, resetAddCriteriaForm) => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'post',
      url: globalConfig.serverAddress + '/addcriterion',
      data: data
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          criteriaSignsState.criterialist.push(response.data.content);
          changeColor(true);
          changeResAdd('Критерий добавлен.');
          resetAddCriteriaForm();
        }
        else {
          changeColor(false);
          changeResAdd(response.data.content);
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['deleteCriterion']: (id, changeresDelete, closeDeleteCriterionForm) => {
    imageLoadOperations.openImageLoad();
    let data = {
      id: id
    };
    axios({
      method: 'delete',
      url: globalConfig.serverAddress + '/deletecriterion',
      data: data
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          closeDeleteCriterionForm();
          criteriaSignsState.criterialist = criteriaSignsState.criterialist.filter(criterion => {
            return criterion.criterion.id != id;
          });
        }
        else {
          changeresDelete(response.data.content);
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['redactCriterion']: (data, changeColor, changeResAdd, redactID) => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'patch',
      url: globalConfig.serverAddress + '/redactcriterion',
      data: { ...data, id: redactID }
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      console.log(response);
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          changeColor(true);
          changeResAdd('Критерий обновлён.');
          criteriaSignsState.criterialist = criteriaSignsState.criterialist.map(criterion => {
            if (criterion.criterion.id == redactID) {
              return response.data.content;
            }
            return criterion;
          });
        }
        else {
          changeColor(false);
          changeResAdd(response.data.content);
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['logOut']: () => {
    criteriaSignsState.competenciesListForCS = [];
    criteriaSignsState.wpListForCS = [];
    criteriaSignsState.redactCompetenceMode = false;
    criteriaSignsState.redactCompetenceModeID = null;
    criteriaSignsState.redactCompetenceModeName = null;
    criteriaSignsState.wpCompetenceConnectsCS = [];
    criteriaSignsState.criterialist = [];
    criteriaSignsState.ranges = {
      [rangesIdList[0]]: { name: '', value: 0 },
      [rangesIdList[1]]: { name: '', value: 0 }
    };
    criteriaSignsState.rangesRedact = {};
    criteriaSignsState.typeCheckboxRedact = false;
    criteriaSignsState.criterionRedact = '';
  }
};