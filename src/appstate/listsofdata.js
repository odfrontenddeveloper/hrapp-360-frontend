import { observable } from "mobx";

let listsofdata = observable({
  listOfWP: [],
  listOfCompetencies: [],
  wpCompetenciesConnect: []
});

export default listsofdata;