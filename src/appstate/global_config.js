let globalConfig = {
    serverAddress: process.env.NODE_ENV === 'development' ? 'http://127.0.0.1:5670' : 'http://142.93.141.170:5670'
};

export let image_path = {
    development: 'http://localhost:5670/image_base?path=',
    production: 'http://142.93.141.170:5670/image_base?path='
};
  
export default globalConfig;