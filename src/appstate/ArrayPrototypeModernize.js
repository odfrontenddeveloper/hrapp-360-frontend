export default Array.prototype.remove = function(element){
  if(this.indexOf(element) != -1){
    this.splice(this.indexOf(element), 1);
  }
  return this;
}