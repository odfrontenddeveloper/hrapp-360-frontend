import authState from "./authstate.js";
import axios from 'axios';
import globalConfig from "./global_config.js";
import listsofdata from "./listsofdata.js";
import { wpCompetenciesState } from "./wpCompetenciesState.js";
import { userslist } from "./redactStaffState.js";
import { criteriaSignsOperations } from "./criteriasignsState.js";
import { assessmentConfigStoreOperations, assessmentConfigStateOperations } from "./assessmentConfigState.js";
import { ugstateOperations } from "./usersGroupsState.js";
import { resultsStateOperations } from "./showResultsState.js";
import { fillingAFstateOperations } from "./fillingassessmentsformsState.js";

var authOperations = {
  getCookie: (name) => {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if(parts.length == 2){
      return parts.pop().split(";").shift();
    }
    else {
      return false;
    }
  },
  setCookie: (name, value, options) => {
    document.cookie = `${name}=${value}; path=/; expires=Tue, 19 Jan 2038 03:14:07 GMT`;
    return true;
  },
  deleteCookie: (name) => {
    document.cookie = `${name}=0; path=/; max-age=-1`;
  },
  logIn: (login, token, utype) => {
    let resCreate = authOperations.setCookie('userLogin', login) && authOperations.setCookie('userToken', token) && authOperations.setCookie('userType', utype);
    if(resCreate){
      authState.auth = true;
    }
    return true;
  },
  checkAuth: async () => {
    let userLogin = authOperations.getCookie('userLogin');
    let userToken = authOperations.getCookie('userToken');
    let userType = authOperations.getCookie('userType');
    if(userLogin && userToken && userType) {
      let login = authOperations.getCookie('userLogin');
      let token = authOperations.getCookie('userToken');
      await axios({
        method: 'get',
        url: globalConfig.serverAddress + '/checktoken',
        data: {
          login: login,
          token: token
        }
      }).then(response => {
        if(response.data == false){
          authOperations.logOut();
        }
      }).catch(error => {
        console.log(error);
      });
      authState.auth = true;
    }
    return true;
  },
  resetState: () => {
    authOperations.deleteCookie('userLogin');
    authOperations.deleteCookie('userToken');
    authOperations.deleteCookie('userType');
    authState.auth = false;
    listsofdata.listOfWP = [];
    listsofdata.wpCompetenciesConnect = [];
    listsofdata.listOfCompetencies = [];
    wpCompetenciesState.redactCompetenciesListForWP = false;
    wpCompetenciesState.redactWPId = false;
    wpCompetenciesState.connectsList = [];
    wpCompetenciesState.searchStateFilter = false;
    userslist.list = [];
    userslist.isAdmin = null;
    userslist.isModer = null;
    userslist.redactId = null;
    userslist.wplistforoptions = [];
  },
  logOut: () => {
    authOperations.resetState();
    criteriaSignsOperations.logOut();
    ugstateOperations.logOut();
    assessmentConfigStateOperations.logOut();
    assessmentConfigStoreOperations.logOut();
    fillingAFstateOperations.logOut();
    resultsStateOperations.logOut();
  },
  getUserAuthData: () => {
    return {
      login: authOperations.getCookie('userLogin'),
      token: authOperations.getCookie('userToken')
    };
  }
};

export default authOperations;