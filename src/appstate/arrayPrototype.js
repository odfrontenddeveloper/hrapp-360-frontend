Array.prototype.unique = function(){
  let newArray = [];
  this.forEach(element => {
    if(!newArray.includes(element)){
      newArray.push(element);
    }
  });
  return newArray;
}

/* =================== */

Array.prototype.createDataset = function (value = null) {
  let keysList = [];
  this.forEach(element => {
    Object.keys(element).forEach(field => {
      if (!keysList.includes(field)) {
        keysList.push(field);
      }
    });
  });
  let collection = this.map(element => {
    let newRow = {};
    keysList.forEach(field => {
      if (!element[field]) {
        newRow[field] = value;
      } else {
        newRow[field] = element[field];
      }
    });
    return newRow;
  });
  return collection;
}

Array.prototype.selectFields = function (fields, value = null) {
  return this.map(element => {
    let returnObject = {};
    fields.forEach(field => {
      if(!element[field]){
        returnObject[field] = value ? value : null;
      } else {
        returnObject[field] = element[field];
      }
    });
    return returnObject;
  });
}

Array.prototype.deleteFields = function(fields){
  return this.map(element => {
    let obj = {};
    Object.keys(element).filter(el => {
      return !fields.includes(el);
    }).map(el => {
      obj[el] = element[el];
    });
    return obj;
  });
}

Array.prototype.findBy = function(fields){
  return this.filter(element => {
    return Object.keys(element).every(() => {
      return Object.keys(fields).every(field => {
        return fields[field] == element[field];
      });
    });
  });
}



// let myCollection = [
//   { a: 1, b: 2 },
//   { b: 3, c: 4 },
//   { c: 5, d: 6 }
// ].createDataset(null);

// console.log(myCollection);



// let myCollection1 = [
//   { a: 1, b: 2 },
//   { b: 3, c: 4 },
//   { c: 5, d: 6 }
// ].selectFields(['a', 'b'], 'default');

// console.log(myCollection1);



// let myCollection2 = [
//   { a: 1, b: 2 },
//   { a: 3, b: 4 },
//   { a: 5, b: 6 }
// ].deleteFields(['a', 'b']);

// console.log(myCollection2);



// let myCollection2 = [
//   { a: 1, b: 2 },
//   { a: 1, b: 4 },
//   { a: 5, b: 6 }
// ].findBy({'a': 1});

// console.log(myCollection2);



// functions list
/*
  createDataset
  selectFields
  deleteFields
  findBy
*/