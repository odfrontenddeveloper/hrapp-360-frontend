import { observable } from "mobx";
import authOperations from "./userauthOperations";

let timeinterval = 200;

export let imageLoadStatus = observable({
  isOpen: false
});

export let stylesConfig = observable({
  dangerColor: 'rgb(191, 48, 0)',
  standartColor: 'rgb(255, 255, 255)',
  hideElementsTiming: timeinterval,
  animationHideElement: timeinterval * 0.001 + 's hideBlock forwards',
  nightMode: authOperations.getCookie('nightMode') == 'on' ? true : false,
  imageLoadIsOpen: false
});

export let imageLoadOperations = {
  openImageLoad: () => {
    imageLoadStatus.isOpen = true;
  },
  closeImageLoad: () => {
    imageLoadStatus.isOpen = false;
  }
}
