import { observable } from "mobx";
import '../appstate/arrayPrototype';

export let resultsState = observable({
  ['assessmentsList']: [],
  ['showMode']: false,
  ['showResMode']: false,
  ['showId']: false,
  ['resultid_user_from']: null,
  ['selectedPunkt']: 1,
  ['resultsList']: [],
  ['chartBlock']: null,
  ['selectedUsersForCheckRes']: []
});

export let resultsStateOperations = observable({
  ['changeSelectedPunkt']: (value) => {
    resultsState.selectedPunkt = value;
  },
  ['changeState']: (id, data) => {
    if (resultsState.showMode) {
      if (resultsState.showId == id) {
        resultsStateOperations.disableState();
        resultsState.showMode = false;
        resultsState.showId = false;
        resultsState.resultsList = [];
      }
      else {
        resultsStateOperations.disableState();
        resultsState.resultsList = data;
          resultsState.selectedUsersForCheckRes = data.map(info => {
            return {
              user_from: info.USER_FROM,
              user_to: info.USER_TO,
              selectedForDisplayResults: true
            };
          }).map(info => JSON.stringify(info)).unique().map(info => { 
            return JSON.parse(info);
          });
        resultsState.showMode = true;
        resultsState.showId = id;
      }
    }
    else {
      resultsStateOperations.disableState();
      resultsState.resultsList = data;
          resultsState.selectedUsersForCheckRes = data.map(info => {
            return {
              user_from: info.USER_FROM,
              user_to: info.USER_TO,
              selectedForDisplayResults: true
            };
          }).map(info => JSON.stringify(info)).unique().map(info => { 
            return JSON.parse(info);
          });
      resultsState.showMode = true;
      resultsState.showId = id;
    }
  },
  ['openUserResults']: (id_t) => {
    // if (resultsState.showResMode) {
    //   if (resultsState.resultid_user_to == id_t) {
    //     resultsState.showResMode = false;
    //     resultsState.resultid_user_to = false;
    //     resultsState.resultsList = [];
    //   }
    //   else {
    //     resultsState.showResMode = true;
    //     resultsState.resultid_user_to = id_t;
    //     resultsState.selectedPunkt = 2;
    //   }
    // }
    // else {
      resultsState.showResMode = true;
      resultsState.resultid_user_to = id_t;
      resultsState.selectedPunkt = 2;
    // }
  },
  ['disableState']: () => {
    resultsState['showMode'] = false;
    resultsState['showId'] = false;
    resultsState['showResMode'] = false;
    resultsState['resultsList'] = [];
    resultsState['resultid_user_from'] = null;
    resultsState['resultid_user_to'] = null;
    resultsState['selectedUsersForCheckRes'] = [];
  },
  ['logOut']: () => {
    resultsState['assessmentsList'] = [];
    resultsState['showMode'] = false;
    resultsState['showResMode'] = false;
    resultsState['showId'] = false;
    resultsState['resultsList'] = [];
    resultsState['selectedPunkt'] = 1;
    resultsState['resultid_user_from'] = null;
    resultsState['resultid_user_to'] = null;
    resultsState['selectedUsersForCheckRes'] = [];
  }
});