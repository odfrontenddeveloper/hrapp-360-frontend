import { observable } from "mobx";

export let userslist = observable({
  list: [],
  wplistforoptions: [],
  redactId: null,
  isModer: null,
  isAdmin: null
});