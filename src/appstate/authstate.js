import { observable } from "mobx";

var authState = observable({
  auth: false
});

export default authState;