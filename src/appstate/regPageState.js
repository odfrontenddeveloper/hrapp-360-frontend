import { observer } from "mobx-react";
import { observable, autorun } from "mobx";
import { action } from "mobx";

var regPageState = observable({
  formRegistration: true
});

export default regPageState;
