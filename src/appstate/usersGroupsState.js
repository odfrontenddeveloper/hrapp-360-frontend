import { observable } from "mobx";
import { imageLoadOperations } from "./styles_config";
import axios from "axios";
import globalConfig from "./global_config";
import authOperations from "./userauthOperations";

export let ugstate = observable({
  listOfUG: [],
  userslistForUG: [],
  redactUGID: null,
  redactUGMode: false,
  usersGroupsConnections: [],
  searchStateFilter: false,
  listOfUGForAssessment: []
});

export let ugstateOperations = {
  ['getUGList']: () => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/searchug?nameUG=&sortabc=${false}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          ugstate.listOfUG = response.data.content;
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['getUserListForUG']: () => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/getuserslist?admin=${authOperations.getCookie('userLogin')}`
    }).then(response => {
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          ugstate.userslistForUG = response.data.content;
        }
      }
      imageLoadOperations.closeImageLoad();
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['changeRedactUGState']: (id) => {
    if (ugstate.redactUGMode) {
      if (ugstate.redactUGID == id) {
        ugstate.redactUGMode = false;
        ugstate.redactUGID = false;
      }
      else {
        ugstate.redactUGMode = true;
        ugstate.redactUGID = id;
      }
    }
    else {
      ugstate.redactUGMode = true;
      ugstate.redactUGID = id;
    }
  },
  ['disableState']: () => {
    ugstate.redactUGMode = false;
    ugstate.redactUGID = false;
    ugstate.searchStateFilter = false;
  },
  ['changeSearchState']: () => {
    ugstate.searchStateFilter = !ugstate.searchStateFilter;
  },
  ['getUGListForAssessment']: () => {
    imageLoadOperations.openImageLoad();
    axios({
      method: 'get',
      url: globalConfig.serverAddress + `/searchug?nameUG=&sortabc=${false}`
    }).then(response => {
      imageLoadOperations.closeImageLoad();
      if (response.status >= 200 && response.status < 300) {
        if (response.data.status == true) {
          ugstate.listOfUGForAssessment = response.data.content;
        }
      }
    }).catch(error => {
      imageLoadOperations.closeImageLoad();
      console.log(error);
    });
  },
  ['logOut']: () => {
    ugstate.listOfUG = [];
    ugstate.userslistForUG = [];
    ugstate.redactUGID = null;
    ugstate.redactUGMode = false;
    ugstate.usersGroupsConnections = [];
    ugstate.listOfUGForAssessment = [];
  }
};