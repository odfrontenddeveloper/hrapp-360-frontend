const path = require('path')
const express = require('express')
const app = express() // create express app

app.use(express.static(path.join(__dirname, 'dist')));

app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, 'dist', 'index.html'));
});

// start express server on port 5000
app.listen(5368, () => {
    console.log('server started on port 5368')
});
